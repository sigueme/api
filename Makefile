.PHONY: pytest clean lint build test

build:
	./setup.py sdist && ./setup.py bdist_wheel

test: lint pytest

clean:
	rm -rf dist/

pytest:
	pytest -xvv

lint:
	flake8 --exclude=.env,.tox,dist,docs,build,*.egg,.venv,migrations --max-line-length 99 .
