#!/usr/bin/env python3
from setuptools import setup
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.rst')) as f:
    long_description = f.read()

with open(path.join(here, 'fleety', 'version.txt')) as f:
    version = f.read().strip()

setup(
    name='fleety',
    description='Device tracker for package delivery.',
    long_description=long_description,
    url='https://gitlab.com/sigueme/api',

    version=version,

    author='Abraham Toriz Cruz',
    author_email='categulario@gmail.com',
    license='GPLv3',

    classifiers=[
        'Development Status :: 4 - Beta',

        'Intended Audience :: Developers',
        'Topic :: Software Development :: Libraries :: Python Modules',

        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',

        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
    ],

    keywords='process',

    packages=[
        'fleety',
        'fleety.db',
        'fleety.db.models',
        'fleety.db.lua',
        'fleety.views',
        'fleety.views.api',
        'fleety.helpers',
        'fleety.auth',
        'fleety.http',
        'fleety.http.forms',
    ],

    include_package_data=True,

    package_data={
        'fleety': ['db/lua/*.lua', 'version.txt'],
    },

    entry_points={
        'console_scripts': [
            'fleety = fleety.main:main',
        ],
    },

    install_requires=[
        'redis',
        'gunicorn',
        'hiredis',
        'bcrypt',
        'psycopg2-binary',
        'coralillo>=2.0.3',
        'hashids',
        'polyline',
        'requests',
        'crontab',
        'Flask',
        'Flask-Testing',
        'Flask-Redis',
        'Flask-Cors',
        'SQLAlchemy',
        'Flask-SQLAlchemy',
        'Flask-Coralillo',
        'Flask-Migrate',
        'GeoAlchemy2',
        'Shapely',
    ],

    setup_requires=[
        'pytest-runner',
    ],

    tests_require=[
        'pytest',
    ],
)
