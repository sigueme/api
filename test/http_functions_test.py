import pytest
from fleety.http.functions import parse_range
from coralillo.errors import InvalidFieldError


def test_parse_range():
    assert parse_range('100+1', 'time') == (100, 101)
    assert parse_range('3784+15', 'time') == (3784, 3799)
    assert parse_range('3784+0', 'time') == (3784, 3784)

    assert parse_range('2343', 'time') == (2343, 2343)

    assert parse_range('', 'time') is None
    assert parse_range(None, 'time') is None

    with pytest.raises(InvalidFieldError) as exc_info:
        parse_range('invalidfield', 'time')

    error = exc_info.value
    assert error.to_json() == {
        'i18n': 'errors.time.invalid',
        'field': 'time',
        'detail': 'time is not valid',
    }
