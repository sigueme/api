from datetime import datetime, timedelta
from flask import json
from itertools import starmap, repeat

from fleety.db.models import Organization, User
from fleety.db.models.bounded import Device, Fleet, Geofence, Route
from fleety.db.models.sql import Log, ReportScheduler, Report, Trip, TripStatus
from fleety import sql


PWD = 'bcrypt$$2b$12$gWQweaiLPJr2OHSPOshyQe3zmnSAPGv2pA.PwOIuxZ3ylvLMN7h6C'


def make_org(subdomain):
    return Organization(
        name=subdomain + 'inc.',
        subdomain=subdomain,
    ).save()


def make_admin(email, org, allow=None, **kwargs):
    name, last_name = email.split('@')

    admin = User(
        name=name,
        last_name=last_name,
        email=email,
        password=PWD,
        **kwargs,
    ).save()

    admin.orgs.set([org])
    admin.allow(allow or org.subdomain)

    return admin


def make_auth(user):
    return {
        'Authorization': 'Basic %s' % user.basic_auth()
    }


def make_date(m):
    """ make dates for tests """
    return datetime(2017, 9, 5, 12, 0, 0) + timedelta(minutes=m)


def make_fleet(**kwargs):
    return Fleet(**kwargs).save()


def make_trip(device, **kwargs):
    trip = Trip(
        org_subdomain='testing',
        status=TripStatus.ONGOING.name,
        device_id=device.id,
        departure=datetime(2017, 7, 1, 9),
    )

    sql.session.add(trip)
    sql.session.commit()

    return trip


def make_route():
    return Route().save()


def make_device(index, fleet, **kwargs):
    dev = Device(
        name='dev{}'.format(index),
        created_at=make_date(index),
        traccar_id=index,
        code='{:05d}'.format(index),
        **kwargs,
    ).save()

    dev.fleet.set(fleet)

    return dev


def make_geofence():
    return Geofence().save()


def make_log(index, org_subdomain='testing', **kwargs):
    log = Log(
        org_subdomain=org_subdomain,
        created_at=make_date(index),
        **kwargs,
    )

    sql.session.add(log)
    sql.session.commit()

    return log


def make_report_scheduler(org_subdomain='testing', builder='SamplePDFBuilder',
                          **kwargs):
    rs = ReportScheduler(
        org_subdomain=org_subdomain,
        builder=builder,
        **kwargs,
    )

    sql.session.add(rs)
    sql.session.commit()

    return rs


def make_report(org_subdomain='testing', builder='SamplePDFBuilder', **kwargs):
    rep = Report(
        org_subdomain=org_subdomain,
        builder=builder,
        **kwargs,
    )

    sql.session.add(rep)
    sql.session.commit()

    return rep


def remove_fields(json_data, fields):
    if type(json_data) == list:
        json_data = list(starmap(
            remove_fields,
            zip(json_data, repeat(fields))
        ))
    elif type(json_data) == dict:
        json_data = {
            key: remove_fields(value, fields)
            for key, value in json_data.items() if key not in fields
        }

    return json_data


def assert_response(client, method, url, expected, data=None, status_code=200,
                    ignore_fields=None, org=None, admin=None):
    ''' asserts that a request returns the expected response '''
    org = org or make_org('testing')
    admin = admin or make_admin('admin@testing.org', org)

    res = getattr(client, method)(url, data=data, headers=make_auth(admin))

    if res.status_code != status_code:
        print(json.loads(res.data))

    assert res.status_code == status_code, \
        'Received status code {} does not match expected {}'.format(
            res.status_code, status_code
        )

    if res.data:
        json_data = json.loads(res.data)

        if ignore_fields:
            json_data = remove_fields(json_data, ignore_fields)

        assert json_data == expected, 'Response does not match expected output'
