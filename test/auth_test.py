from datetime import datetime, timedelta
from flask import json

from fleety.db.models import User, Organization

from .utils import make_admin, make_org, PWD


def test_login(client):
    org = make_org('baz')
    admin = make_admin('admin@testing.org', org)

    res = client.post('/auth/login', data={
        'email': admin.email,
        'password': '123456',
        'org_subdomain': org.subdomain,
    })

    assert res.status_code == 200

    response = json.loads(res.data)

    # these three values can make tests fail if request and assertions
    # happen in different seconds
    del response['data']['session']['expires']
    del response['data']['session']['created_at']
    del response['data']['session']['updated_at']
    del response['data']['session']['last_read']

    assert response == {
        'data': {
            'session': {
                '_type': 'user',
                'id': admin.id,
                'name': admin.name,
                'last_name': admin.last_name,
                'email': admin.email,
                'is_active': admin.is_active,
                'is_god': admin.is_god,
                'confirmed': admin.confirmed,
                'api_key': admin.api_key,
                'permissions': ['baz'],
                'empty_account': True,
            },
            'org': org.to_json(),
        },
    }


def test_key_login(client):
    org = make_org('baz')
    admin = make_admin('admin@testing.org', org)

    res = client.post('/auth/key_login', data=dict(
        api_key=admin.api_key,
        org_subdomain=org.subdomain,
    ))

    assert res.status_code == 200

    response = json.loads(res.data)
    del response['data']['session']['expires']
    del response['data']['session']['created_at']
    del response['data']['session']['updated_at']
    del response['data']['session']['last_read']

    assert response == {
        'data': {
            'session': {
                '_type': 'user',
                'id': admin.id,
                'name': admin.name,
                'last_name': admin.last_name,
                'email': admin.email,
                'is_active': True,
                'is_god': False,
                'confirmed': False,
                'api_key': admin.api_key,
                'permissions': ['baz'],
                'empty_account': True,
            },
            'org': org.to_json(),
        },
    }


def test_first_login(client):
    org = make_org('baz')
    admin = make_admin('admin@testing.org', org)

    res = client.post('/auth/login', data=dict(
        email=admin.email,
        password='123456',
        org_subdomain=org.subdomain,
    ))

    assert res.status_code == 200

    response = json.loads(res.data)

    assert response['data']['session']['empty_account']


def test_login_with_wrong_params(client):
    res = client.post('/auth/login', data=dict())

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'i18n': 'errors.email.required',
                'detail': 'email is required',
                'field': 'email',
            },
            {
                'i18n': 'errors.org_subdomain.required',
                'detail': 'org_subdomain is required',
                'field': 'org_subdomain',
            },
            {
                'i18n': 'errors.password.required',
                'detail': 'password is required',
                'field': 'password',
            },
        ]
    }


def test_cannot_login_invalid_email(client):
    org = make_org('baz')

    res = client.post('/auth/login', data={
        'email': 'curro@foo.com',
        'password': 'nones',
        'org_subdomain': org.subdomain,
    })

    assert res.status_code == 401
    assert json.loads(res.data) == {
        'errors': [{
            'i18n': 'errors.http401.not_found',
            'detail': 'not_found email',
            'field': 'email',
        }],
    }


def test_cannot_login_invalid_credentials(client):
    org = make_org('baz')
    admin = make_admin('admin@testing.org', org)

    res = client.post('/auth/login', data={
        'email': admin.email,
        'password': 'nones',
        'org_subdomain': org.subdomain,
    })

    assert res.status_code == 401
    assert json.loads(res.data) == {
        'errors': [{
            'i18n': 'errors.http401.invalid',
            'detail': 'invalid password',
            'field': 'password',
        }],
    }


def test_cannot_login_if_not_active(client):
    org = make_org('baz')

    disabled = User(
        name='Disabled',
        last_name='Tomson',
        email='disabled@testing.com',
        password=PWD,
        is_active=False,
    ).save()
    disabled.orgs.add(org)

    res = client.post('/auth/login', data=dict(
        email='disabled@testing.com',
        password='123456',
        org_subdomain=org.subdomain,
    ))

    assert res.status_code == 401
    assert json.loads(res.data) == {
        'errors': [{
            'i18n': 'errors.http401.disabled',
            'detail': 'disabled email',
            'field': 'email',
        }],
    }


def test_cannot_login_if_not_in_org(client):
    org = make_org('baz')

    User(
        name='User',
        last_name='Last',
        email='user@otherorg.com',
        password=PWD,
        is_active=True,
    ).save()

    res = client.post('/auth/login', data=dict(
        email='user@otherorg.com',
        password='123456',
        org_subdomain=org.subdomain,
    ))

    assert res.status_code == 401
    assert json.loads(res.data) == {
        'errors': [{
            'i18n': 'errors.http401.not_org_member',
            'detail': 'not_org_member email',
            'field': 'email',
        }],
    }


def test_can_login_if_expired(client):
    org = make_org('baz')

    expired = User(
        name='Expired',
        last_name='Wilson',
        email='expired@testing.com',
        password=PWD,
        is_active=True,
        expired=datetime.now() - timedelta(hours=1),
    ).save()
    expired.orgs.add(org)

    res = client.post('/auth/login', data=dict(
        email='expired@testing.com',
        password='123456',
        org_subdomain=org.subdomain,
    ))

    response = json.loads(res.data)

    assert res.status_code == 200
    assert response['data']['session']['id'] == expired.id


def test_can_login_if_not_yet_expired(client):
    org = make_org('baz')

    notyetexpired = User(
        name='Not Yet Expired',
        last_name='Wilson',
        email='notyetexpired@testing.com',
        password=PWD,
        is_active=True,
        expired=datetime.now() + timedelta(hours=1),
    ).save()
    notyetexpired.orgs.add(org)

    res = client.post('/auth/login', data=dict(
        email='notyetexpired@testing.com',
        password='123456',
        org_subdomain=org.subdomain,
    ))

    response = json.loads(res.data)

    assert res.status_code == 200
    assert response['data']['session']['id'] == notyetexpired.id


def test_ping_with_credentials(client):
    org = make_org('baz')
    admin = make_admin('admin@testing.org', org)

    res = client.get(
        '/auth/ping?org_subdomain={}'.format(org.subdomain), headers={
            'Authorization': 'Basic %s' % admin.basic_auth()
        })
    response = json.loads(res.data)

    assert res.status_code == 200
    assert response['data']['session']['id'] == admin.id


def test_ping_without_credentials(client):
    res = client.get('/auth/ping')
    assert res.status_code == 200
    assert json.loads(res.data) == {
        'data': {
            'session': None,
            'org': None,
        },
    }


def test_confirm(client):
    org = make_org('baz')
    unconfirmed = User(
        name='Unconfirmed',
        last_name='Wayers',
        email='unconfirmed@testing.com',
        password=PWD,
    ).save()
    unconfirmed.orgs.add(org)

    assert unconfirmed.confirmed is False

    confirm_token = unconfirmed.confirm_token()
    res = client.get('/auth/confirm?token={}&org={}'.format(
        confirm_token, org.subdomain
    ))

    assert res.status_code == 302
    assert res.headers['Location'] == 'http://baz.getfleety.dev'

    unconfirmed.reload()

    assert unconfirmed.confirmed


def test_confirm_only_owned_organizations(client):
    otherorg = Organization(
        name='Other Inc.',
        subdomain='other',
    ).save()
    unconfirmed = User(
        name='Unconfirmed',
        last_name='Wayers',
        email='unconfirmed@testing.com',
        password=PWD,
    ).save()

    assert otherorg not in unconfirmed.orgs

    confirm_token = unconfirmed.id
    res = client.get('/auth/confirm?token={}&org={}'.format(
        confirm_token, otherorg.subdomain
    ))

    assert res.status_code == 400


def test_confirm_token_expires_in_24h(client):
    org = make_org('baz')
    User(
        name='Unconfirmed',
        last_name='Wayers',
        email='unconfirmed@testing.com',
        password=PWD,
    ).save()

    confirm_token = 'g34f4'
    res = client.get('/auth/confirm?token={}&org={}'.format(
        confirm_token, org.subdomain
    ))
    assert res.status_code == 400

    confirm_token = 'nf3if'
    res = client.get('/auth/confirm?token={}&org={}'.format(
        confirm_token, org.subdomain
    ))
    assert res.status_code == 400


def test_confirm_invalid_token(client):
    org = make_org('baz')
    confirm_token = 'f34843hr8u43'

    res = client.get('/auth/confirm?token={}&org={}'.format(
        confirm_token, org.subdomain
    ))

    assert res.status_code == 400
