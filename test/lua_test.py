from coralillo.datamodel import Location
from fleety import eng
from fleety.db.models.bounded import Device, Fleet
from fleety.core import id_function
import time
from datetime import datetime
import math

from .utils import make_org


def test_create_device():
    org = make_org('testing')

    new_time = int(time.time())
    val = eng.lua.device_motion(args=[
        '-100',
        '20',
        org.code + '01234',
        1,
        new_time,
        new_time,
        id_function(),
        id_function(),
        0,  # outdated
        datetime.fromtimestamp(new_time).strftime('%Y-%m-%dT%H:%M:%SZ'),
    ])

    newdevice = Device.get_by('code', '01234')

    assert val == [org.subdomain, newdevice.id]

    assert newdevice is not None

    assert newdevice.code == '01234'
    assert newdevice.created_at == datetime.utcfromtimestamp(new_time)
    assert newdevice.last_pos == Location(lon=-100, lat=20)


def test_update_device_position():
    org = make_org('testing')

    old_position = Location(-103.30375671386719, 20.639531429485633)
    fleet = Fleet(abbr='NDV', name='the fleet').save()
    dev = Device(
        code='01234',
        traccar_id=1,
        last_pos=old_position,
        created_at=datetime(2017, 6, 23),
    ).save()
    dev.fleet.set(fleet)

    new_time = int(time.time())

    eng.lua.device_motion(args=[
        '-100',
        '20',
        org.code + '01234',
        1,
        new_time,
        new_time,
        id_function(),
        id_function(),
        0,
        datetime.fromtimestamp(new_time).strftime('%Y-%m-%dT%H:%M:%SZ'),
    ])

    old_device = Device.get_by('code', '01234')

    assert old_device is not None

    assert old_device.code == '01234'
    assert old_device.created_at == datetime(2017, 6, 23)
    assert old_device.last_pos == Location(lon=-100, lat=20)


def test_set_device_course_n_speed():
    org = make_org('testing')
    t1 = datetime(2017, 10, 25, 16, 11, 0)
    t2 = datetime(2017, 10, 25, 17, 11, 0)  # one hour
    p1 = (-103.36967468261719, 20.657201194347387)
    p2 = (-102.960833, 21.03833)

    eng.lua.device_motion(args=[
        p1[0],
        p1[1],
        org.code + '01234',
        1,
        int(t1.timestamp()),
        int(t1.timestamp()),
        id_function(),
        id_function(),
        0,
        t1.strftime('%Y-%m-%dT%H:%M:%SZ'),
    ])
    eng.lua.device_motion(args=[
        p2[0],
        p2[1],
        org.code + '01234',
        1,
        int(t2.timestamp()),
        int(t2.timestamp()),  # server time
        id_function(),
        id_function(),
        0,
        t2.strftime('%Y-%m-%dT%H:%M:%SZ'),
    ])

    device = Device.get_by('code', '01234')

    assert device is not None

    assert device.code == '01234'
    assert device.updated_at == t1
    assert device.last_update == t2
    assert device.last_pos == Location(lon=p2[0], lat=p2[1])
    assert abs(device.last_speed - 60) < .1
    assert abs(device.last_course * 180 / math.pi - 45) < .1


def test_speed_compute_fails_gracefuly_if_no_last_update():
    org = make_org('testing')
    t1 = datetime(2017, 10, 25, 16, 11, 0)
    t2 = datetime(2017, 10, 25, 17, 11, 0)  # one hour
    p2 = (-102.960833, 21.03833)

    fleet = Fleet().save()
    dev = Device(code='01234', updated_at=t1).save()
    dev.fleet.set(fleet)

    eng.lua.device_motion(args=[
        p2[0],
        p2[1],
        org.code + '01234',
        1,
        int(t2.timestamp()),
        int(t2.timestamp()),  # server time
        id_function(),
        id_function(),
        0,
        t2.strftime('%Y-%m-%dT%H:%M:%SZ'),
    ])

    device = Device.get_by('code', '01234')

    assert device is not None

    assert device.code == '01234'
    assert device.updated_at == t1
    assert device.last_update == t2
    assert device.last_pos == Location(lon=p2[0], lat=p2[1])
    assert device.last_speed == 0
    assert device.last_course == 0


def test_computed_course_with_no_distance():
    org = make_org('testing')
    t1 = datetime(2017, 10, 25, 16, 11, 0)
    t2 = datetime(2017, 10, 25, 17, 11, 0)  # one hour
    p1 = (-103.36967468261719, 20.657201194347387)

    fleet = Fleet().save()
    dev = Device(
        code='01234',
        updated_at=t1,
        last_pos=Location(lon=p1[0], lat=p1[1]),
        last_update=t1,
        last_course=0.5,
    ).save()
    dev.fleet.set(fleet)

    eng.lua.device_motion(args=[
        p1[0],
        p1[1],
        org.code + '01234',
        1,
        int(t2.timestamp()),
        int(t2.timestamp()),  # server time
        id_function(),
        id_function(),
        0,
        t2.strftime('%Y-%m-%dT%H:%M:%SZ'),
    ])

    device = Device.get_by('code', '01234')

    assert device is not None

    assert device.code == '01234'
    assert device.updated_at == t1
    assert device.last_update == t2
    assert device.last_pos == Location(lon=p1[0], lat=p1[1])
    assert device.last_speed == 0
    assert device.last_course == 0.5


def test_computed_speed_no_timedelta():
    org = make_org('testing')
    t1 = datetime(2017, 10, 25, 16, 11, 0)
    p1 = (-103.36967468261719, 20.657201194347387)
    p2 = (-102.960833, 21.03833)

    fleet = Fleet().save()
    dev = Device(
        code='01234',
        updated_at=t1,
        last_pos=Location(lon=p1[0], lat=p1[1]),
        last_update=t1,
        last_speed=20,
    ).save()
    dev.fleet.set(fleet)

    eng.lua.device_motion(args=[
        p2[0],
        p2[1],
        org.code + '01234',
        1,
        int(t1.timestamp()),
        int(t1.timestamp()),  # server time
        id_function(),
        id_function(),
        0,
        t1.strftime('%Y-%m-%dT%H:%M:%SZ'),
    ])

    device = Device.get_by('code', '01234')

    assert device is not None

    assert device.code == '01234'
    assert device.updated_at == t1
    assert device.last_update == t1
    assert device.last_pos == Location(lon=p2[0], lat=p2[1])
    assert device.last_speed == 20
    assert device.last_course == 0.7853607239427406
