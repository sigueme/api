from flask import json
from fleety.db.models.bounded import Route


def test_create_route(client, auth):
    res = client.post('/v1/testing/route', headers=auth, data=dict(
        name='myroute',
        polyline='kwh}BxuzuRpwC|uG|kFk|GyoEcoE'
    ))

    assert res.status_code == 201

    body = json.loads(res.data)
    assert body['data']['polyline'] == 'kwh}BxuzuRpwC|uG|kFk|GyoEcoE'


def test_list_route(client, auth):
    route = Route(
        name='myroute',
        polyline='kwh}BxuzuRpwC|uG|kFk|GyoEcoE'
    ).save()

    res = client.get('/v1/testing/route', headers=auth)
    assert res.status_code == 200

    body = json.loads(res.data)
    assert body['data'][0] == route.to_json()


def test_read_route(client, auth):
    route = Route(
        name='myroute',
        polyline='kwh}BxuzuRpwC|uG|kFk|GyoEcoE'
    ).save()

    res = client.get('/v1/testing/route/{}'.format(route.id), headers=auth)

    body = json.loads(res.data)

    assert res.status_code == 200
    assert body['data'] == route.to_json()


def test_update_route(client, auth):
    route = Route(
        name='myroute',
        polyline='kwh}BxuzuRpwC|uG|kFk|GyoEcoE'
    ).save()
    newline = 'kwh}BxuzuRndKmEyoEcoE'

    res = client.put('/v1/testing/route/{}'.format(
        route.id
    ), headers=auth, data=dict(
        polyline=newline
    ))

    assert res.status_code == 200

    body = json.loads(res.data)

    assert body['data']['polyline'] == newline

    assert body['data'] == route.reload().to_json()


def test_delete_route(client, auth):
    route = Route(
        name='myroute',
        polyline='kwh}BxuzuRpwC|uG|kFk|GyoEcoE'
    ).save()

    # delete
    res = client.delete('/v1/testing/route/{}'.format(route.id), headers=auth)

    assert res.status_code == 204

    # read
    res = client.get('/v1/testing/route/{}'.format(route.id), headers=auth)

    assert res.status_code == 404
