from flask import json
from fleety.db.models import User
from fleety.db.models.bounded import Fleet

from ..utils import make_fleet, make_device, PWD


def test_create(client, auth):
    res = client.post('/v1/testing/fleet', headers=auth, data={
        'abbr': 'GDL',
        'name': 'Guadalajara'
    })
    assert res.status_code == 201

    fleet = Fleet.get_by('abbr', 'GDL')
    assert fleet is not None
    assert fleet.abbr == 'GDL'
    assert fleet.name == 'Guadalajara'


def test_read(client, auth):
    fleet = make_fleet()

    res = client.get('/v1/testing/fleet/{}'.format(fleet.id), headers=auth)
    assert res.status_code == 200

    data = json.loads(res.data)['data']

    fleet.devices.fill()

    assert data == fleet.to_json()


def test_update(client, auth):
    fleet = make_fleet(abbr='TPF')

    res = client.put('/v1/testing/fleet/{}'.format(
        fleet.id
    ), headers=auth, data={
        'name': 'La perla tapatía'
    })
    assert res.status_code == 200

    data = json.loads(res.data)['data']

    fleet.reload()

    assert fleet.name == 'La perla tapatía'
    assert fleet.abbr == 'TPF'

    assert data == fleet.to_json()


def test_list(client, auth):
    fleet = make_fleet()

    res = client.get('/v1/testing/fleet', headers=auth)
    assert res.status_code == 200

    data = json.loads(res.data)['data'][0]

    assert data == fleet.to_json()


def test_delete(client, auth):
    fleet = make_fleet()
    dev = make_device(0, fleet)

    res = client.delete('/v1/testing/fleet/{}'.format(fleet.id), headers=auth)

    assert res.status_code == 204

    fleet = Fleet.get(fleet.id)
    assert fleet is None

    ndv = dev.fleet.get()
    assert ndv is not None

    assert ndv.abbr == 'NDV'


def test_delete_clears_permissions(client, auth):
    fleet = make_fleet()

    og = User(name='og', last_name='a', email='a@o.com', password=PWD).save()
    og.allow(fleet.permission('view'))

    assert 'testing:fleet:{}/view'.format(fleet.id) in og.get_perms()

    res = client.delete('/v1/testing/fleet/{}'.format(fleet.id), headers=auth)
    assert res.status_code == 204

    assert not 'testing:fleet:{}/view'.format(fleet.id) in og.get_perms()


def test_can_create_fleet_with_old_abbr(client, auth):
    res = client.post('/v1/testing/fleet', headers=auth, data={
        'abbr': 'PRE',
        'name': 'navy',
    })
    assert res.status_code == 201

    fleet = Fleet.get_by('abbr', 'PRE')
    res = client.delete('/v1/testing/fleet/{}'.format(fleet.id), headers=auth)
    assert res.status_code == 204

    fleet = Fleet.get_by('abbr', 'PRE')
    assert fleet is None
    return

    res = client.post('/v1/testing/fleet', headers=auth, data={
        'abbr': 'PRE',
        'name': 'new navy',
    })
    assert res.status_code == 201

    data = json.loads(res.data)['data']

    assert data['abbr'] == 'PRE'
    assert data['name'] == 'new navy'


def test_add_devices(client, auth):
    old_fleet = make_fleet()
    new_fleet = make_fleet()
    dev = make_device(0, old_fleet)

    res = client.put('/v1/testing/device/{}'.format(
        dev.id
    ), headers=auth, data={
        'fleet': new_fleet.id,
    })

    assert res.status_code == 200


def test_list_devices(client, auth):
    fleet = make_fleet()
    dev = make_device(0, fleet)

    res = client.get('/v1/testing/fleet/{}?embed=devices'.format(
        fleet.id
    ), headers=auth)

    assert res.status_code == 200

    data = json.loads(res.data)['data']['devices']

    assert len(data) == 1

    assert data[0] == dev.to_json()


def test_move_devices(client, auth):
    gdl = make_fleet(name='Guadalajara', abbr='GDL')
    qro = make_fleet(name='Queretaro', abbr='QRO')
    dev = make_device(0, gdl)

    res = client.put('/v1/testing/device/{}'.format(
        dev.id
    ), headers=auth, data={
        'fleet': gdl.id,
    })
    assert res.status_code == 200

    res = client.get('/v1/testing/fleet/{}?embed=devices'.format(
        gdl.id
    ), headers=auth)
    data = json.loads(res.data)['data']['devices']
    assert len(data) == 1

    res = client.put('/v1/testing/device/{}'.format(
        dev.id
    ), headers=auth, data={
        'fleet': qro.id,
    })
    assert res.status_code == 200

    res = client.get('/v1/testing/fleet/{}?embed=devices'.format(
        gdl.id
    ), headers=auth)
    data = json.loads(res.data)['data']['devices']
    assert len(data) == 0
