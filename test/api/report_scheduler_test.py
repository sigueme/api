from datetime import datetime

from fleety.db.models.sql import ReportScheduler
from fleety.helpers.dates import get_iso

from ..utils import make_org, make_admin, make_report_scheduler
from ..utils import assert_response


def test_list(client):
    ''' tests that listing only show scheduled reports for my user '''

    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)

    make_report_scheduler(org_subdomain='testing', user_id='c')
    rs1 = make_report_scheduler(org_subdomain='testing', user_id=admin.id)
    make_report_scheduler(org_subdomain='other', user_id='b')
    make_report_scheduler(org_subdomain='other', user_id='a')

    assert_response(client, 'get', '/v1/testing/report_scheduler', {
        'data': [
            rs1.to_json(),
        ],
    }, org=org, admin=admin)


def test_create(client):
    run_at = datetime.now()

    assert_response(client, 'post', '/v1/testing/report_scheduler', {
        'data': {
            '_type': 'testing:report_scheduler',
            'active': True,
            'builder': 'SamplePDFBuilder',
            'cron': None,
            'id': 1,
            'last_run_at': None,
            'name': 'My first scheduled report',
            'params': {
                'var': 'log',
            },
            'run_at': get_iso(run_at),
        },
    }, data={
        'builder': 'SamplePDFBuilder',
        'name': 'My first scheduled report',
        'run_at': get_iso(run_at),
        'param_var': 'log',
    }, status_code=201, ignore_fields=['created_at'])


def test_create_cron(client):
    assert_response(client, 'post', '/v1/testing/report_scheduler', {
        'data': {
            '_type': 'testing:report_scheduler',
            'active': True,
            'builder': 'SamplePDFBuilder',
            'cron': '* * * * *',
            'id': 1,
            'last_run_at': None,
            'name': 'My first scheduled report',
            'params': {
                'var': 'log',
            },
            'run_at': None,
        },
    }, data={
        'builder': 'SamplePDFBuilder',
        'name': 'My first scheduled report',
        'cron': '* * * * *',
        'param_var': 'log',
    }, status_code=201, ignore_fields=['created_at'])


def test_create_requires_only_one_scheduler(client):
    ''' you cannot specify both run_at and cron '''
    assert_response(client, 'post', '/v1/testing/report_scheduler', {
        'errors': [
            {
                'detail': 'Must have exactly one of run_at or cron fields',
                'field': 'run_at',
                'i18n': 'errors.run_at.conflicting_schedule',
            },
        ],
    }, data={
        'builder': 'SamplePDFBuilder',
        'name': 'My first scheduled report',
        'run_at': get_iso(datetime.now()),
        'cron': '* * * * *',
    }, status_code=400)


def test_create_validates_cron(client):
    assert_response(client, 'post', '/v1/testing/report_scheduler', {
        'errors': [
            {
                'detail': 'cron is not valid',
                'field': 'cron',
                'i18n': 'errors.cron.invalid',
            },
        ],
    }, data={
        'builder': 'SamplePDFBuilder',
        'name': 'My first scheduled report',
        'cron': 'an invalid cron',
    }, status_code=400)


def test_update(client):
    # Check state before request
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)
    make_report_scheduler(
        user_id=admin.id,
        cron='1 2 3 4 5',
        name='old name 1',
    )

    old_rs = ReportScheduler.query.get(1)
    assert old_rs.active
    assert old_rs.cron == '1 2 3 4 5'
    assert old_rs.name == 'old name 1'
    assert old_rs.run_at is None

    # set new values via api request
    new_date = datetime(2018, 1, 15, 15, 57)

    assert_response(client, 'put', '/v1/testing/report_scheduler/1', {
        'data': {
            '_type': 'testing:report_scheduler',
            'id': 1,
            'active': False,
            'builder': 'SamplePDFBuilder',
            'cron': None,
            'run_at': get_iso(new_date),
            'last_run_at': None,
            'name': 'updated name',
            'params': {
                'baz': 'sos',
            },
        },
    }, data={
        'run_at': get_iso(new_date),
        'builder': 'AnotherBuilder',
        'active': False,
        'name': 'updated name',
        'param_baz': 'sos',
    }, ignore_fields=['created_at'])

    # Check new values in database
    updated_rs = ReportScheduler.query.get(1)
    assert not updated_rs.active
    assert updated_rs.cron is None
    assert updated_rs.run_at == new_date
    assert updated_rs.name == 'updated name'
    assert updated_rs.builder == 'SamplePDFBuilder'


def test_read(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)
    rs = make_report_scheduler(
        user_id=admin.id,
    )

    assert_response(client, 'get', '/v1/testing/report_scheduler/1', {
        'data': rs.to_json(),
    })


def test_delete(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)
    make_report_scheduler(
        user_id=admin.id,
    )

    assert ReportScheduler.query.get(1) is not None

    assert_response(client, 'delete', '/v1/testing/report_scheduler/1', {
        'data': None,
    }, status_code=204)

    assert ReportScheduler.query.get(1) is None
