from fleety.helpers.dates import get_iso

from ..utils import make_date, make_log, assert_response


def test_list(client):
    log0 = make_log(0, org_subdomain='testing')
    make_log(1, org_subdomain='otherorg')

    assert_response(client, 'get', '/v1/testing/log', {
        'data': [log0.to_json()],
    })


def test_filter_event_eq(client):
    log0 = make_log(0, event='ev1')
    make_log(1, event='ev2')

    assert_response(client, 'get', '/v1/testing/log?event={}'.format('ev1'), {
        'data': [
            log0.to_json(),
        ],
    })


def test_filter_event_in(client):
    log1 = make_log(1, event='ev1')
    log2 = make_log(2, event='ev2')
    make_log(3, event='ev3')

    assert_response(client, 'get', '/v1/testing/log?event__in={},{}'.format(
        'ev1', 'ev2'
    ), {
        'data': [
            log2.to_json(),
            log1.to_json(),
        ],
    })


def test_filter_created_at_between(client):
    make_log(1)
    log2 = make_log(2)
    log3 = make_log(3)
    log4 = make_log(4)
    make_log(5)

    assert_response(
        client, 'get', '/v1/testing/log?created_at__between={},{}'.format(
            get_iso(make_date(2)), get_iso(make_date(4))
        ), {
            'data': [
                log4.to_json(),
                log3.to_json(),
                log2.to_json(),
            ],
        })


def test_filter_created_at_lt(client):
    log1 = make_log(1)
    make_log(2)

    assert_response(client, 'get', '/v1/testing/log?created_at__lt={}'.format(
        get_iso(make_date(2))
    ), {
        'data': [
            log1.to_json(),
        ],
    })


def test_filter_channel_eq(client):
    log1 = make_log(1, channel='a')
    make_log(2, channel='b')

    assert_response(client, 'get', '/v1/testing/log?channel={}'.format('a'), {
        'data': [
            log1.to_json(),
        ],
    })


def test_filter_channel_startswith(client):
    log1 = make_log(1, channel='aba')
    make_log(2, channel='oba')

    assert_response(
        client, 'get', '/v1/testing/log?channel__startswith={}'.format('a'), {
            'data': [
                log1.to_json(),
            ],
        })


def test_filter_works_with_embed_fields(client):
    make_log(1)

    assert_response(client, 'get', '/v1/testing/log?embed=agent&fields=id', {
        'data': [
            {
                'id': 1,
            },
        ],
    })


def test_limit(client):
    make_log(1)
    log2 = make_log(2)

    assert_response(client, 'get', '/v1/testing/log?limit=1', {
        'data': [
            log2.to_json(),
        ],
    })


def test_order_by_channel_default(client):
    log1 = make_log(1, channel='a')
    log2 = make_log(2, channel='b')

    assert_response(client, 'get', '/v1/testing/log?order_by=channel', {
        'data': [
            log1.to_json(),
            log2.to_json(),
        ],
    })


def test_order_by_channel_asc(client):
    log1 = make_log(1, channel='a')
    log2 = make_log(2, channel='b')

    assert_response(client, 'get', '/v1/testing/log?order_by=channel.asc', {
        'data': [
            log1.to_json(),
            log2.to_json(),
        ],
    })


def test_order_by_channel_desc(client):
    log1 = make_log(1, channel='a')
    log2 = make_log(2, channel='b')

    assert_response(client, 'get', '/v1/testing/log?order_by=channel.desc', {
        'data': [
            log2.to_json(),
            log1.to_json(),
        ],
    })


def test_count(client):
    make_log(1)
    make_log(2)
    make_log(3)

    assert_response(client, 'get', '/v1/testing/log?count=1', {
        'data': 3,
    })
