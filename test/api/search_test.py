from flask import json
from fleety.db.models import Organization, User
from fleety.db.models.bounded import Device

from ..utils import make_fleet, make_device, make_org, make_admin, make_auth


def test_search(client, auth):
    fleet = make_fleet()
    device = make_device(0, fleet)

    res = client.get('/v1/testing/search?q=0', headers=auth)
    assert res.status_code == 200

    dev_json = device.to_json()
    dev_json['_searchable'] = '00000 dev0'

    assert json.loads(res.data) == {
        'data': [
            dev_json,
        ],
    }


def test_search_with_null_values(client, auth):
    Device().save()

    res = client.get('/v1/testing/search?q=my', headers=auth)
    assert res.status_code == 200


def test_search_admin_restricts_org(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)
    auth = make_auth(admin)

    otherorg = Organization().save()
    otherorg.users.add(User(
        name='admidelfonso',
    ).save())

    assert otherorg.users.count(), 1

    res = client.get('/v1/testing/search?q=adm', headers=auth)
    assert res.status_code == 200

    data = admin.to_json()
    data['_searchable'] = 'admin testing.org admin@testing.org'

    assert json.loads(res.data) == {
        'data': [
            data,
        ],
    }
