from datetime import datetime

from fleety.helpers.dates import get_iso

from ..utils import make_admin, make_org, assert_response


def test_update_last_read(client):
    olddate = datetime(2017, 12, 19, 5)
    newdate = datetime(2017, 12, 19, 10, 5)

    org = make_org('testing')
    admin = make_admin('admin@testing.org', org, last_read=olddate)

    assert_response(client, 'put', '/v1/testing/me/last_read', {
        'data': get_iso(newdate),
    }, data={
        'last_read': get_iso(newdate),
    }, org=org, admin=admin)

    assert admin.reload().last_read == newdate


def test_update_last_read_no_backwards(client):
    olddate = datetime(2018, 1, 1)
    newdate = datetime(2017, 12, 19, 10, 5)

    org = make_org('testing')
    admin = make_admin('admin@testing.org', org, last_read=olddate)

    assert_response(client, 'put', '/v1/testing/me/last_read', {
        'data': get_iso(olddate),
    }, data={
        'last_read': get_iso(newdate),
    }, org=org, admin=admin)

    assert admin.reload().last_read == olddate
