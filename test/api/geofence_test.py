from fleety.db.models import User

from ..utils import assert_response, make_geofence, PWD


def test_list(client):
    geofence = make_geofence()

    assert_response(client, 'get', '/v1/testing/geofence', {
        'data': [geofence.to_json()],
    })


def test_delete_clears_permissions(client, auth):
    geofence = make_geofence()

    og = User(name='og', last_name='a', email='a@o.com', password=PWD).save()
    og.allow(geofence.permission('view'))

    assert 'testing:geofence:{}/view'.format(geofence.id) in og.get_perms()

    res = client.delete('/v1/testing/geofence/{}'.format(
        geofence.id
    ), headers=auth)
    assert res.status_code == 204

    assert not 'testing:geofence:{}/view'.format(geofence.id) in og.get_perms()
