from flask import json

from fleety import eng
from fleety.db.models import Organization, User
from fleety.db.models.bounded import Fleet

from ..utils import make_org, make_admin, make_auth, PWD


def test_create_user(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)
    auth = make_auth(admin)

    res = client.post('/v1/testing/user', headers=auth, data=dict(
        name='og',
        last_name='astorga',
        email='oastorga@tracsa.com.mx',
        password='test'
    ))

    assert res.status_code == 201

    og = json.loads(res.data)['data']

    assert og['name'] == 'og'

    og = User.get(og['id'])
    testing = Organization.get_by('subdomain', 'testing')

    assert admin in testing.users
    assert og in testing.users


def test_create_user_makes_no_god(client, auth):
    res = client.post('/v1/testing/user', headers=auth, data=dict(
        name='og',
        last_name='astorga',
        email='oastorga@tracsa.com.mx',
        password='test',
        is_god='1',
    ))
    assert res.status_code == 201

    og = User.get_by('email', 'oastorga@tracsa.com.mx')

    assert not og.is_god


def test_list_user(client, auth):
    res = client.post('/v1/testing/user', headers=auth, data=dict(
        name='og',
        last_name='astorga',
        email='oastorga@tracsa.com.mx',
        password='test'
    ))
    assert res.status_code == 201

    res = client.get('/v1/testing/user', headers=auth)
    body = json.loads(res.data)
    assert len(body['data']) == 2


def test_read_user(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)
    auth = make_auth(admin)

    res = client.get('/v1/testing/user/{}'.format(admin.id), headers=auth)
    body = json.loads(res.data)
    assert res.status_code == 200
    assert body['data']['id'] == admin.id
    assert 'api_key' not in body['data']


def test_update_user(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)
    auth = make_auth(admin)

    newname = 'Pedro'
    newlast_name = 'Díaz'

    res = client.put('/v1/testing/user/{}'.format(
        admin.id
    ), headers=auth, data=dict(
        name=newname
    ))
    body = json.loads(res.data)
    assert res.status_code == 200
    assert body['data']['name'] == newname
    assert body['data']['last_name'] == admin.last_name

    res = client.put('/v1/testing/user/{}'.format(
        admin.id
    ), headers=auth, data=dict(
        last_name=newlast_name
    ))
    body = json.loads(res.data)
    assert res.status_code == 200
    assert body['data']['name'] == newname
    assert body['data']['last_name'] == newlast_name


def test_update_user_makes_no_god(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)
    auth = make_auth(admin)

    res = client.put('/v1/testing/user/{}'.format(
        admin.id
    ), headers=auth, data=dict(
        is_god='1',
    ))

    assert res.status_code == 200

    admin.reload()

    assert not admin.is_god


def test_change_password(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)
    auth = make_auth(admin)

    client.put('/v1/testing/user/{}'.format(admin.id), headers=auth, data=dict(
        password='qwerty'
    ))

    res = client.post('/auth/login', data=dict(
        email=admin.email,
        password='qwerty',
        org_subdomain=org.subdomain,
    ))
    body = json.loads(res.data)

    assert res.status_code == 200
    assert body['data']['session']['id'] == admin.id


def test_delete(client, auth):
    res = client.post('/v1/testing/user', headers=auth, data=dict(
        name='og',
        last_name='astorga',
        email='oastorga@tracsa.com.mx',
        password='test'
    ))
    assert res.status_code == 201

    og = json.loads(res.data)['data']

    res = client.get('/v1/testing/user/{}'.format(og['id']), headers=auth)
    assert res.status_code == 200

    res = client.delete('/v1/testing/user/{}'.format(og['id']), headers=auth)
    assert res.status_code == 204

    res = client.get('/v1/testing/user/{}'.format(og['id']), headers=auth)
    assert res.status_code == 404

    res = client.get('/v1/testing/user', headers=auth)
    assert res.status_code == 200


def test_delete_fails_if_not_in_org(client, auth):
    user = User().save()

    res = client.delete('/v1/testing/user/{}'.format(user.id), headers=auth)
    assert res.status_code == 404


def test_delete_only_removes_from_org_if_in_others(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)
    auth = make_auth(admin)

    otherorg = Organization().save()
    user = User().save()
    user.orgs.set([otherorg, org])

    assert otherorg in user.orgs
    assert org in user.orgs

    res = client.delete('/v1/testing/user/{}'.format(user.id), headers=auth)
    assert res.status_code == 204

    user = User.get(user.id)

    assert user is not None
    assert otherorg in user.orgs
    assert user in otherorg.users
    assert org not in user.orgs
    assert user not in org.users


def test_list_permissions(client, auth):
    og = User(
        name='og',
        last_name='astorga',
        email='oastorga@tracsa.com.mx',
        password=PWD,
    ).save()
    fleet = Fleet(name='the fleet', abbr='TFL').save()
    og.allow('testing:fleet/view')
    og.allow(fleet.permission())

    eng.redis.sadd('user:{}:allow'.format(og.id), 'otherorg:fleet')

    res = client.get('/v1/testing/user/{}/perms'.format(og.id), headers=auth)
    assert res.status_code == 200

    data = json.loads(res.data)['data']['perms']

    assert len(data) == 2
    assert 'testing:fleet/view' in data
    assert 'testing:fleet:{}'.format(fleet.id) in data


def test_list_permissions_embed(client, auth):
    og = User(
        name='og',
        last_name='astorga',
        email='oastorga@tracsa.com.mx',
        password=PWD,
    ).save()
    fleet = Fleet(name='the fleet', abbr='TFL').save()
    og.allow('testing:fleet/view')
    og.allow(fleet.permission())
    og.allow('otherorg:fleet:afleet')

    res = client.get('/v1/testing/user/{}/perms?embed=1'.format(
        og.id
    ), headers=auth)
    assert res.status_code == 200

    data = json.loads(res.data)['data']['perms']

    assert len(data) == 2

    item1 = data[0]
    item2 = data[1]

    if item1['perm'] != 'testing:fleet/view':
        item1, item2 = item2, item1

    assert item1 == {
        'perm': 'testing:fleet/view',
        'obj': {
            '_type': 'class',
            'id': 'testing:fleet',
            'name': 'classes.fleet',
        },
    }
    assert item2 == {
        'perm': 'testing:fleet:{}'.format(fleet.id),
        'obj': fleet.to_json(),
    }


def test_list_permissions_embed_superadmin(client, auth):
    og = User(
        name='og',
        last_name='astorga',
        email='oastorga@tracsa.com.mx',
        password=PWD,
    ).save()
    og.allow('testing')

    res = client.get('/v1/testing/user/{}/perms?embed=1'.format(
        og.id
    ), headers=auth)
    assert res.status_code == 200

    data = json.loads(res.data)['data']['perms']

    assert data == [
        {
            'perm': 'testing',
            'obj': {
                'id': 'testing',
                'name': 'testing',
                'subdomain': 'testing',
                '_type': 'organization',
            },
        },
    ]


def test_list_permissions_org_view(client, auth):
    og = User(
        name='og',
        last_name='astorga',
        email='oastorga@tracsa.com.mx',
        password=PWD,
    ).save()
    og.allow('testing/view')

    res = client.get('/v1/testing/user/{}/perms?embed=1'.format(
        og.id
    ), headers=auth)
    assert res.status_code == 200

    data = json.loads(res.data)['data']['perms']

    assert data == [
        {
            'perm': 'testing/view',
            'obj': {
                'id': 'testing',
                'name': 'testing',
                'subdomain': 'testing',
                '_type': 'organization',
            },
        },
    ]


def test_add_permissions_requirements(client, auth):
    og = User(
        name='og',
        last_name='astorga',
        email='oastorga@tracsa.com.mx',
        password=PWD,
    ).save()
    Organization(
        name='tostong',
        subdomain='tostong',
    ).save()
    fleet = Fleet(name='the fleet', abbr='TFL').save()

    # requires the org to be the same
    res = client.post('/v1/testing/user/{}/perms'.format(
        og.id
    ), headers=auth, data={
        'perm': 'tostong:fleet:{}/view'.format(fleet.id),
    })
    assert res.status_code == 400

    # requires the class to exist
    res = client.post('/v1/testing/user/{}/perms'.format(
        og.id
    ), headers=auth, data={
        'perm': 'testing:cow:{}/view'.format(fleet.id),
    })
    assert res.status_code == 400

    # requires the object to exist
    res = client.post('/v1/testing/user/{}/perms'.format(
        og.id
    ), headers=auth, data={
        'perm': 'testing:fleet:9345487348568347658347568347688/view',
    })
    assert res.status_code == 400

    # requires the class to be properly prefixed
    res = client.post('/v1/testing/user/{}/perms'.format(
        og.id
    ), headers=auth, data={
        'perm': 'fleet:{}/view'.format(fleet.id),
    })
    assert res.status_code == 400

    # requires the permission to be admin or view
    res = client.post('/v1/testing/user/{}/perms'.format(
        og.id
    ), headers=auth, data={
        'perm': 'testing:fleet:{}/explode'.format(fleet.id),
    })
    assert res.status_code == 400


def test_add_permissions(client, auth):
    og = User(
        name='og',
        last_name='astorga',
        email='oastorga@tracsa.com.mx',
        password=PWD,
    ).save()
    fleet = Fleet(name='the fleet', abbr='TFL').save()

    assert not og.is_allowed(fleet.permission())

    res = client.post('/v1/testing/user/{}/perms'.format(
        og.id
    ), headers=auth, data={
        'perm': fleet.fqn() + '/view',
    })
    assert res.status_code == 200

    assert og.is_allowed(fleet.permission('view'))
    assert not og.is_allowed(fleet.permission())


def test_add_user_permission(client, auth):
    og = User(
        name='og',
        last_name='astorga',
        email='oastorga@tracsa.com.mx',
        password=PWD,
    ).save()

    assert not og.is_allowed('testing:user')

    res = client.post('/v1/testing/user/{}/perms'.format(
        og.id
    ), headers=auth, data={
        'perm': 'testing:user',
    })
    assert res.status_code == 200

    assert og.is_allowed('testing:user')
    assert eng.redis.sismember('user:{}:allow'.format(og.id), 'testing:user')


def test_revoke_permissions(client, auth):
    og = User(
        name='og',
        last_name='astorga',
        email='oastorga@tracsa.com.mx',
        password=PWD,
    ).save()
    fleet = Fleet(name='the fleet', abbr='TFL').save()

    og.allow(fleet.permission('view'))

    assert og.is_allowed(fleet.permission('view'))

    res = client.delete('/v1/testing/user/{}/perms'.format(
        og.id
    ), headers=auth, data={
        'perm': fleet.fqn() + '/view',
    })
    assert res.status_code == 200

    assert not og.is_allowed(fleet.permission('view'))


def test_downgrade_permissions(client, auth):
    og = User(
        name='og',
        last_name='astorga',
        email='oastorga@tracsa.com.mx',
        password=PWD,
    ).save()
    fleet = Fleet(name='the fleet', abbr='TFL').save()

    og.allow(fleet.permission())

    assert og.is_allowed(fleet.permission())

    res = client.put('/v1/testing/user/{}/perms'.format(
        og.id
    ), headers=auth, data={
        'perm': fleet.fqn() + '/view',
    })
    assert res.status_code == 200

    assert not og.is_allowed(fleet.permission())
    assert og.is_allowed(fleet.permission('view'))


def test_upgrade_permissions(client, auth):
    og = User(
        name='og',
        last_name='astorga',
        email='oastorga@tracsa.com.mx',
        password=PWD,
    ).save()
    fleet = Fleet(name='the fleet', abbr='TFL').save()

    og.allow(fleet.permission('view'))

    assert og.is_allowed(fleet.permission('view'))
    assert not og.is_allowed(fleet.permission())

    res = client.put('/v1/testing/user/{}/perms'.format(
        og.id
    ), headers=auth, data={
        'perm': fleet.fqn(),
    })
    assert res.status_code == 200

    assert og.is_allowed(fleet.permission())
    assert og.is_allowed(fleet.permission('view'))


def test_grant_root(client, auth):
    og = User(
        name='og',
        last_name='astorga',
        email='oastorga@tracsa.com.mx',
        password=PWD,
    ).save()

    assert not og.is_allowed('testing:user')

    res = client.post('/v1/testing/user/{}/perms'.format(
        og.id
    ), headers=auth, data={
        'perm': 'testing',
    })
    assert res.status_code == 200

    assert og.is_allowed('testing:user')
    assert og.get_perms() == set(['testing'])


def test_revoke_root(client, auth):
    og = User(
        name='og',
        last_name='astorga',
        email='oastorga@tracsa.com.mx',
        password=PWD,
    ).save()
    og.allow('testing')
    assert og.is_allowed('testing:user')

    res = client.delete('/v1/testing/user/{}/perms'.format(
        og.id
    ), headers=auth, data={
        'perm': 'testing',
    })
    assert res.status_code == 200

    assert not og.is_allowed('testing:user')


def test_search_user(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)
    auth = make_auth(admin)

    og = User(
        name='Og Norberto Josué',
        last_name='Astorga Díaz',
        email='og.asdiaz@gmail.com',
        password=PWD,
    ).save()
    og.orgs.set([org])

    abraham = User(
        name='Abraham',
        last_name='Toriz',
        email='categulario@gmail.com',
        password=PWD,
    ).save()
    abraham.orgs.set([org])

    noelle = User(
        name='Noëlle',
        last_name='Laflèche',
        email='no-lafleche@gmail.com',
        password=PWD,
    ).save()
    noelle.orgs.set([org])

    kota = User(
        name='薮',
        last_name='宏太',
        email='kota.yabu@gmail.com',
        password=PWD,
    ).save()
    kota.orgs.set([org])

    res = client.get('/v1/testing/search?q=薮&context=user', headers=auth)
    body = json.loads(res.data)

    assert len(body['data']) == 1
    assert body['data'][0]['id'] == kota.id

    res = client.get(
        '/v1/testing/search?q=categulario&context=user', headers=auth
    )
    body = json.loads(res.data)

    assert body['data'][0]['id'] == abraham.id

    res = client.get('/v1/testing/search?q=Josue', headers=auth)
    body = json.loads(res.data)

    assert body['data'][0]['id'] == og.id
