from flask import json
from datetime import datetime
from coralillo import datamodel
from random import randint

from fleety import eng, hashids
from fleety.db.models.sql import Trip, sql, TripStatus

from ..utils import make_device, make_fleet, make_trip, make_org, make_admin, \
    make_auth, make_route, assert_response


def test_create(client, auth):
    fleet = make_fleet()
    device = make_device(0, fleet)

    res = client.post('/v1/testing/trip', headers=auth, data={
        'device_id': device.id,
        'scheduled_departure': '2017-07-01T09:00:00Z',
        'scheduled_arrival': '2017-07-01T10:00:00Z',
        'origin': 'Santa Mónica',
        'origin_pos': '-103.34941,20.62346',
        'destination': 'Cancún de Hidalgo',
        'destination_pos': '-103.26461,20.67647',
    })
    assert res.status_code == 201
    body = json.loads(res.data)['data']

    trip = sql.session.query(Trip).filter_by(id=body['id']).first()

    assert res.status_code == 201
    assert trip.device_id == device.id
    assert trip.org_subdomain == 'testing'
    assert body['status'] == TripStatus.SCHEDULED.name
    assert body['origin'] == 'Santa Mónica'
    assert body['origin_pos'] == {'lon': -103.34941, 'lat': 20.62346}
    assert body['destination'] == 'Cancún de Hidalgo'
    assert body['destination_pos'] == {'lon': -103.26461, 'lat': 20.67647}
    assert body['fleet_id'] == device.fleet.get().id


def test_dates_are_congruent(client, auth):
    res = client.post('/v1/testing/trip', headers=auth, data={
        'scheduled_departure': '2017-07-01T09:00:00Z',
        'scheduled_arrival': '2017-07-01T09:00:00Z',
        'origin': 'Santa Mónica',
        'origin_pos': '-100,20',
        'destination': 'Cancún',
        'destination_pos': '-100,20',
    })
    assert res.status_code == 400
    body = json.loads(res.data)

    assert body == {
        'errors': [
            {
                'detail': 'scheduled_arrival is not valid',
                'field': 'scheduled_arrival',
                'i18n': 'errors.scheduled_arrival.invalid',
            },
        ],
    }

    res = client.post('/v1/testing/trip', headers=auth, data={
        'scheduled_departure': '2017-07-01T09:00:00Z',
        'scheduled_arrival': '2017-07-01T08:00:00Z',
        'origin': 'Santa Mónica',
        'origin_pos': '-100,20',
        'destination': 'Cancún',
        'destination_pos': '-100,20',
    })
    assert res.status_code == 400
    body = json.loads(res.data)

    assert body == {
        'errors': [
            {
                'detail': 'scheduled_arrival is not valid',
                'field': 'scheduled_arrival',
                'i18n': 'errors.scheduled_arrival.invalid',
            },
        ],
    }


def test_create_requirements(client, auth):
    res = client.post('/v1/testing/trip', headers=auth)
    assert res.status_code == 400
    body = json.loads(res.data)

    assert body == {
        'errors': [
            {
                'detail': 'destination is required',
                'field': 'destination',
                'i18n': 'errors.destination.required',
            },
            {
                'detail': 'destination_pos is required',
                'field': 'destination_pos',
                'i18n': 'errors.destination_pos.required',
            },
            {
                'detail': 'origin is required',
                'field': 'origin',
                'i18n': 'errors.origin.required',
            },
            {
                'detail': 'origin_pos is required',
                'field': 'origin_pos',
                'i18n': 'errors.origin_pos.required',
            },
            {
                'detail': 'scheduled_arrival is required',
                'field': 'scheduled_arrival',
                'i18n': 'errors.scheduled_arrival.required',
            },
            {
                'detail': 'scheduled_departure is required',
                'field': 'scheduled_departure',
                'i18n': 'errors.scheduled_departure.required',
            },
        ],
    }


def test_read(client, auth):
    fleet = make_fleet()
    device = make_device(0, fleet)
    trip = make_trip(device)

    res = client.get('/v1/testing/trip/{}'.format(trip.id), headers=auth)
    body = json.loads(res.data)['data']

    assert res.status_code == 200
    assert body['id'] == trip.id
    assert body['device']['id'] == device.id


def test_read_notfound(client, auth):
    res = client.get(
        '/v1/testing/trip/{}'.format(randint(10, 100)), headers=auth
    )

    assert res.status_code == 404


def test_update(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)
    auth = make_auth(admin)
    route = make_route()
    fleet = make_fleet()
    device = make_device(0, fleet)

    trip = Trip(
        org_subdomain='testing',
    )
    sql.session.add(trip)
    sql.session.commit()

    assert trip.status == TripStatus.SCHEDULED
    assert trip.origin is None
    assert trip.destination is None
    assert trip.user_id is None
    assert trip.route_id is None
    assert trip.scheduled_departure is None
    assert trip.scheduled_arrival is None
    assert trip.notes is None
    assert trip.device_id is None
    assert trip.fleet_id is None
    assert trip.departure is None
    assert trip.arrival is None
    assert trip.polyline is None

    res = client.put('/v1/testing/trip/{}'.format(
        trip.id
    ), headers=auth, data={
        'status': TripStatus.ONGOING.name,
        'origin': 'Montreal',
        'destination': 'Toronto',
        'user_id': admin.id,
        'route_id': route.id,
        'scheduled_departure': '2017-10-01T06:00:00Z',
        'scheduled_arrival': '2017-10-01T06:00:00Z',
        'notes': 'some notes',
        'device_id': device.id,
        'departure': '2017-10-01T06:00:00Z',
        'arrival': '2017-10-01T06:00:00Z',
        'polyline': 'not a polyline',
    })
    assert res.status_code == 200
    body = json.loads(res.data)['data']

    trip = Trip.query.get(body['id'])

    assert trip.status == TripStatus.SCHEDULED
    assert trip.origin == 'Montreal'
    assert trip.destination == 'Toronto'
    assert trip.user_id == admin.id
    assert trip.route_id == route.id
    assert trip.scheduled_departure == datetime(2017, 10, 1, 6)
    assert trip.scheduled_arrival == datetime(2017, 10, 1, 6)
    assert trip.notes == 'some notes'
    assert trip.device_id == device.id
    assert trip.fleet_id == device.fleet.get().id
    assert trip.departure is None
    assert trip.arrival is None
    assert trip.polyline is None


def test_update_user_id(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)
    auth = make_auth(admin)

    trip = Trip(
        org_subdomain='testing',
        status=TripStatus.SCHEDULED,
    )
    sql.session.add(trip)
    sql.session.commit()

    assert trip.user_id is None

    res = client.put('/v1/testing/trip/{}'.format(
        trip.id
    ), headers=auth, data={
        'user_id': admin.id,
    })
    assert res.status_code == 200
    body = json.loads(res.data)['data']

    trip = Trip.query.get(body['id'])

    assert trip.user_id == admin.id


def test_update_device_id(client, auth):
    fleet = make_fleet()
    device = make_device(0, fleet)

    trip = Trip(
        org_subdomain='testing',
        status=TripStatus.SCHEDULED,
    )
    sql.session.add(trip)
    sql.session.commit()

    assert trip.user_id is None
    assert trip.fleet_id is None
    assert trip.device_id is None

    res = client.put('/v1/testing/trip/{}'.format(
        trip.id
    ), headers=auth, data={
        'device_id': device.id,
    })
    assert res.status_code == 200
    body = json.loads(res.data)['data']

    trip = Trip.query.get(body['id'])

    assert trip.device_id == device.id
    assert trip.fleet_id == device.fleet.get().id


def test_delete(client, auth):
    fleet = make_fleet()
    device = make_device(0, fleet)
    trip = make_trip(device)

    res = client.delete('/v1/testing/trip/{}'.format(trip.id), headers=auth)
    assert res.status_code == 204

    count = sql.session.query(Trip).filter_by(id=trip.id).count()
    assert count == 0


def test_list_device_trips(client, auth):
    fleet = make_fleet()
    device = make_device(0, fleet)
    trip = make_trip(device)

    res = client.get('/v1/testing/device/{}/trips'.format(
        device.id
    ), headers=auth)
    assert res.status_code == 200
    trips = json.loads(res.data)['data']

    assert len(trips) == 1
    assert trips[0]['id'] == trip.id


def test_list(client, auth):
    fleet = make_fleet()
    device = make_device(0, fleet)

    trip1 = Trip(
        org_subdomain='testing',
        status=TripStatus.ONGOING.name,
        device_id=device.id,
        departure=datetime(2017, 7, 1, 9),
    )
    trip2 = Trip(
        org_subdomain='testing',
        status=TripStatus.ONGOING.name,
        device_id=device.id,
        departure=datetime(2017, 7, 1, 10),
    )
    trip3 = Trip(
        org_subdomain='testing',
        status=TripStatus.ONGOING.name,
        device_id=device.id,
        departure=datetime(2017, 7, 1, 11),
    )
    sql.session.add(trip1)
    sql.session.add(trip2)
    sql.session.add(trip3)
    sql.session.commit()

    res = client.get('/v1/testing/trip', headers=auth)
    assert res.status_code == 200

    trips = json.loads(res.data)['data']
    assert len(trips) == 3

    assert trips[0]['id'] == trip3.id
    assert trips[1]['id'] == trip2.id
    assert trips[2]['id'] == trip1.id


def test_list_with_two_orders(client, auth):
    fleet = make_fleet()
    device = make_device(0, fleet)

    sql.session.query(Trip).delete()
    trips = [
        Trip(org_subdomain='testing', status=TripStatus.SCHEDULED.name,
             device_id=device.id, departure=datetime(2017, 12, 7, 16)),
        Trip(org_subdomain='testing', status=TripStatus.ONGOING.name,
             device_id=device.id, departure=datetime(2017, 12, 7, 15)),
        Trip(org_subdomain='testing', status=TripStatus.SCHEDULED.name,
             device_id=device.id, departure=datetime(2017, 12, 7, 14)),
        Trip(org_subdomain='testing', status=TripStatus.FINISHED.name,
             device_id=device.id, departure=datetime(2017, 12, 7, 13)),
        Trip(org_subdomain='testing', status=TripStatus.ONGOING.name,
             device_id=device.id, departure=datetime(2017, 12, 7, 12)),
        Trip(org_subdomain='testing', status=TripStatus.FINISHED.name,
             device_id=device.id, departure=datetime(2017, 12, 7, 11)),
    ]
    sql.session.add_all(trips)
    sql.session.commit()

    assert_response(client, 'get', '/v1/testing/trip', {
        'data': [
            trips[0].to_json(),
            trips[2].to_json(),
            trips[1].to_json(),
            trips[4].to_json(),
            trips[3].to_json(),
            trips[5].to_json(),
        ],
    })


def test_list_with_custom_orders(client, auth):
    fleet = make_fleet()
    device = make_device(0, fleet)

    sql.session.query(Trip).delete()
    trips = [
        Trip(org_subdomain='testing', status=TripStatus.SCHEDULED.name,
             device_id=device.id, departure=datetime(2017, 12, 7, 16)),
        Trip(org_subdomain='testing', status=TripStatus.ONGOING.name,
             device_id=device.id, departure=datetime(2017, 12, 7, 15)),
        Trip(org_subdomain='testing', status=TripStatus.SCHEDULED.name,
             device_id=device.id, departure=datetime(2017, 12, 7, 14)),
        Trip(org_subdomain='testing', status=TripStatus.FINISHED.name,
             device_id=device.id, departure=datetime(2017, 12, 7, 13)),
        Trip(org_subdomain='testing', status=TripStatus.ONGOING.name,
             device_id=device.id, departure=datetime(2017, 12, 7, 12)),
        Trip(org_subdomain='testing', status=TripStatus.FINISHED.name,
             device_id=device.id, departure=datetime(2017, 12, 7, 11)),
    ]
    sql.session.add_all(trips)
    sql.session.commit()

    assert_response(
        client, 'get', '/v1/testing/trip?order_by=departure.asc,status.desc', {
            'data': [
                trips[5].to_json(),
                trips[4].to_json(),
                trips[3].to_json(),
                trips[2].to_json(),
                trips[1].to_json(),
                trips[0].to_json(),
            ],
        })


def test_schedule_trip(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)
    auth = make_auth(admin)
    fleet = make_fleet()
    device = make_device(0, fleet)
    route = make_route()

    res = client.post('/v1/testing/trip', headers=auth, data={
        'origin': 'Guadalajara, Jalisco',
        'origin_pos': '-103.31542,20.66362',
        'destination': 'Veracruz, Veracruz',
        'destination_pos': '-96.13037,19.17630',
        'user_id': admin.id,
        'device_id': device.id,
        'route_id': route.id,
        'scheduled_departure': '2017-09-01T06:00:00Z',
        'scheduled_arrival': '2017-09-01T18:00:00Z',
        'notes': 'Llamar al 44 1234 4321 al llegar al puerto',
    })
    assert res.status_code == 201
    body = json.loads(res.data)

    trip = Trip.query.get(body['data']['id'])

    assert trip.status == TripStatus.SCHEDULED
    assert trip.user_id == admin.id
    assert trip.device_id == device.id
    assert trip.route_id == route.id


def test_trip_hashid(client, auth):
    fleet = make_fleet()
    device = make_device(0, fleet)
    trip = make_trip(device)

    device.last_update = datetime(2017, 11, 7, 15, 25)
    device.last_pos = datamodel.Location(lat=20, lon=-100)
    device.save()

    res = client.get('/v1/testing/trip/{}'.format(trip.id), headers=auth)
    body = json.loads(res.data)['data']

    assert body['hashid'] == hashids.encode(trip.id)


def test_trip_start(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)
    auth = make_auth(admin)
    fleet = make_fleet()
    device = make_device(0, fleet)

    res = client.post('/v1/testing/trip', headers=auth, data={
        'origin': 'Guadalajara, Jalisco',
        'origin_pos': '-103.31542,20.66362',
        'destination': 'Veracruz, Veracruz',
        'destination_pos': '-96.13037,19.17630',
        'user_id': admin.id,
        'device_id': device.id,
        'scheduled_departure': '2017-09-01T06:00:00Z',
        'scheduled_arrival': '2017-09-01T18:00:00Z',
        'notes': 'Llamar al 44 1234 4321 al llegar al puerto',
    })
    assert res.status_code == 201
    body = json.loads(res.data)

    trip = Trip.query.get(body['data']['id'])
    res = client.put('v1/testing/trip/{}/control'.format(
        trip.id
    ), headers=auth, data={
        'status': TripStatus.ONGOING.name
    })
    assert res.status_code == 200
    body = json.loads(res.data)

    trip = Trip.query.get(body['data']['id'])
    assert trip.status == TripStatus.ONGOING


def test_trip_finish(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)
    auth = make_auth(admin)
    fleet = make_fleet()
    device = make_device(0, fleet)

    res = client.post('/v1/testing/trip', headers=auth, data={
        'origin': 'Guadalajara, Jalisco',
        'origin_pos': '-103.31542,20.66362',
        'destination': 'Veracruz, Veracruz',
        'destination_pos': '-96.13037,19.17630',
        'user_id': admin.id,
        'device_id': device.id,
        'scheduled_departure': '2017-09-01T06:00:00Z',
        'scheduled_arrival': '2017-09-01T18:00:00Z',
        'notes': 'Llamar al 44 1234 4321 al llegar al puerto',
    })
    assert res.status_code == 201
    body = json.loads(res.data)

    # start
    trip = Trip.query.get(body['data']['id'])
    res = client.put('v1/testing/trip/{}/control'.format(
        trip.id
    ), headers=auth, data={
        'status': TripStatus.ONGOING.name
    })
    assert res.status_code == 200
    trip = Trip.query.get(body['data']['id'])
    assert trip.status == TripStatus.ONGOING

    # end
    res = client.put('v1/testing/trip/{}/control'.format(
        trip.id
    ), headers=auth, data={
        'status': TripStatus.FINISHED.name
    })
    assert res.status_code == 200
    trip = Trip.query.get(body['data']['id'])
    assert trip.status == TripStatus.FINISHED


def test_trip_schedule_notify(client, auth):
    fleet = make_fleet()
    device = make_device(0, fleet)

    res = client.post('/v1/testing/trip', headers=auth, data={
        'origin': 'Guadalajara, Jalisco',
        'origin_pos': '-103.31542,20.66362',
        'destination': 'Veracruz, Veracruz',
        'destination_pos': '-96.13037,19.17630',
        'device_id': device.id,
        'scheduled_departure': '2017-09-01T06:00:00Z',
        'scheduled_arrival': '2017-09-01T18:00:00Z',
        'notify_stop': 30,
    })
    assert res.status_code == 201
    body = json.loads(res.data)['data']
    assert body['notify_stop'] == 30

    res = client.put('/v1/testing/trip/{}'.format(
        body['id']
    ), headers=auth, data={
        'origin': 'Montreal',
        'destination': 'Toronto',
    })
    assert res.status_code == 200
    body = json.loads(res.data)['data']
    assert body['notify_stop'] == 30

    res = client.put('/v1/testing/trip/{}'.format(
        body['id']
    ), headers=auth, data={
        'notify_stop': 60,
    })
    assert res.status_code == 200
    body = json.loads(res.data)['data']
    assert body['notify_stop'] == 60


def test_trip_mb_propagation(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)
    auth = make_auth(admin)
    fleet = make_fleet()
    device = make_device(0, fleet)

    pubsub = eng.redis.pubsub()
    pubsub.psubscribe('testing:fleet:*', 'testing:lost_found')

    # Schedule
    res = client.post('/v1/testing/trip', headers=auth, data={
        'origin': 'Guadalajara, Jalisco',
        'origin_pos': '-103.31542,20.66362',
        'destination': 'Veracruz, Veracruz',
        'destination_pos': '-96.13037,19.17630',
        'user_id': admin.id,
        'device_id': device.id,
        'scheduled_departure': '2017-09-01T06:00:00Z',
        'scheduled_arrival': '2017-09-01T18:00:00Z',
        'notes': 'Llamar al 44 1234 4321 al llegar al puerto',
    })
    assert res.status_code == 201
    body = json.loads(res.data)
    trip = Trip.query.get(body['data']['id'])

    # skip two subscription messages
    sub_msg = pubsub.get_message()
    assert sub_msg['type'] == 'psubscribe'
    sub_msg = pubsub.get_message()
    assert sub_msg['type'] == 'psubscribe'

    msg = pubsub.get_message()
    assert msg['channel'] == 'testing:fleet:{}'.format(fleet.id)
    msg_data = json.loads(msg['data'])
    assert msg_data['event'] == 'create'
    assert msg_data['data']['trip']['id'] == trip.id

    # Start
    res = client.put('v1/testing/trip/{}/control'.format(
        trip.id
    ), headers=auth, data={
        'status': TripStatus.ONGOING.name
    })
    assert res.status_code == 200

    msg = pubsub.get_message()
    assert msg['channel'] == 'testing:fleet:{}'.format(fleet.id)
    msg_data = json.loads(msg['data'])
    assert msg_data['event'] == 'trip-started'
    assert msg_data['data']['trip']['id'] == trip.id
    assert msg_data['data']['trip']['status'] == 'ONGOING'

    # Finish
    res = client.put('v1/testing/trip/{}/control'.format(
        trip.id
    ), headers=auth, data={
        'status': TripStatus.FINISHED.name
    })
    assert res.status_code == 200

    msg = pubsub.get_message()
    assert msg['channel'] == 'testing:fleet:{}'.format(fleet.id)
    msg_data = json.loads(msg['data'])
    assert msg_data['event'] == 'trip-finished'
    assert msg_data['data']['trip']['id'] == trip.id
    assert msg_data['data']['trip']['status'] == 'FINISHED'

    # Delete
    res = client.delete('v1/testing/trip/{}'.format(trip.id), headers=auth)
    assert res.status_code == 204

    msg = pubsub.get_message()
    assert msg['channel'] == 'testing:fleet:{}'.format(fleet.id)
    msg_data = json.loads(msg['data'])
    assert msg_data['event'] == 'delete'
    assert msg_data['data']['trip']['id'] == trip.id
