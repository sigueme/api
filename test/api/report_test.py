from datetime import datetime
from flask import json

from fleety import eng
from fleety.db.models.sql import Report

from ..utils import make_report, make_org, make_admin, assert_response


def test_list(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)

    make_report(org_subdomain='testing', user_id='c')
    r1 = make_report(org_subdomain='testing', user_id=admin.id)
    make_report(org_subdomain='other', user_id='a')
    make_report(org_subdomain='other', user_id='b')

    assert_response(client, 'get', '/v1/testing/report', {
        'data': [
            r1.to_json(),
        ],
    }, org=org, admin=admin)


def test_create(client):
    pubsub = eng.redis.pubsub()
    pubsub.psubscribe('testing:user:*')

    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)

    new_report = {
        '_type': 'testing:report',
        'id': 1,
        'name': 'My first report',
        'builder': 'SamplePDFBuilder',
        'completed_at': None,
        'params': {
            'var': 'log',
        },
        'scheduler_id': None,
        'user_id': admin.id,
        'status': 'IN_PROGRESS',
        'failed_reason': None,
        'url': None,
    }

    assert_response(client, 'post', '/v1/testing/report', {
        'data': new_report,
    }, data={
        'builder': 'SamplePDFBuilder',
        'name': 'My first report',
        'param_var': 'log',
    }, status_code=201, ignore_fields=['requested_at'], org=org, admin=admin)

    # skip two subscription messages
    sub_msg = pubsub.get_message()
    assert sub_msg['type'] == 'psubscribe'

    msg = pubsub.get_message()
    assert msg is not None, 'No broker message sent'
    assert msg['channel'] == 'testing:user:{}'.format(admin.id)

    msg_data = json.loads(msg['data'])
    assert (datetime.strptime(
        msg_data['data']['report']['requested_at'], '%Y-%m-%dT%H:%M:%SZ'
    ) - datetime.now()).total_seconds() < 1
    del msg_data['data']['report']['requested_at']

    assert msg_data == {
        'event': 'report-requested',
        'data': {
            'org_name': 'testing',
            'report': new_report,
        },
    }


def test_show(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)
    report = make_report(user_id=admin.id)

    assert_response(client, 'get', '/v1/testing/report/1', {
        'data': report.to_json(),
    }, org=org, admin=admin)


def test_delete(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)
    make_report(user_id=admin.id)

    assert_response(
        client, 'delete', '/v1/testing/report/1', {}, status_code=204
    )

    assert Report.query.get(1) is None
