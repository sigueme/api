from flask import json

from fleety.db.models import User, Subscription

from ..utils import make_admin, make_org, make_auth


def test_permissions_are_checked(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org, allow='foo')
    auth = make_auth(admin)

    res = client.post('/v1/testing/subscription', headers=auth, data={
        'channel': 'testing:var:log',
        'event': 'something-happened',
        'handler': 'Email',
        'params': '{"a":1}',
    })

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [{
            'detail': 'this user has no permissions over this channel',
            'field': 'channel',
            'i18n': 'errors.channel.forbidden_subscription',
        }],
    }


def test_handler_is_checked(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org, allow='foo')
    auth = make_auth(admin)
    admin.allow('testing:var:log')

    res = client.post('/v1/testing/subscription', headers=auth, data={
        'channel': 'testing:var:log',
        'channel_name': 'Var Log',
        'event': 'something-happened',
        'user_id': admin.id,
        'handler': 'disabled',
        'params': '{"a":1}',
    })
    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [{
            'detail': 'handler is not valid',
            'field': 'handler',
            'i18n': 'errors.handler.invalid',
        }],
    }


def test_create(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org, allow='foo')
    auth = make_auth(admin)
    admin.allow('testing:var:log')

    res = client.post('/v1/testing/subscription', headers=auth, data={
        'channel': 'testing:var:log',
        'channel_name': 'Var Log',
        'event': 'something-happened',
        'user_id': admin.id,
        'handler': 'Sms',
        'params': '{"a":"1"}',
    })

    assert res.status_code == 201

    assert 1 == admin.subscriptions.count()

    sub = admin.subscriptions.get()[0]

    assert sub.channel == 'testing:var:log'
    assert sub.channel_name == 'Var Log'
    assert sub.event == 'something-happened'
    assert sub.user.get().id == admin.id
    assert sub.handler == 'Sms'
    assert sub.params == {'a': '1'}


def test_list(client):
    otheruser = User().save()
    subs = [Subscription(channel='a').save(), Subscription(channel='a').save()]
    otheruser.subscriptions.add(subs[1])

    org = make_org('testing')
    admin = make_admin('admin@testing.org', org, allow='foo')
    auth = make_auth(admin)
    admin.subscriptions.add(subs[0])

    res = client.get('/v1/testing/subscription', headers=auth)
    assert res.status_code == 200

    data = json.loads(res.data)['data']

    assert len(data) == 2

    data.sort(key=lambda s: s['id'])
    subs.sort(key=lambda s: s.id)

    assert data[0] == subs[0].to_json()
    assert data[1] == subs[1].to_json()


def test_list_user_subscriptions(client):
    otheruser = User().save()
    sub1 = Subscription(channel='a').save()
    sub2 = Subscription(channel='a').save()
    otheruser.subscriptions.add(sub2)

    org = make_org('testing')
    admin = make_admin('admin@testing.org', org, allow='foo')
    auth = make_auth(admin)
    admin.subscriptions.add(sub1)

    res = client.get('/v1/testing/user/{}/subscriptions'.format(
        admin.id
    ), headers=auth)
    assert res.status_code == 200

    data = json.loads(res.data)['data']

    assert len(data) == 1
    assert data[0] == sub1.to_json()


def test_delete(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org, allow='foo')
    auth = make_auth(admin)

    sub1 = Subscription(channel='a').save()
    sub2 = Subscription(channel='b').save()
    admin.subscriptions.add(sub1)
    admin.subscriptions.add(sub2)

    res = client.delete('/v1/testing/subscription/{}'.format(
        sub2.id
    ), headers=auth)

    assert res.status_code == 204

    assert admin.subscriptions.count() == 1
    assert len(admin.subscriptions.get()) == 1
    assert Subscription.get(sub2.id) is None


def test_god_can_subscribe_to_meta(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org, allow='foo')
    auth = make_auth(admin)

    admin.is_god = True
    admin.save()

    res = client.post('/v1/testing/subscription', headers=auth, data={
        'channel': 'meta',
        'channel_name': 'Eventos del servidor',
        'event': '*',
        'user_id': admin.id,
        'handler': 'Sms',
        'params': '',
    })
    assert res.status_code == 201

    assert 1 == admin.subscriptions.count()

    sub = admin.subscriptions.get()[0]

    assert sub.channel == 'meta'
    assert sub.channel_name == 'Eventos del servidor'
    assert sub.event == '*'
    assert sub.user.get().id == admin.id
    assert sub.handler == 'Sms'
    assert sub.params == {}
