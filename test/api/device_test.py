from coralillo.datamodel import Location
from datetime import datetime
from flask import json
from math import radians

from fleety.db.models.bounded import Device, DeviceDynamicAttribute
from fleety.db.models.sql import Position, TraccarDevice
from fleety import sql
from fleety.helpers.dates import get_iso

from ..utils import make_date, make_fleet, make_device, assert_response


def test_list(client):
    fleet = make_fleet()
    devices = [
        make_device(0, fleet),
        make_device(1, fleet),
    ]

    assert_response(client, 'get', '/v1/testing/device', {
        'data': [
            devices[0].to_json(),
            devices[1].to_json(),
        ],
    })


def test_read(client):
    fleet = make_fleet()
    dev = make_device(0, fleet)

    res = dev.to_json()
    res['fields'] = []

    assert_response(client, 'get', '/v1/testing/device/{}'.format(dev.id), {
        'data': res,
    })


def test_read_with_fleet(client):
    fleet = make_fleet()
    dev = make_device(0, fleet)

    res = dev.to_json()
    res['fields'] = []
    res['fleet'] = fleet.to_json()

    assert_response(client, 'get', '/v1/testing/device/{}?include=*,fleet.*'.format(
        dev.id
    ), {
        'data': res,
    })


def test_update(client, auth):
    fleet = make_fleet()
    dev = make_device(0, fleet)

    res = client.put('/v1/testing/device/{}'.format(
        dev.id
    ), headers=auth, data={
        'name': 'yourdevice',
    })
    assert res.status_code == 200

    dev.reload()

    assert dev.name == 'yourdevice'


def test_delete(client):
    fleet = make_fleet()
    dev0 = make_device(0, fleet)
    dev1 = make_device(1, fleet)
    dev2 = make_device(2, fleet)

    assert_response(client, 'delete', '/v1/testing/device/{}'.format(
        dev0.id
    ), {
    }, status_code=204)

    assert Device.get(dev0.id) is None

    assert Device.get(dev1.id) is not None
    assert Device.get(dev2.id) is not None


def test_update_fleet(client, auth):
    prev_fleet = make_fleet()
    new_fleet = make_fleet()

    dev = make_device(0, prev_fleet)

    assert dev in prev_fleet.devices
    assert dev not in new_fleet.devices

    res = client.put('/v1/testing/device/{}'.format(
        dev.id
    ), headers=auth, data={
        'fleet': new_fleet.id,
    })
    assert res.status_code == 200

    assert dev in new_fleet.devices
    assert dev not in prev_fleet.devices


def test_create_device_dynamic_field(client, auth):
    res = client.post(
        '/v1/testing/device_dynamic_attribute', headers=auth, data={
            'label': 'Marca'
        })

    assert res.status_code == 201

    body = json.loads(res.data)
    assert body['data']['label'] == 'Marca'


def test_update_device_dynamic_field(client, auth):
    brand_field = DeviceDynamicAttribute(
        label='marca'
    ).save()

    client.put('/v1/testing/device_dynamic_attribute/{}'.format(
        brand_field.id
    ), headers=auth, data={
        'label': 'Marca'
    })

    loaded_model = DeviceDynamicAttribute.get(brand_field.id)
    assert loaded_model.label == 'Marca'


def test_delete_device_dynamic_field(client, auth):
    brand_field = DeviceDynamicAttribute(
        label='marca'
    ).save()

    client.delete('/v1/testing/device_dynamic_attribute/{}'.format(
        brand_field.id
    ), headers=auth)

    loaded_model = DeviceDynamicAttribute.get(brand_field.id)
    assert loaded_model is None


def test_update_with_dynamic_fields(client, auth):
    fleet = make_fleet()
    dev = make_device(0, fleet)

    brand_field = DeviceDynamicAttribute(
        label='Marca'
    ).save()

    model_field = DeviceDynamicAttribute(
        label='Modelo'
    ).save()

    year_field = DeviceDynamicAttribute(
        label='Año'
    ).save()

    res = client.put('/v1/testing/device/{}'.format(
        dev.id
    ), headers=auth, data={
        'dynamic_{}'.format(brand_field.id): 'Toyota',
        'dynamic_{}'.format(model_field.id): 'Yaris',
        'dynamic_{}'.format(year_field.id): '2015',
        'dynamic_{}'.format('fakefieldid'): '2015',
    })

    body = json.loads(res.data)

    assert body['data']['dynamic'] == {
        brand_field.id: 'Toyota',
        model_field.id: 'Yaris',
        year_field.id: '2015',
    }

    assert len(body['data']['fields']) == 3


def test_get_device_history(client):
    fleet = make_fleet()
    dev = make_device(1, fleet)

    sql.session.add(TraccarDevice(id=1, name='dev1', uniqueid='1234567800001'))
    sql.session.add(TraccarDevice(id=2, name='dev2', uniqueid='1234567800002'))
    sql.session.commit()

    time = datetime(2018, 1, 10, 11, 39)

    sql.session.add(Position(
        deviceid=1, latitude=20, longitude=-100, speed=24.29804,
        servertime=time, fixtime=time, devicetime=time, valid=True,
        altitude=0, course=0
    ))
    sql.session.add(Position(
        deviceid=2, latitude=20, longitude=-100, speed=24.29804,
        servertime=time, fixtime=time, devicetime=time, valid=True,
        altitude=0, course=0
    ))
    sql.session.commit()

    assert_response(
        client,
        'get',
        '/v1/testing/device/{}/location_history'.format(dev.id), {
            'data': [{
                'id': 1,
                'lat': 20.0,
                'lon': -100.0,
                'server_time': get_iso(time),
                'device_time': get_iso(time),
                'speed': 44.99999437804,
                '_type': 'position',
            }],
        })


def test_get_devices_locations_by_daterange(client, auth):
    fleet = make_fleet()
    dev = make_device(1, fleet)

    sql.session.add(TraccarDevice(id=1, name='dev1', uniqueid='1234567800001'))
    sql.session.add(TraccarDevice(id=2, name='dev2', uniqueid='1234567800002'))
    sql.session.commit()

    sql.session.add(Position(
        deviceid=1, latitude=20, longitude=-100, devicetime=make_date(0),
        servertime=make_date(0), fixtime=make_date(0), valid=True, altitude=0,
        course=0, speed=0
    ))
    sql.session.add(Position(
        deviceid=2, latitude=20, longitude=-100, devicetime=make_date(1),
        servertime=make_date(1), fixtime=make_date(1), valid=True, altitude=0,
        course=0, speed=0
    ))
    sql.session.add(Position(
        deviceid=1, latitude=21, longitude=-100, devicetime=make_date(2),
        servertime=make_date(2), fixtime=make_date(2), valid=True, altitude=0,
        course=0, speed=0
    ))
    sql.session.add(Position(
        deviceid=2, latitude=21, longitude=-100, devicetime=make_date(3),
        servertime=make_date(3), fixtime=make_date(3), valid=True, altitude=0,
        course=0, speed=0
    ))
    sql.session.add(Position(
        deviceid=1, latitude=22, longitude=-100, devicetime=make_date(4),
        servertime=make_date(4), fixtime=make_date(4), valid=True, altitude=0,
        course=0, speed=0
    ))
    sql.session.add(Position(
        deviceid=2, latitude=22, longitude=-100, devicetime=make_date(5),
        servertime=make_date(5), fixtime=make_date(5), valid=True, altitude=0,
        course=0, speed=0
    ))
    sql.session.add(Position(
        deviceid=1, latitude=23, longitude=-100, devicetime=make_date(6),
        servertime=make_date(6), fixtime=make_date(6), valid=True, altitude=0,
        course=0, speed=0
    ))
    sql.session.commit()

    res = client.get(
        '/v1/testing/device/{}/location_history?devicetime={}%2B240'.format(
            dev.id, int(make_date(1).timestamp())
        ), headers=auth)

    assert res.status_code == 200

    data = json.loads(res.data)['data']

    assert len(data) == 2

    for i, item in enumerate(data):
        assert item['lon'] == -100
        assert item['lat'] == 20 + i + 1


def test_find_available_codes(client, auth):
    fleet = make_fleet()
    make_device(1, fleet)

    res = client.get('/v1/testing/device/freecode?current=00001', headers=auth)

    assert res.status_code == 200
    assert json.loads(res.data) == {
        'data': '00002',
    }


def test_filter_traccar_id(client):
    fleet = make_fleet()
    make_device(1, fleet)
    make_device(2, fleet)
    dev3 = make_device(3, fleet)

    assert_response(client, 'get', '/v1/testing/device?traccar_id=3', {
        'data': [
            dev3.to_json(),
        ],
    })


def test_filter_name(client):
    fleet = make_fleet()
    make_device(1, fleet)
    make_device(2, fleet)
    dev3 = make_device(3, fleet)

    assert_response(client, 'get', '/v1/testing/device?name__endswith=3', {
        'data': [
            dev3.to_json(),
        ],
    })


def test_filter_contains(client):
    fleet = make_fleet()
    make_device(1, fleet)
    make_device(2, fleet)
    dev3 = make_device(3, fleet)

    assert_response(client, 'get', '/v1/testing/device?name__contains=3', {
        'data': [
            dev3.to_json(),
        ],
    })


def test_filter_created_at(client):
    fleet = make_fleet()
    make_device(1, fleet)
    dev3 = make_device(3, fleet)
    make_device(5, fleet)

    assert_response(
        client, 'get', '/v1/testing/device?created_at__between={},{}'.format(
            get_iso(make_date(2)), get_iso(make_date(4))
        ), {
            'data': [
                dev3.to_json(),
            ],
        })


def test_filter_last_pos(client):
    fleet = make_fleet()

    make_device(0, fleet, last_pos=Location(-103.37894, 20.68161))
    make_device(1, fleet, last_pos=Location(-103.41499, 20.74905))
    make_device(2, fleet, last_pos=Location(-103.43353, 20.62251))
    dev3 = make_device(3, fleet, last_pos=Location(
        -103.3346477150917, 20.714371192326006
    ))
    dev4 = make_device(4, fleet, last_pos=Location(
        -103.31886023283005, 20.69446095761939
    ))
    make_device(5, fleet, last_pos=Location(-103.31199, 20.62314))

    assert_response(
        client, 'get',
        '/v1/testing/device?last_pos__radius=-103.32469,20.70859,2km', {
            'data': [
                dev3.to_json(),
                dev4.to_json(),
            ],
        })


def test_filter_last_speed(client):
    fleet = make_fleet()

    make_device(0, fleet, last_speed=10)
    make_device(1, fleet, last_speed=10)
    dev2 = make_device(2, fleet, last_speed=40)
    make_device(3, fleet, last_speed=10)
    dev4 = make_device(4, fleet, last_speed=40)
    make_device(5, fleet, last_speed=10)

    assert_response(
        client, 'get', '/v1/testing/device?last_speed__between=30,50', {
            'data': [
                dev2.to_json(),
                dev4.to_json(),
            ],
        })


def test_filter_last_course(client):
    fleet = make_fleet()

    make_device(0, fleet, last_course=radians(1))
    make_device(1, fleet, last_course=radians(45))
    make_device(2, fleet, last_course=radians(80))
    dev3 = make_device(3, fleet, last_course=radians(115))
    make_device(4, fleet, last_course=radians(190))
    make_device(5, fleet, last_course=radians(200))

    assert_response(
        client, 'get', '/v1/testing/device?last_course__heading=SE', {
            'data': [
                dev3.to_json(),
            ],
        })


def test_filter_fleet(client):
    fleet = make_fleet()
    other_fleet = make_fleet()

    dev0 = make_device(0, fleet)
    make_device(1, other_fleet)

    assert_response(client, 'get', '/v1/testing/device?fleet={}'.format(
        fleet.id
    ), {
        'data': [
            dev0.to_json(),
        ],
    })


def test_filter_status(client):
    fleet = make_fleet()

    make_device(0, fleet, status='stopped')
    make_device(1, fleet, status='stopped')
    dev2 = make_device(2, fleet, status='moving')
    make_device(3, fleet, status='stopped')
    dev4 = make_device(4, fleet, status='alarm')
    make_device(5, fleet, status='stopped')

    assert_response(
        client, 'get', '/v1/testing/device?status__in=moving,alarm', {
            'data': [
                dev2.to_json(),
                dev4.to_json(),
            ],
        })
