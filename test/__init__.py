import pytest

# This allows introspection to work in helper functions with asserts
# https://stackoverflow.com/questions/41522767/pytest-assert-introspection-in-helper-function
# https://docs.pytest.org/en/latest/writing_plugins.html#assertion-rewriting
pytest.register_assert_rewrite('test.utils')
