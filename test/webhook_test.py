from coralillo.datamodel import Location
from datetime import datetime
from flask import json
import time

from fleety import eng, app
from fleety.db.models.bounded import Device, Fleet

from .utils import make_org


def test_create_device(client, pubsub):
    pubsub.psubscribe('testing:fleet:*', 'testing:device:*')
    messages = pubsub.listen()
    org = make_org('testing')

    res = client.post('/webhook/{}/traccar/event'.format(
        app.config['WEBHOOK_KEY']
    ), data=json.dumps({
        'event': {
            "type": "deviceMoving",
            "serverTime": "2017-08-14T15:52:00.401+0000",
        },
        'device': {
            "id": 1,
            "uniqueId": org.code + "01234",
            "status": "online",
            "lastUpdate": "2017-08-14T15:52:01.401+0000",
        },
        'position': {
            'altitude': 1400,
            'course': 137,
            'deviceTime': '2017-08-14T15:52:02.401+0000',
            'fixTime': '2017-08-14T15:52:03.401+0000',
            'latitude': 20,
            'longitude': -100,
            'outdated': False,
            'protocol': 'pt502',
            'serverTime': '2017-08-14T15:52:04.401+0000',
            'speed': 15,  # in knots
            'valid': True,
        },
    }))

    assert res.status_code == 200

    newdevice = Device.get_by('code', '01234')

    assert newdevice is not None, 'Device was not created'

    newdevicefleet = newdevice.fleet.get()
    assert newdevicefleet is not None
    assert newdevicefleet.name == 'Nuevos dispositivos'
    assert newdevicefleet.abbr == 'NDV'
    assert newdevicefleet.created_at == datetime(2017, 8, 14, 15, 52)
    assert newdevicefleet.updated_at == datetime(2017, 8, 14, 15, 52)

    assert eng.redis.sismember('testing:device:members', newdevice.id)
    assert len(newdevice.id) == 12
    assert len(newdevicefleet.id) == 12
    assert eng.redis.sismember('testing:fleet:members', newdevicefleet.id)
    assert eng.redis.hget(
        'testing:fleet:index_abbr', newdevicefleet.abbr
    ) == newdevicefleet.id

    assert newdevice is not None

    assert newdevice.name == 'New device'
    assert newdevice.code == '01234'
    assert newdevice.traccar_id == 1
    assert newdevice.created_at == datetime.strptime(
        '2017-08-14T15:52:00', '%Y-%m-%dT%H:%M:%S'
    )
    assert newdevice.updated_at == datetime.strptime(
        '2017-08-14T15:52:00', '%Y-%m-%dT%H:%M:%S'
    )
    assert newdevice.last_pos == Location(lon=-100, lat=20)
    assert newdevice.status == 'moving'

    expected_event = {
        'event': 'device-update',
        'data': {
            '_type': 'testing:device',
            'id': newdevice.id,
            'last_pos': {
                'lat': 20,
                'lon': -100,
            },
            'last_update': '2017-08-14T15:52:02Z',
            'last_speed': 0,
            'last_course': 0,
            'status': 'moving',
            'name': 'New device',
        },
    }

    fleet_event = next(messages)
    assert fleet_event is not None
    assert fleet_event['channel'] == newdevicefleet.fqn()
    assert json.loads(fleet_event['data']) == expected_event


def test_outdated_position(client, pubsub):
    pubsub.psubscribe('testing:fleet:*', 'testing:device:*')
    messages = pubsub.listen()
    org = make_org('testing')
    fleet = Fleet(
        name='the best fleet',
        abbr='a fleet',
    ).save()
    device = Device(
        name='olddevice',
        code='04444',
        traccar_id=4,
        last_pos=Location(-100, 20),
        created_at=datetime(2017, 8, 14, 0, 0, 0),
        updated_at=datetime(2017, 8, 14, 0, 0, 0),
    ).save()
    device.fleet.set(fleet)

    res = client.post('/webhook/{}/traccar/event'.format(
        app.config['WEBHOOK_KEY']
    ), data=json.dumps({
        'event': {
            "type": "deviceMoving",
            "serverTime": "2017-08-13T00:00:00.000+0000",
        },
        'device': {
            'id': 4,
            "uniqueId": org.code + "04444",
            "status": "online",
            "lastUpdate": "2017-08-14T00:00:00.000+0000",
        },
        'position': {
            'altitude': 1400,
            'course': 137,
            'deviceTime': '2017-08-13T00:00:00.000+0000',
            'fixTime': '2017-08-13T00:00:00.000+0000',
            'latitude': 19,
            'longitude': -97,
            'outdated': True,
            'protocol': 'pt502',
            'serverTime': '2017-08-13T00:00:00.000+0000',
            'speed': 15,  # in knots
            'valid': True,
        },
    }))

    assert res.status_code == 200

    device.reload()

    assert device.created_at == datetime(2017, 8, 14, 0, 0, 0)
    assert device.updated_at == datetime(2017, 8, 14, 0, 0, 0)
    assert device.last_pos == Location(lon=-100, lat=20)

    assert eng.redis.sismember('testing:device:members', device.id)

    pubsub.get_message()  # Consume the subscribe message
    message = next(messages)

    assert message is not None
    assert json.loads(message['data'])['event'] == 'device-update'


def test_update_device_position(client, pubsub):
    pubsub.psubscribe('testing:fleet:*', 'testing:device:*')
    messages = pubsub.listen()
    org = make_org('testing')
    old_position = Location(-103.30375671386719, 20.639531429485633)

    fleet = Fleet(abbr='NDV', name='the fleet').save()
    dev = Device(
        code='05555',
        traccar_id=5,
        last_pos=old_position,
        created_at=datetime(2017, 6, 23),
        updated_at=datetime(2017, 6, 23),
    ).save()

    dev.fleet.set(fleet)

    newtime = int(time.time())

    newtime = datetime(2017, 8, 23)

    res = client.post('/webhook/{}/traccar/event'.format(
        app.config['WEBHOOK_KEY']
    ), data=json.dumps({
        'event': {
            "type": "deviceMoving",
            "serverTime": newtime.strftime('%Y-%m-%dT%H:%M:%S.%f+0000'),
        },
        'device': {
            'id': 5,
            "uniqueId": org.code + "05555",
            "status": "online",
            "lastUpdate": newtime.strftime('%Y-%m-%dT%H:%M:%S.%f+0000'),
        },
        'position': {
            'altitude': 1400,
            'course': 137,
            'deviceTime': newtime.strftime('%Y-%m-%dT%H:%M:%S.%f+0000'),
            'fixTime': newtime.strftime('%Y-%m-%dT%H:%M:%S.%f+0000'),
            'latitude': 20,
            'longitude': -100,
            'outdated': False,
            'protocol': 'pt502',
            'serverTime': newtime.strftime('%Y-%m-%dT%H:%M:%S.%f+0000'),
            'speed': 15,  # in knots
            'valid': True,
        },
    }))

    assert eng.redis.sismember('testing:device:members', dev.id)

    assert res.status_code == 200

    old_device = Device.get_by('code', '05555')

    assert old_device is not None

    assert old_device.code == '05555'
    assert old_device.created_at == datetime(2017, 6, 23)
    assert old_device.updated_at == datetime(2017, 6, 23)
    assert old_device.last_update == newtime
    assert old_device.last_pos == Location(lon=-100, lat=20)

    pubsub.get_message()  # Consume the subscribe message
    message = next(messages)

    assert message is not None
    assert json.loads(message['data'])['event'] == 'device-update'


def test_first_course_is_set(client):
    org = make_org('testing')

    res = client.get(
        '/webhook/{}/traccar/position?code={}&protocol=osmand&'
        'time=1511006796000&lat=21.289472&lon=-102.256038&'
        'status=0xF11C&device_time=1511006796000&id=78'.format(
            app.config['WEBHOOK_KEY'], org.code + '00001'
        ))

    assert res.status_code == 200

    res = client.get(
        '/webhook/{}/traccar/position?code={}&protocol=osmand&'
        'time=1511007095000&lat=21.289455&lon=-102.174047&'
        'status=0xF11C&device_time=1511007095000&id=78 '.format(
            app.config['WEBHOOK_KEY'], org.code + '00001'
        ))

    assert res.status_code == 200


def test_other_positions(client):
    org = make_org('testing')

    res = client.get(
        '/webhook/{}/traccar/position?code={}&protocol=pt502&'
        'time=1511460185147&lat=20.606488333333335&'
        'lon=-103.39978166666667&status=0xF020&'
        'device_time=1511460185147&id=4'.format(
            app.config['WEBHOOK_KEY'], org.code + '00001'
        ))

    assert res.status_code == 200

    res = client.get(
        '/webhook/{}/traccar/position?code={}&protocol=pt502&'
        'time=1511460213148&lat=20.606488333333335&'
        'lon=-103.39978166666667&status=0xF020&'
        'device_time=1511460213148&id=4'.format(
            app.config['WEBHOOK_KEY'], org.code + '00001'
        ))

    assert res.status_code == 200


def test_device_joined_offline_and_unkown(client):
    org = make_org('testing')
    old_position = Location(-103.30375671386719, 20.639531429485633)
    created_at = datetime(2017, 8, 15, 0, 0, 0)
    updated_at = datetime(2017, 8, 15, 0, 0, 0)

    fleet = Fleet(
        name='the best fleet',
        abbr='a fleet',
    ).save()

    dev = Device(
        code='05555',
        traccar_id=5,
        last_pos=old_position,
        created_at=created_at,
        updated_at=updated_at,
        status='offline',
    ).save()

    dev.fleet.set(fleet)

    for event in ["deviceUnknown", "deviceOffline"]:
        serverTime = datetime(2017, 8, 15, 3, 0, 0)
        lastUpdate = datetime(2017, 8, 15, 3, 0, 0)

        res = client.post('/webhook/{}/traccar/event'.format(
            app.config['WEBHOOK_KEY']
        ), data=json.dumps({
            'event': {
                "type": event,
                "serverTime": serverTime.strftime('%Y-%m-%dT%H:%M:%S.%f+0000'),
            },
            'device': {
                'id': 5,
                "uniqueId": org.code + "05555",
                "status": event[6:].lower(),
                "lastUpdate": lastUpdate.strftime(
                    '%Y-%m-%dT%H:%M:%S.%f+0000'
                ),
            },
        }))
        assert res.status_code == 200

        dev.reload()

        assert dev.last_pos == old_position
        assert dev.created_at == datetime(2017, 8, 15)
        assert dev.updated_at == datetime(2017, 8, 15, 0)
        assert dev.last_update == datetime(2017, 8, 15, 3)
        assert dev.status == 'offline'


def test_device_status(client, pubsub):
    pubsub.psubscribe('testing:fleet:*', 'testing:device:*')
    messages = pubsub.listen()
    org = make_org('testing')
    old_position = Location(-103.30375671386719, 20.639531429485633)
    created_at = datetime(2017, 8, 15, 0, 0, 0)
    updated_at = datetime(2017, 8, 15, 0, 0, 0)

    fleet = Fleet(
        name='the best fleet',
        abbr='a fleet',
    ).save()

    dev = Device(
        code='05555',
        traccar_id=5,
        last_pos=old_position,
        created_at=created_at,
        updated_at=updated_at,
        status='offline',
    ).save()

    dev.fleet.set(fleet)

    pubsub.get_message()  # Consume the subscribe message

    for event in ["deviceOnline", "deviceUnknown", "deviceOffline",
                  "deviceMoving", "deviceStopped", "deviceOverspeed",
                  "deviceFuelDrop"]:
        serverTime = datetime(2017, 8, 15, 3, 0, 0)
        lastUpdate = datetime(2017, 8, 15, 3, 0, 0)

        res = client.post('/webhook/{}/traccar/event'.format(
            app.config['WEBHOOK_KEY']
        ), data=json.dumps({
            'event': {
                "type": event,
                "serverTime": serverTime.strftime('%Y-%m-%dT%H:%M:%S.%f+0000'),
            },
            'device': {
                'id': 5,
                "uniqueId": org.code + "05555",
                "status": event[6:].lower(),
                "lastUpdate": lastUpdate.strftime(
                    '%Y-%m-%dT%H:%M:%S.%f+0000'
                ),
            },
        }))
        assert res.status_code == 200

        dev.reload()

        assert eng.redis.sismember('testing:device:members', dev.id)

        assert dev.last_pos == old_position
        assert dev.created_at == datetime(2017, 8, 15)
        assert dev.updated_at == datetime(2017, 8, 15, 0)
        assert dev.last_update == datetime(2017, 8, 15, 3)

        expected_event_name = app.config['TRACCAR_EVENT_MAPPING'][event]

        assert dev.status == expected_event_name

        event_data = {
            'data': {
                '_type': 'testing:device',
                'status': expected_event_name,
                'id': dev.id,
            },
            'event': 'device-update',
        }

        message = next(messages)

        assert message is not None
        assert message['channel'] == dev.fleet.get().fqn()
        assert json.loads(message['data']) == event_data

        if event in ['deviceOverspeed', 'deviceFuelDrop']:
            message = next(messages)

            assert message is not None
            assert message['channel'] == dev.fleet.get().fqn()
            assert json.loads(message['data']) == {
                'data': {
                    'device': dev.to_json(),
                    'type': event,
                    'time': serverTime.strftime('%Y-%m-%dT%H:%M:%SZ'),
                    'org_name': 'testing',
                },
                'event': 'alarm',
            }


def test_offline_event(client):
    org = make_org('testing')
    fleet = Fleet(
        name='the best fleet',
        abbr='a fleet',
    ).save()

    dev = Device(
        code='05555',
        traccar_id=5,
        last_pos=Location(-103.30375671386719, 20.639531429485633),
        created_at=datetime(2017, 8, 15, 0, 0, 0),
        updated_at=datetime(2017, 8, 15, 0, 0, 0),
        status='offline',
    ).save()

    dev.fleet.set(fleet)

    serverTime = datetime(2017, 8, 15, 3, 0, 0)
    lastUpdate = datetime(2017, 8, 15, 3, 0, 0)

    res = client.post('/webhook/{}/traccar/event'.format(
        app.config['WEBHOOK_KEY']
    ), data=json.dumps({
        'event': {
            "type": 'deviceUnknown',
            "serverTime": serverTime.strftime('%Y-%m-%dT%H:%M:%S.%f+0000'),
        },
        'device': {
            'id': 5,
            "uniqueId": org.code + "05555",
            "status": 'unknown',
            "lastUpdate": lastUpdate.strftime('%Y-%m-%dT%H:%M:%S.%f+0000'),
        },
    }))
    assert res.status_code == 200


def test_device_alarm(client, pubsub):
    pubsub.psubscribe('testing:fleet:*', 'testing:device:*')
    messages = pubsub.listen()
    org = make_org('testing')
    oldtime = datetime(2017, 8, 23)
    newtime = datetime(2017, 10, 30)

    fleet = Fleet(abbr='NDV', name='the fleet').save()
    dev = Device(
        code='05555',
        traccar_id=5,
        updated_at=oldtime,
    ).save()

    dev.fleet.set(fleet)

    res = client.post('/webhook/{}/traccar/event'.format(
        app.config['WEBHOOK_KEY']
    ), data=json.dumps({
        'event': {
            'type': 'alarm',
            'attributes': {
                'alarm': 'fatigueDriving',
            },
            'serverTime': newtime.strftime('%Y-%m-%dT%H:%M:%S.%f+0000'),
        },
        'device': {
            'id': 5,
            "uniqueId": org.code + "05555",
            "status": "online",
            "lastUpdate": newtime.strftime('%Y-%m-%dT%H:%M:%S.%f+0000'),
        },
        'position': {
            'altitude': 1400,
            'course': 137,
            'deviceTime': newtime.strftime('%Y-%m-%dT%H:%M:%S.%f+0000'),
            'fixTime': newtime.strftime('%Y-%m-%dT%H:%M:%S.%f+0000'),
            'latitude': 20,
            'longitude': -100,
            'outdated': False,
            'protocol': 'pt502',
            'serverTime': newtime.strftime('%Y-%m-%dT%H:%M:%S.%f+0000'),
            'speed': 15,  # in knots
            'valid': True,
        },
    }))
    assert res.status_code == 200

    dev.reload()

    assert dev.last_update == newtime
    assert dev.updated_at == oldtime
    assert dev.status == 'alarm'
    assert dev.alarm_type == 'fatigueDriving'

    pubsub.get_message()  # Consume the subscribe message

    # event in fleet's channel
    message = next(messages)
    assert message is not None
    assert json.loads(message['data'])['event'] == 'device-update'

    # the alarm
    message = next(messages)
    assert message is not None
    assert message['channel'] == 'testing:fleet:{}'.format(fleet.id)
    assert json.loads(message['data']) == {
        'event': 'alarm',
        'data': {
            'type': 'fatigueDriving',
            'device': dev.to_json(),
            'time': newtime.strftime('%Y-%m-%dT%H:%M:%SZ'),
            'org_name': 'testing',
        },
    }
