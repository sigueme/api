from fleety.core import id_function
from fleety.db.models import User
from fleety.db.models.bounded import Device


def test_short_ids_function():
    id1 = id_function()
    id2 = id_function()

    assert type(id1) == str

    assert id1 != id2

    assert len(id1) == 12


def test_id_function_in_use():
    user = User().save()

    assert len(user.id) == 12

    dev = Device().save()
    assert len(dev.id) == 12
