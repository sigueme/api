import pytest

from fleety.db.models.sql import Position, TraccarDevice, Log, Report, \
    ReportScheduler, Trip

from .utils import make_org, make_admin, make_auth

TESTING_SETTINGS = {
    'TESTING': True,
    'DEBUG': True,

    'WEBHOOK_KEY': 'WBHKEY',

    'CORS_ORIGINS': '.*',

    'DOMAIN_EXTENSION': 'dev',

    'FLEETY_BROKER_HANDLERS': [
        'Email',
        'Sms',
    ],

    'FLEETY_REPORT_BUILDERS': [
        'SamplePDFBuilder',
    ],

    'ORGANIZATION': 'testing',
}


@pytest.fixture
def client():
    ''' makes and returns a testclient for the flask application '''
    from fleety import app

    app.config.from_mapping(TESTING_SETTINGS)

    return app.test_client()


@pytest.fixture(autouse=True)
def eng():
    ''' Binds the models to a coralillo engine, returns nothing '''
    from fleety import eng

    eng.lua.drop(args=['*'])


@pytest.fixture(autouse=True)
def sql():
    from fleety import app, sql

    sql.session.query(Log).delete()
    sql.session.query(Position).delete()
    sql.session.query(TraccarDevice).delete()
    sql.session.query(ReportScheduler).delete()
    sql.session.query(Report).delete()
    sql.session.query(Trip).delete()

    sql.get_engine(app).execute('ALTER SEQUENCE trip_id_seq RESTART WITH 1')
    sql.get_engine(app).execute('ALTER SEQUENCE report_id_seq RESTART WITH 1')
    sql.get_engine(app).execute(
        'ALTER SEQUENCE report_scheduler_id_seq RESTART WITH 1'
    )
    sql.get_engine(app).execute('ALTER SEQUENCE log_id_seq RESTART WITH 1')
    sql.get_engine(app).execute(
        'ALTER SEQUENCE positions_id_seq RESTART WITH 1'
    )
    sql.get_engine(app).execute(
        'ALTER SEQUENCE devices_id_seq RESTART WITH 1'
    )


@pytest.fixture
def pubsub():
    from fleety import eng

    p = eng.redis.pubsub(ignore_subscribe_messages=True)

    yield p

    p.unsubscribe()


@pytest.fixture
def auth():
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)

    return make_auth(admin)
