import pytest

from fleety.helpers.geo import geocoding
from fleety.helpers.strings import normalize, submatch


@pytest.mark.skip
def test_geoencode():
    gdl = geocoding(20.6768693, -103.3686317)

    assert gdl == 'Guadalajara, Jalisco, Mexico'


def test_normalize():
    assert normalize("HOLA") == "hola"
    assert normalize("Josué") == "josue"
    assert normalize("ÑÁÇü") == "nacu"
    assert normalize("  Og Astorga \t\n Díaz ") == "og astorga diaz"


def test_submatch():
    assert submatch('Pedro Fernandez', 'fndz ped') is True
    assert submatch('Torton T17', '17 tortn') is True
    assert submatch('Torton T17', '19 tortn') is False
    assert submatch('Camión', 'ion') is True
