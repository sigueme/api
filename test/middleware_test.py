from flask import json

import fleety
import fleety.views.test  # noqa

from .utils import make_org, make_auth, make_admin


def test_cannot_acces_org_realm_without_auth(client):
    make_org('testing')

    res = client.get('/v1/testing/simple')

    assert res.status_code == 401


def test_org_realm_warns_invalid_api(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)

    res = client.get('/pollo/testing/simple', headers=make_auth(admin))

    assert res.status_code == 404
    assert json.loads(res.data) == {
        'errors': [{
            'detail': 'api_version not found',
            'i18n': 'errors.http.not_found',
            'resource': 'api_version',
        }],
    }


def test_org_realm_warns_invalid_org(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)

    res = client.get('/v1/noorg/simple', headers=make_auth(admin))

    assert res.status_code == 404
    assert json.loads(res.data) == {
        'errors': [{
            'detail': 'organization not found',
            'i18n': 'errors.http.not_found',
            'resource': 'organization',
        }],
    }


def test_org_realm_warns_user_not_in_org(client):
    make_org('testing')
    other_org = make_org('tostong')
    other_admin = make_admin('admin@testing.org', other_org)

    res = client.get('/v1/testing/simple', headers=make_auth(other_admin))

    assert res.status_code == 403


def test_org_realm(client):
    org = make_org('testing')
    admin = make_admin('admin@testing.org', org)

    res = client.get('/v1/testing/simple', headers=make_auth(admin))

    assert res.status_code == 200
    assert res.data == b'ok'
