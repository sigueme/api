pidfile = '/run/gunicorn/fleety-back.pid'
bind = ['unix:/run/gunicorn/fleety-back.sock']
user = 'www-data'
group = 'www-data'
accesslog = '/var/log/gunicorn/fleety-back.access.log'
errorlog = '/var/log/gunicorn/fleety-back.error.log'
raw_env = [
    'FLEETY_SETTINGS=/home/fleety/webapps/api.getfleety.com/'
    'settings_production.py'
]
capture_output = True
