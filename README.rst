Baquén de Fleety
================

.. image:: https://gitlab.com/sigueme/api/badges/master/pipeline.svg
   :target: https://gitlab.com/sigueme/api/commits/master
   :alt: Pipeline status

Desarrollo
----------

Cosas necesarias:

* Tener ``python`` y de preferencia alguna forma de entorno virtual, como
  ``virtualenv``, ``pipenv`` o ``poetry``. En este manual se asume pipenv.
* instalar ``postgres``, y ``postgis``,
* instalar ``redis``,
* crear la base de datos,
* añadir soporte postgis a esa base de datos con ``CREATE EXTENSION postgis;``.

Establecer las siguientes variables de entorno en un archivo `.env` en la raíz
del proyecto:

.. code:: bash

    FLASK_APP=fleety
    FLASK_DEBUG=1

    SECRET_KEY=unacadenade50caracteresalazar
    REDIS_URL=redis://localhost:6379/0
    SQLALCHEMY_DATABASE_URI=postgresql://usuario:password@localhost:puerto/basededatos

Instalar las dependencias:

.. code:: bash

    pipenv install -r requirements.txt

Correr las migraciones e instalar la semilla de la base de datos:

.. code:: bash

    pipenv run flask db upgrade
    pipenv run flask seed

Finalmente es posible correr el servicio con

.. code:: bash

    pipenv run flask run

Base de datos
.............

Las migraciones se hacen por medio de
`flask-migrate <https://flask-migrate.readthedocs.io/en/latest/>`_ que tiene una
interfaz similar a alembic pero detrás del subcomando ``db``. Por ejemplo para
crear una nueva revisión:

.. code:: bash

    pipenv run flask db revision --autogenerate -m 'el mensaje'

Pruebas
-------

Puedes correr todas las pruebas con

.. code:: bash

    pipenv run pytest

o correr solamente algunas con

.. code:: bash

    pipenv run pytest -xvv some_test

Depuración de scripts Lua
-------------------------

Se puede hacer usando esta función

.. code:: lua

    redis.log(redis.LOG_WARNING, "hello")

y este comando

.. code:: bash

    journalctl --unit redis -f -e
