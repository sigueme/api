local lon          = ARGV[1]
local lat          = ARGV[2]
local device_uid   = ARGV[3]
local device_id    = ARGV[4]
local device_time  = ARGV[5]
local server_time  = ARGV[6]
local id_for_dev   = ARGV[7]
local id_for_fleet = ARGV[8]
local outdated     = ARGV[9]
local parsed_time  = ARGV[10]
local device_status= ARGV[11]

-- more global variables
local last_course = 0
local last_speed = 0

-- First find organization
local org_code    = device_uid:sub(1, 8)
local device_code = device_uid:sub(9, 13)

if org_code == '' or device_code == '' then
    return 2
end

-- Finds organization subdomain from its code
local function get_org_subdomain(org_code)
    local org_id = redis.call('HGET', 'organization:index_code', org_code)

    if org_id == false then
        return false
    end

    return redis.call('HGET', 'organization:'..org_id..':obj', 'subdomain')
end

-- Finds or creates the new devices fleet
local function get_or_create_fleet(org_subdomain)
    local fleet_index_key = org_subdomain..':fleet:index_abbr'
    local maybeid = redis.call('HGET', fleet_index_key, 'NDV')

    if maybeid == false then
        -- Create the new device fleet
        local new_dev_fleet_key = org_subdomain..':fleet:'..id_for_fleet..':obj'

        redis.call('HSET', new_dev_fleet_key, 'abbr', 'NDV')
        redis.call('HSET', new_dev_fleet_key, 'name', 'Nuevos dispositivos')
        redis.call('HSET', new_dev_fleet_key, 'created_at', server_time)
        redis.call('HSET', new_dev_fleet_key, 'updated_at', server_time)
        redis.call('HSET', org_subdomain..':fleet:index_abbr', 'NDV', id_for_fleet)
        redis.call('SADD', org_subdomain..':fleet:members', id_for_fleet)

        return id_for_fleet
    end

    return maybeid
end

-- Finds or creates a device
local function get_or_create_dev(org_subdomain)
    local dev_index_code = org_subdomain..':device:index_code'
    local geo_key = org_subdomain..':device:geo_last_pos'

    -- Try to find this device in the database
    local prev_id = redis.call('HGET', dev_index_code, device_code)
    local dev_key = nil

    if prev_id == false then
        dev_key = org_subdomain..':device:'..id_for_dev..':obj'
        local fleet_id = nil

        -- create this device
        redis.call('HSET', dev_key, 'id', id_for_dev)
        redis.call('HSET', dev_key, 'code', device_code)
        redis.call('HSET', dev_key, 'traccar_id', device_id)
        redis.call('HSET', dev_key, 'created_at', server_time)
        redis.call('HSET', dev_key, 'updated_at', server_time)
        redis.call('HSET', dev_key, 'last_update', device_time)
        redis.call('HSET', dev_key, 'name', 'New device')

        if device_status then
            redis.call('HSET', dev_key, 'status', device_status)
        else
            redis.call('HSET', dev_key, 'status', 'stopped')
        end

        redis.call('HSET', dev_index_code, device_code, id_for_dev)

        -- add it to new vehicles fleet
        local fleet_id = get_or_create_fleet(org_subdomain)
        local fleet_rel_key = org_subdomain..':fleet:'..fleet_id..':srel_devices'

        -- relate each other
        redis.call('HSET', dev_key, 'fleet', fleet_id)
        redis.call('SADD', fleet_rel_key, id_for_dev)

        -- add the last position
        redis.call('GEOADD', geo_key, lon, lat, id_for_dev)

        -- add it to the members key
        redis.call('SADD', org_subdomain..':device:members', id_for_dev)

        return {id_for_dev, fleet_id}
    end

    dev_key = org_subdomain..':device:'..prev_id..':obj'

    -- device already exists, update
    if outdated == '0' then
        if device_status then
            redis.call('HSET', dev_key, 'status', device_status)
        end

        -- check if this data exists before computing
        local last_pos = redis.call('GEOPOS', geo_key, prev_id)[1]
        local time = redis.call('HGET', dev_key, 'last_update')

        -- update position and time
        redis.call('GEOADD', geo_key, lon, lat, prev_id)
        redis.call('HSET', dev_key, 'last_update', device_time)

        if last_pos and time then
            -- compute speed
            redis.call('GEOADD', geo_key, last_pos[1], last_pos[2], prev_id..'-old')

            local dist = redis.call('GEODIST', geo_key, prev_id, prev_id..'-old')/1000

            redis.call('ZREM', geo_key, prev_id..'-old')

            local timedelta = (device_time - time)/3600

            if timedelta > 0 then
                last_speed = dist/timedelta
            else
                last_speed = redis.call('HGET', dev_key, 'last_speed')

                if not last_speed then
                    last_speed = 0
                end
            end

            if dist > 0 then
                -- compute course
                redis.call('GEOADD', geo_key, last_pos[1], lat, prev_id..'-temp')
                local delta = redis.call('GEODIST', geo_key, prev_id, prev_id..'-temp')/1000

                redis.call('ZREM', geo_key, prev_id..'-temp')

                if lon > last_pos[1] then
                    delta = delta * -1
                end

                local quotient = delta/dist

                if quotient > 1 then
                    quotient = 1
                elseif quotient < -1 then
                    quotient = -1
                end

                last_course = math.asin(quotient)

                if lat < last_pos[2] then
                    last_course = math.pi - last_course
                end
            else
                last_course = redis.call('HGET', dev_key, 'last_course')

                if not last_course then
                    last_course = 0
                end
            end
        end

        redis.call('HSET', dev_key, 'last_speed', last_speed)
        redis.call('HSET', dev_key, 'last_course', last_course)
    end

    return {prev_id, redis.call('HGET', dev_key, 'fleet')}
end

-- Publishes a message in the appropiate channel
local function publish_new_position(org_subdomain, fleet_id, dev_id)
    local fleet_key = org_subdomain..':fleet:'..fleet_id
    local device_key = org_subdomain..':device:'..dev_id
    local device_data = {
        _type       = org_subdomain..':device',
        id          = dev_id,
        last_pos    = {
            lat = tonumber(lat),
            lon = tonumber(lon),
        },
        last_update = parsed_time,
        last_speed = last_speed,
        last_course = last_course,
        name = redis.call('HGET', org_subdomain..':device:'..dev_id..':obj', 'name'),
    }

    if device_status then
        device_data['status'] = device_status
    end

    local event_data = cjson.encode({
        event = 'device-update',
        data = device_data,
    })

    redis.call('PUBLISH', fleet_key, event_data)
end

local function main()
    local org_subdomain = get_org_subdomain(org_code)

    if org_subdomain == false then
        return 1
    end

    local dev_fleet = get_or_create_dev(org_subdomain)

    publish_new_position(org_subdomain, dev_fleet[2], dev_fleet[1])

    return {org_subdomain, dev_fleet[1]}
end

return main()
