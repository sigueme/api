local device_uid     = ARGV[1]
local received_at    = ARGV[2]
local event          = ARGV[3]
local parsed_time    = ARGV[4]

-- First find organization
local org_code    = device_uid:sub(1, 8)
local device_code = device_uid:sub(9, 13)

if org_code == '' or device_code == '' then
    return 2
end

-- Finds organization subdomain from its code
local function get_org_subdomain(org_code)
    local org_id = redis.call('HGET', 'organization:index_code', org_code)

    if org_id == false then
        return false
    end

    return redis.call('HGET', 'organization:'..org_id..':obj', 'subdomain')
end

-- Finds or creates a device
local function get_dev(org_subdomain)
    local dev_index_code = org_subdomain..':device:index_code'

    -- Try to find this device in the database
    local prev_id = redis.call('HGET', dev_index_code, device_code)

    if prev_id == false then
        return 1
    end

    local dev_key = nil
    dev_key = org_subdomain..':device:'..prev_id..':obj'

    redis.call('HSET', dev_key, 'last_update', received_at)
    redis.call('HSET', dev_key, 'status', event)

    return {prev_id, redis.call('HGET', dev_key, 'fleet')}
end

-- Publishes a message in the appropiate channel
local function publish_event(org_subdomain, fleet_id, dev_id)
    local fleet_key = org_subdomain..':fleet:'..fleet_id
    local dev_key = org_subdomain..':device:'..dev_id

    local event_data = cjson.encode({
        event = 'device-update',
        data = {
            _type       = org_subdomain..':device',
            id          = dev_id,
            status      = event,
            last_update = parsed_time,
        },
    })

    redis.call('PUBLISH', fleet_key, event_data)
end

local function main()
    local org_subdomain = get_org_subdomain(org_code)

    if org_subdomain == false then
        return org_code
    end

    local dev_data = get_dev(org_subdomain)

    if dev_data == 1 then
        return 1
    end

    publish_event(org_subdomain, dev_data[2], dev_data[1])

    return {org_subdomain, dev_data[1]}
end

return main()
