from fleety import eng
import os

SCRIPT_PATH = os.path.dirname(__file__)

scripts = filter(lambda s: s.endswith('.lua'), os.listdir(SCRIPT_PATH))

for scriptname in scripts:
    with open(os.path.join(SCRIPT_PATH, scriptname)) as script:
        eng.lua.register(scriptname.split('.')[0], script.read())
