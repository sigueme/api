local org     = ARGV[1]
local current = tonumber(ARGV[2])

for i=current,9999 do
    local val = string.format('%05d', i)

    if redis.call('hexists', org..':device:index_code', val) == 0 then
        return val
    end
end

return '00000'
