from datetime import datetime
from enum import Enum
from flask import json
from geoalchemy2 import Geometry
from geoalchemy2.shape import to_shape
from json.decoder import JSONDecodeError
from sqlalchemy import Sequence

from fleety import sql, app
from fleety.helpers.dates import get_iso
from fleety.helpers.json import get_json


class Position(sql.Model):
    ''' Extracted from traccar '''
    __tablename__ = 'positions'

    id = sql.Column(
        sql.Integer, Sequence('positions_id_seq'), primary_key=True
    )
    deviceid = sql.Column(sql.Integer)
    latitude = sql.Column(sql.Float)
    longitude = sql.Column(sql.Float)
    devicetime = sql.Column(sql.DateTime)
    servertime = sql.Column(sql.DateTime)
    fixtime = sql.Column(sql.DateTime)
    valid = sql.Column(sql.Boolean)
    altitude = sql.Column(sql.Float)
    speed = sql.Column(sql.Float)
    course = sql.Column(sql.Float)

    def to_json(self):
        server_time = self.servertime
        device_time = self.devicetime

        if server_time is not None:
            server_time = server_time.replace(microsecond=0).isoformat() + 'Z'

        if device_time is not None:
            device_time = device_time.replace(microsecond=0).isoformat() + 'Z'

        return {
            'id': self.id,
            'lat': self.latitude,
            'lon': self.longitude,
            'server_time': server_time,
            'device_time': device_time,
            'speed': self.speed * 1.852001 if self.speed else 0,
            # traccar speed is in knots = nautical miles/hour = 1852 m/h
            '_type': 'position',
        }


class TripStatus(Enum):
    SCHEDULED = 1
    ONGOING = 2
    FINISHED = 3


class Trip(sql.Model):
    id = sql.Column(sql.Integer, Sequence('trip_id_seq'), primary_key=True)
    org_subdomain = sql.Column(sql.String, nullable=False, index=True)
    status = sql.Column(sql.Enum(TripStatus), nullable=False, index=True,
                        default=TripStatus.SCHEDULED)

    # Filled when scheduling
    origin = sql.Column(sql.String)
    origin_pos = sql.Column(Geometry('POINT'))
    destination = sql.Column(sql.String)
    destination_pos = sql.Column(Geometry('POINT'))
    user_id = sql.Column(sql.String, index=True)
    route_id = sql.Column(sql.String)
    fleet_id = sql.Column(sql.String)
    scheduled_departure = sql.Column(sql.DateTime)
    scheduled_arrival = sql.Column(sql.DateTime)
    notes = sql.Column(sql.String)
    notify_stop = sql.Column(sql.Integer)

    # Filled at departure
    device_id = sql.Column(sql.String, index=True)
    departure = sql.Column(sql.DateTime, index=True)

    # Filled at arrival
    arrival = sql.Column(sql.DateTime, index=True)
    polyline = sql.Column(sql.String)

    def to_json(self):
        from fleety.db.models import User
        from fleety.db.models.bounded import Route, Device

        def get_point(thing):
            if thing is None:
                return None

            point = to_shape(thing)

            return {
                'lon': point.x,
                'lat': point.y,
            }

        route = Route.get(self.route_id) if self.route_id else None
        device = Device.get(self.device_id) if self.device_id else None
        user = User.get(self.user_id) if self.user_id else None

        route = route.to_json() if route else None
        device = device.to_json() if device else None
        user = user.to_json() if user else None

        return {
            '_type': '{}:trip'.format(app.config['ORGANIZATION']),

            'id': self.id,
            'status': self.status.name,
            'origin': self.origin,
            'origin_pos': get_point(self.origin_pos),
            'destination': self.destination,
            'destination_pos': get_point(self.destination_pos),

            'user_id': self.user_id,
            'user': user,

            'route_id': self.route_id,
            'route': route,

            'device_id': self.device_id,
            'device': device,

            'fleet_id': self.fleet_id,
            'scheduled_departure': get_iso(self.scheduled_departure),
            'scheduled_arrival': get_iso(self.scheduled_arrival),
            'notes': self.notes,
            'notify_stop': self.notify_stop,
            'departure': get_iso(self.departure),
            'arrival': get_iso(self.arrival),
            'polyline': self.polyline,
        }


class Log(sql.Model):
    id = sql.Column(sql.Integer, Sequence('log_id_seq'), primary_key=True)
    org_subdomain = sql.Column(sql.String, index=True)
    channel = sql.Column(sql.String)
    event = sql.Column(sql.String)
    data = sql.Column(sql.Text, default='{}')
    created_at = sql.Column(sql.DateTime, default=datetime.now)

    def to_json(self):
        try:
            data = json.loads(self.data)
        except JSONDecodeError:
            data = dict()

        return {
            '_type': '{}:log'.format(self.org_subdomain),
            'id': self.id,
            'channel': self.channel,
            'event': self.event,
            'data': data,
            'created_at': self.created_at.replace(
                microsecond=0
            ).isoformat() + 'Z',
        }


class TraccarDevice(sql.Model):
    ''' Extracted from traccar '''
    __tablename__ = 'devices'

    id = sql.Column(sql.Integer, primary_key=True)
    name = sql.Column(sql.String)
    uniqueid = sql.Column(sql.String)


class ReportScheduler(sql.Model):
    __tablename__ = 'report_scheduler'

    id = sql.Column(
        sql.Integer, Sequence('report_scheduler_id_seq'), primary_key=True
    )
    org_subdomain = sql.Column(sql.String, nullable=False, index=True)
    builder = sql.Column(sql.String, nullable=False)
    params = sql.Column(sql.Text, nullable=False, default='{}')
    created_at = sql.Column(sql.DateTime, nullable=False, default=datetime.now)
    last_run_at = sql.Column(sql.DateTime, nullable=True, default=None)
    name = sql.Column(sql.String, nullable=True, default=None)
    run_at = sql.Column(sql.DateTime, nullable=True, default=None)
    cron = sql.Column(sql.String, nullable=True, default=None)
    active = sql.Column(sql.Boolean, nullable=False, default=True)
    user_id = sql.Column(sql.String, nullable=False, default=None)

    def to_json(self):
        return {
            '_type': '{}:report_scheduler'.format(self.org_subdomain),
            'id': self.id,
            'builder': self.builder,
            'params': get_json(self.params),
            'created_at': get_iso(self.created_at),
            'last_run_at': self.last_run_at,
            'name': self.name,
            'run_at': get_iso(self.run_at),
            'cron': self.cron,
            'active': self.active,
        }


class ReportStatus(Enum):
    IN_PROGRESS = 1
    COMPLETED = 2
    FAILED = 3


class Report(sql.Model):
    id = sql.Column(sql.Integer, Sequence('report_id_seq'), primary_key=True)
    scheduler_id = sql.Column(sql.Integer, sql.ForeignKey(
        'report_scheduler.id', ondelete='SET NULL'
    ), nullable=True)
    org_subdomain = sql.Column(sql.String, nullable=False, default=None,
                               index=True)
    status = sql.Column(sql.Enum(ReportStatus), nullable=False, index=True,
                        default=ReportStatus.IN_PROGRESS)
    builder = sql.Column(sql.String, nullable=False)
    params = sql.Column(sql.Text, nullable=False, default='{}')
    requested_at = sql.Column(sql.DateTime, nullable=False,
                              default=datetime.now)
    completed_at = sql.Column(sql.DateTime, nullable=True, default=None)
    failed_reason = sql.Column(sql.Text, nullable=True, default=None)
    name = sql.Column(sql.String, nullable=True, default=None)
    user_id = sql.Column(sql.String, nullable=False, default=None)
    url = sql.Column(sql.String, nullable=True, default=None)

    def to_json(self):
        return {
            '_type': '{}:report'.format(self.org_subdomain),
            'id': self.id,
            'scheduler_id': self.scheduler_id,
            'status': self.status.name,
            'builder': self.builder,
            'params': get_json(self.params),
            'requested_at': get_iso(self.requested_at),
            'completed_at': get_iso(self.completed_at),
            'name': self.name,
            'user_id': self.user_id,
            'failed_reason': self.failed_reason,
            'url': self.url,
        }
