from coralillo import BoundedModel, fields
from coralillo.errors import UnboundModelError
from datetime import datetime
from fleety import app, eng


class FleetyBoundedModel(BoundedModel):
    @classmethod
    def prefix(cls):
        try:
            return app.config['ORGANIZATION']
        except KeyError:
            raise UnboundModelError('oraganization not set')

    class Meta:
        engine = eng


class Device(FleetyBoundedModel):
    code = fields.Text(index=True)
    traccar_id = fields.Integer()
    name = fields.Text()
    description = fields.Text()
    created_at = fields.Datetime(default=datetime.now)
    updated_at = fields.Datetime(default=datetime.now)
    last_pos = fields.Location()
    last_speed = fields.Float(default=0.0)
    last_course = fields.Float(default=0.0)
    last_update = fields.Datetime()
    fleet = fields.ForeignIdRelation(
        'fleety.db.models.bounded.Fleet', inverse='devices'
    )
    status = fields.Text(default='offline')  # stopped offline moving alarm
    alarm_type = fields.Text()
    dynamic = fields.Dict()

    _searchable = ('code', 'name')


class DeviceDynamicAttribute(FleetyBoundedModel):
    label = fields.Text()


class Route(FleetyBoundedModel):
    name = fields.Text()
    polyline = fields.Text()
    created_at = fields.Datetime(default=datetime.now)
    updated_at = fields.Datetime(default=datetime.now)

    _searchable = ('name',)


class Geofence(FleetyBoundedModel):
    name = fields.Text()
    polyline = fields.Text()
    created_at = fields.Datetime(default=datetime.now)
    updated_at = fields.Datetime(default=datetime.now)

    notify = True
    _searchable = ('name',)


class Fleet(FleetyBoundedModel):
    name = fields.Text()
    abbr = fields.Text(index=True)
    devices = fields.SetRelation(Device, inverse='fleet')
    created_at = fields.Datetime(default=datetime.now)
    updated_at = fields.Datetime(default=datetime.now)

    _searchable = ('abbr', 'name')
# If you add fields here, don't forget to edit device_motion.lua accordingly
