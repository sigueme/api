from base64 import b64encode
from coralillo import Model, fields
from coralillo.auth import PermissionHolder
from coralillo.errors import InvalidFieldError
from coralillo.validation import validation_rule
from datetime import datetime, timedelta
from random import choice
import re
import string

from fleety import app, eng, hashids
from fleety.http.errors import AuthFieldError


def compute_expire():
    return datetime.now() + timedelta(hours=24)


def create_api_key():
    return ''.join(
        choice(string.ascii_lowercase + string.digits) for c in range(40)
    )


def default_org_code():
    while True:
        proposed = ''.join(choice('0123456789') for r in range(8))

        if eng.redis.hexists('organization:index_code', proposed):
            continue

        if proposed.startswith('0'):
            continue

        return proposed


class Organization(Model):
    name = fields.Text()
    subdomain = fields.Text(
        index=True,
        regex=r'^[a-z0-9](?:[a-z0-9\-]{0,61}[a-z0-9])?$',
        forbidden=app.config['FLEETY_RESTRICTED_DOMAINS'],
    )
    users = fields.SetRelation('fleety.db.models.User', inverse='orgs')
    code = fields.Text(
        index=True,
        regex='^[0-9]{8}$',
        default=default_org_code,
    )
    created_at = fields.Datetime(default=datetime.now)
    updated_at = fields.Datetime(default=datetime.now)

    class Meta:
        engine = eng


class Subscription(Model):
    channel = fields.TreeIndex()
    channel_name = fields.Text()
    event = fields.Text()
    user = fields.ForeignIdRelation(
        'fleety.db.models.User', inverse='subscriptions'
    )
    handler = fields.Text()
    params = fields.Dict()
    created_at = fields.Datetime(default=datetime.now)
    updated_at = fields.Datetime(default=datetime.now)

    @validation_rule
    def validate_handler(data):
        if data.handler not in app.config['FLEETY_BROKER_HANDLERS']:
            raise InvalidFieldError('handler')

    class Meta:
        engine = eng


class User(Model, PermissionHolder):
    name = fields.Text()
    last_name = fields.Text()
    email = fields.Text(index=True, regex=r'^[\w.%+-]+@[\w.-]+\.[a-zA-Z]{2,}$')
    password = fields.Hash(private=True)
    is_active = fields.Bool(default=True)
    expires = fields.Datetime(required=False, default=compute_expire)
    api_key = fields.Text(
        private=True, index=True, required=False, default=create_api_key
    )
    is_god = fields.Bool(default=False, fillable=False)
    orgs = fields.SetRelation('fleety.db.models.Organization', inverse='users')
    confirmed = fields.Bool(default=False, fillable=False)
    subscriptions = fields.SetRelation(
        'fleety.db.models.Subscription', inverse='user'
    )
    created_at = fields.Datetime(default=datetime.now)
    updated_at = fields.Datetime(default=datetime.now)
    last_read = fields.Datetime(default=datetime.now)
    telegram_chat_id = fields.Text(index=True, required=False, private=True)

    _searchable = ('name', 'last_name', 'email')

    def basic_auth(self):
        return b64encode(('api:%s' % self.api_key).encode()).decode('ascii')

    def is_allowed(self, objspec):
        if self.is_god:
            return True

        return super().is_allowed(objspec)

    def perms_for_org(self, subdomain):
        return sorted(filter(
            lambda p: re.match('^' + subdomain + '([/:].*)?$', p),
            self.get_perms()
        ))

    def confirm_token(self):
        return hashids.encode(
            hashids.decode(self.id)[0],
            int((datetime.now() + timedelta(hours=24)).timestamp()),
        )

    def check_login(self, org):
        if not self.is_active:
            raise AuthFieldError(
                field='email',
                reason='disabled',
            )

        if self not in org.users:
            raise AuthFieldError(
                field='email',
                reason='not_org_member',
            )

    def to_session(self, org_subdomain):
        ''' like to_json but for session responses '''
        data = self.to_json()

        data['api_key'] = self.api_key
        data['permissions'] = self.perms_for_org(org_subdomain)

        app.config['ORGANIZATION'] = org_subdomain

        from fleety.db.models.bounded import Device
        data['empty_account'] = Device.count() == 0

        return data

    class Meta:
        engine = eng
