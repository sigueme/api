from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_coralillo import Coralillo
from hashids import Hashids
import time
import os

# Read config values
app = Flask(__name__)
app.config.from_object('fleety.settings')

# Hashids
hashids = Hashids(salt=app.config['SECRET_KEY'])

# Timezone
os.environ['TZ'] = app.config.get('TIMEZONE', 'UTC')
time.tzset()

# Cross Origin Resource Sharing
cors = CORS(app, resources={r'/*': {'origins': app.config['CORS_ORIGINS']}})

# Database
from fleety.core import id_function  # noqa
eng = Coralillo(
    app,
    id_function=id_function,
    decode_responses=True,
)
sql = SQLAlchemy(app)
migrate = Migrate(app, sql)

# Register lua scripts
import fleety.db.lua  # noqa

# Session
import fleety.auth  # noqa

# CLI Commands
import fleety.cli  # noqa

# Load views
import fleety.views  # noqa
import fleety.views.auth  # noqa
import fleety.views.api.device  # noqa
import fleety.views.api.dynamic_attribute  # noqa
import fleety.views.api.fleet  # noqa
import fleety.views.api.geofence  # noqa
import fleety.views.api.log  # noqa
import fleety.views.api.me  # noqa
import fleety.views.api.report  # noqa
import fleety.views.api.report_scheduler  # noqa
import fleety.views.api.route  # noqa
import fleety.views.api.search  # noqa
import fleety.views.api.subscription  # noqa
import fleety.views.api.trip  # noqa
import fleety.views.api.user  # noqa
import fleety.views.webhooks  # noqa
import fleety.views.open  # noqa

# Load error handlers
import fleety.http.error_handlers  # noqa
