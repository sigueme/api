from fleety.http.middleware import org_realm


@org_realm('/simple')
def test_simple():
    return 'ok'
