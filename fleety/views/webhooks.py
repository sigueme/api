from coralillo.errors import MissingFieldError
from datetime import datetime
from flask import request, json
import requests

from fleety import app, eng
from fleety.core import id_function
from fleety.db.models import User
from fleety.db.models.bounded import Device
from fleety.http.errors import WantsJsonError
from fleety.http.errors import HttpBadRequest
from fleety.http.forms.api import TraccarPositionForm
from fleety.http.json import ok_response
from fleety.http.middleware import webhook


@webhook('/test')
def webhook_test():
    return ok_response('hello')


@webhook('/traccar/position', methods=['GET'])
def traccar_position():
    data = TraccarPositionForm.validate(**request.args.to_dict())

    # Traccar positions are given in milliseconds
    device_time = int(int(data.device_time) / 1000)

    server_time = int(datetime.now().timestamp())

    eng.lua.device_motion(args=[
        data.lon,
        data.lat,
        data.code,
        data.id,
        device_time,
        server_time,
        id_function(),
        id_function(),
        0,
        datetime.fromtimestamp(device_time).strftime('%Y-%m-%dT%H:%M:%SZ'),
    ])

    return ok_response('position registered')


@webhook('/traccar/event', methods=['POST'])
def traccar_event():
    try:
        data = json.loads(request.data)
    except json.decoder.JSONDecodeError:
        raise WantsJsonError('this request accepts only application/json')

    if 'event' not in data:
        raise MissingFieldError('event')

    if data['event']['type'] == 'alarm':
        event_name = 'alarm'
        alarm_type = data['event']['attributes']['alarm']
    else:
        event_name = app.config['TRACCAR_EVENT_MAPPING'][data['event']['type']]
        alarm_type = data['event']['type']

    if 'position' in data:
        res = eng.lua.device_motion(args=[
            data['position']['longitude'],
            data['position']['latitude'],
            data['device']['uniqueId'],
            data['device']['id'],
            int(datetime.strptime(
                data['position']['deviceTime'], '%Y-%m-%dT%H:%M:%S.%f%z'
            ).timestamp()),
            int(datetime.strptime(
                data['event']['serverTime'], '%Y-%m-%dT%H:%M:%S.%f%z'
            ).timestamp()),
            id_function(),
            id_function(),
            int(data['position']['outdated']),
            datetime.strptime(
                data['position']['deviceTime'], '%Y-%m-%dT%H:%M:%S.%f%z'
            ).strftime('%Y-%m-%dT%H:%M:%SZ'),
            event_name,
        ])
    else:
        if data['device']['lastUpdate'] is None:
            update = int(datetime.now().timestamp())
        else:
            update = int(datetime.strptime(
                data['device']['lastUpdate'], '%Y-%m-%dT%H:%M:%S.%f%z'
            ).timestamp())

        res = eng.lua.device_status(args=[
            data['device']['uniqueId'],
            update,
            event_name,
        ])

    if type(res) == int:
        app.logger.warning(
            'Got integer response ({}) from lua script'.format(res)
        )
        return ok_response('failed')

    if event_name == 'alarm':
        org_name = res[0]
        app.config['ORGANIZATION'] = org_name
        dev = Device.get(res[1])
        dev.update(alarm_type=alarm_type)
        channel = '{}:fleet:{}'.format(org_name, dev.fleet.get().id)

        eng.redis.publish(channel, json.dumps({
            'event': 'alarm',
            'data': {
                'device': dev.to_json(),
                'type': alarm_type,
                'time': data['event']['serverTime'][:19] + 'Z',
                'org_name': org_name,
            },
        }))

    return ok_response('event registered')


@webhook('/telegram', methods=['GET', 'POST'])
def telegram():
    if not request.is_json:
        raise WantsJsonError('this request wants JSON data')

    if 'message' not in request.json:
        raise HttpBadRequest('need message key')

    if 'text' not in request.json['message']:
        raise HttpBadRequest('need text message')

    chat_id = request.json['message']['chat']['id']
    user = User.get_by('telegram_chat_id', chat_id)
    message = request.json['message']['text']

    if user is None:
        user = User.get(message)

        if user is None:
            resp = 'Envía tu id de usuario para configurar este chat para ' \
                   'alertas',
        else:
            resp = 'Tu chat ha sido vinculado!'
            user.telegram_chat_id = chat_id
            user.save()
    else:
        resp = 'Tu cuenta ya está configurada!'

    requests.post('https://api.telegram.org/bot{}/sendMessage'.format(
        app.config['TELEGRAM_BOT_KEY']
    ), data={
        'chat_id': chat_id,
        'text': resp,
    })

    return ok_response(True)
