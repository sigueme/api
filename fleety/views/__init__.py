from fleety import app
from fleety.http.json import ok_response


@app.route('/')
def index():
    app.logger.warning('Someone visited the api index')

    return ok_response({
        'msg': 'Hello, world',
    })
