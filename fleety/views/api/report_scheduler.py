from flask import request, g
from coralillo.errors import ModelNotFoundError
import json

from fleety import sql
from fleety.http.middleware import org_realm
from fleety.http.json import ok_response
from fleety.http.forms.api import ReportSchedulerCreateForm
from fleety.http.forms.api import ReportSchedulerUpdateForm
from fleety.db.models.sql import ReportScheduler
from fleety.http.api_spec import sql_filtered_query


@org_realm('/report_scheduler', methods=['GET'])
def report_scheduler_list():
    base_query = ReportScheduler.query \
        .filter(ReportScheduler.org_subdomain == g.org.subdomain) \
        .filter(ReportScheduler.user_id == g.user.id)

    return sql_filtered_query(
        ReportScheduler, base_query, request.args,
        default_order='created_at.desc'
    )


@org_realm('/report_scheduler', methods=['POST'])
def report_scheduler_create():
    data = ReportSchedulerCreateForm.validate(**request.form.to_dict())

    params = json.dumps({
        key[6:]: value for key, value in request.form.to_dict().items()
        if key.startswith('param_')
    })

    new_report = ReportScheduler(
        org_subdomain=g.org.subdomain,
        name=data.name,
        builder=data.builder,
        user_id=g.user.id,
        params=params,
    )

    if request.form.get('run_at'):
        new_report.run_at = data.run_at
    elif request.form.get('cron'):
        new_report.cron = data.cron

    sql.session.add(new_report)
    sql.session.commit()

    return ok_response(new_report.to_json()), 201


@org_realm('/report_scheduler/<id>', methods=['DELETE'])
def report_scheduler_delete(id):
    report_scheduler = ReportScheduler.query.get(id)

    if report_scheduler is None:
        raise ModelNotFoundError('report_scheduler not found')

    sql.session.delete(report_scheduler)
    sql.session.commit()

    return ok_response(None), 204


@org_realm('/report_scheduler/<id>', methods=['GET'])
def report_scheduler_read(id):
    report_scheduler = ReportScheduler.query.get(id)

    if report_scheduler is None:
        raise ModelNotFoundError('report_scheduler not found')

    return ok_response(report_scheduler.to_json())


@org_realm('/report_scheduler/<id>', methods=['PUT'])
def report_scheduler_update(id):
    report_scheduler = ReportScheduler.query.get(id)

    if report_scheduler is None:
        raise ModelNotFoundError('report_scheduler not found')

    data = ReportSchedulerUpdateForm.validate(**request.form.to_dict())

    params = json.dumps({
        key[6:]: value for key, value in request.form.to_dict().items()
        if key.startswith('param_')
    })

    fields = [
        'name',
        'active',
        'run_at',
        'cron',
    ]

    for fieldname in fields:
        if request.form.get(fieldname):
            setattr(report_scheduler, fieldname, getattr(data, fieldname))

    if request.form.get('run_at'):
        report_scheduler.cron = None
    elif request.form.get('cron'):
        report_scheduler.run_at = None

    report_scheduler.params = params

    sql.session.commit()

    return ok_response(report_scheduler.to_json())
