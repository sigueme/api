from flask import request, g
from datetime import datetime

from fleety.db.models.bounded import Device, DeviceDynamicAttribute, Fleet, eng
from fleety.db.models.sql import Position, Trip
from fleety.http.functions import parse_int, parse_range
from fleety.http.middleware import org_realm
from fleety.http.json import ok_response
from fleety.http.api_spec import redis_filtered_query
from fleety.http.errors import NoMoreDeviceCodes


@org_realm('/device', methods=['GET'])
def device_list():
    return redis_filtered_query(Device, request.args)


@org_realm('/device/<id>', methods=['GET'])
def device_read(id):
    device = Device.get_or_exception(id)

    dynamic_attrs = DeviceDynamicAttribute.all()
    dynamic_attrs_set = set(map(lambda attr: attr.id, dynamic_attrs))

    device.dynamic = dict(filter(
        lambda attr: attr[0] in dynamic_attrs_set,
        device.dynamic.items()
    ))

    json = device.to_json(**g.json_params)
    json['fields'] = list(map(
        lambda dynamic: dynamic.to_json(),
        dynamic_attrs
    ))

    return ok_response(json)


@org_realm('/device/<id>', methods=['PUT'])
def device_update(id):
    device = Device.get_or_exception(id)

    dynamic_attrs = DeviceDynamicAttribute.all()
    dynamic_attrs_set = set(map(lambda attr: attr.id, dynamic_attrs))

    attributes = request.form.to_dict()

    device.update(**attributes)
    device.dynamic = dict(
        filter(
            lambda attr: attr[0] in dynamic_attrs_set,
            map(
                lambda attr: (attr[0].replace("dynamic_", ""), attr[1]),
                filter(
                    lambda attr: attr[0].startswith("dynamic_"),
                    attributes.items()
                )
            )
        )
    )
    device.save()

    fleetId = request.form.get('fleet')

    if fleetId is not None:
        fleet = Fleet.get(fleetId)
        device.fleet.set(fleet)

    json = device.to_json(**g.json_params)
    json['fields'] = list(map(
        lambda dynamic: dynamic.to_json(),
        dynamic_attrs
    ))

    return ok_response(json)


@org_realm('/device/<id>', methods=['DELETE'])
def device_delete(id):
    Device.get_or_exception(id).delete()

    return '', 204


@org_realm('/device/<id>/location_history', methods=['GET'])
def device_location_history(id):
    device = Device.get_or_exception(id)

    range = parse_range(request.args.get('devicetime'), 'devicetime')

    if range is None:
        return ok_response([
            p.to_json()
            for p in Position.query.filter(
                Position.deviceid == device.traccar_id
            ).all()
        ])

    range = list(map(lambda d: datetime.fromtimestamp(d), range))

    return ok_response([
        p.to_json()
        for p in Position.query.filter(
            Position.deviceid == device.traccar_id
        ).filter(Position.fixtime.between(*range)).all()
    ])


@org_realm('/device/<id>/trips', methods=['GET'])
def device_trips(id):
    device = Device.get_or_exception(id)
    trips = Trip.query.filter_by(device_id=device.id).all()

    return ok_response(list(map(lambda trip: trip.to_json(), trips)))


@org_realm('/device/freecode', methods=['GET'])
def device_freecode():
    current = request.args.get('current')

    if current:
        start = parse_int(current, 'current')
    else:
        start = 1

    code = eng.lua.find_next_code(args=[g.org.subdomain, start])

    if code == '00000':
        code = eng.lua.find_next_code(args=[g.org.subdomain, start])

    if code == '00000':
        raise NoMoreDeviceCodes(
            'your organization ran out of device codes, sorry'
        )

    return ok_response(code)
