from flask import request

from fleety.http.json import ok_response
from fleety.http.middleware import org_realm
from fleety.db.models.bounded import DeviceDynamicAttribute


@org_realm('/device_dynamic_attribute', methods=['POST'])
def device_dynamic_attribute_create():
    new_obj = DeviceDynamicAttribute.validate(**request.form.to_dict()).save()

    return ok_response(new_obj.to_json()), 201


@org_realm('/device_dynamic_attribute/<id>', methods=['PUT'])
def device_dynamic_attribute_update(id):
    loaded_model = DeviceDynamicAttribute.get_or_exception(id)
    loaded_model.update(**request.form.to_dict())

    return ok_response(loaded_model.to_json())


@org_realm('/device_dynamic_attribute/<id>', methods=['DELETE'])
def device_dynamic_attribute_delete(id):
    loaded_model = DeviceDynamicAttribute.get_or_exception(id)

    loaded_model.delete()

    return ok_response({
        'data': None,
    }), 204
