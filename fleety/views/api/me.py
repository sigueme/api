from flask import request, g
from fleety.http.middleware import org_realm
from fleety.http.json import ok_response
from fleety.http.forms.api import UpdateLastReadForm
from fleety.helpers.dates import get_iso


@org_realm('/me/last_read', methods=['PUT'])
def update_last_read():
    ''' Helps count notifications read by the user '''
    last_read = UpdateLastReadForm.validate(**request.form.to_dict()).last_read

    if g.user.last_read is None or g.user.last_read < last_read:
        g.user.update(
            last_read=last_read,
        )

    return ok_response(get_iso(g.user.last_read))
