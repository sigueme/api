from flask import request, g

from fleety.db.models.sql import Log
from fleety.http.middleware import org_realm
from fleety.http.api_spec import sql_filtered_query


@org_realm('/log', methods=['GET'])
def log():
    base_query = Log\
        .query\
        .filter(Log.org_subdomain == g.org.subdomain)

    return sql_filtered_query(
        Log, base_query, request.args, default_order='created_at.desc'
    )
