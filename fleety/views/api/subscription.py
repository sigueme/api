from flask import request, g
from coralillo.errors import BadField

from fleety.http.middleware import org_realm
from fleety.db.models import User, Subscription
from fleety.http.json import ok_response
from fleety.http.api_spec import redis_filtered_query


class ForbiddenSubscription(BadField):
    message = 'this user has no permissions over this channel'
    errorcode = 'forbidden_subscription'


@org_realm('/subscription', methods=['GET'])
def subscription_list():
    return redis_filtered_query(Subscription, request.args)


@org_realm('/subscription', methods=['POST'])
def subscription_create():
    channel = request.form.get('channel')

    if channel and not g.user.is_allowed(channel):
        raise ForbiddenSubscription('channel')

    new_obj = Subscription.validate(**request.form.to_dict()).save()
    g.user.subscriptions.add(new_obj)

    return ok_response(new_obj.to_json(**g.json_params)), 201


@org_realm('/user/<user_id>/subscriptions', methods=['GET'])
def user_subscriptions(user_id):
    user = User.get_or_exception(user_id)

    return ok_response(list(map(
        lambda x: x.to_json(**g.json_params),
        user.subscriptions.get()
    )))


@org_realm('/subscription/<id>', methods=['DELETE'])
def subscription_delete(id):
    item = Subscription.get_or_exception(id)
    item.delete()

    return ok_response({
        'data': None
    }), 204
