from flask import request, g
from coralillo.errors import ModelNotFoundError
from coralillo.utils import camelCase
import re

from fleety.db.models import User, bounded
from fleety.http.forms.api import PermissionForm
from fleety.http.middleware import org_realm
from fleety.http.errors import HttpNotFound
from fleety.http.json import ok_response
from fleety.db.models.sql import Trip, TripStatus


def perm_embed(perm_fqn):
    if not request.args.get('embed'):
        return perm_fqn

    matches = re.match(
        r'^(?P<objspec>(?P<org>[a-z_]+)(:(?P<classname>[a-z_]+)'
        r'(:(?P<id>[a-zA-Z0-9]{12}))?)?)(/(?P<type>view))?$', perm_fqn)

    if matches.group('id'):
        try:
            class_name = camelCase(matches.group('classname'))

            model = getattr(bounded, class_name)

            item = model.get_or_exception(matches.group('id'))

            obj = item.to_json()
        except AttributeError:
            obj = {}
        except ModelNotFoundError:
            obj = {}
    elif matches.group('classname'):
        obj = {
            'id': matches.group('objspec'),
            '_type': 'class',
            'name': 'classes.' + matches.group('classname'),
        }
    else:
        obj = {
            'id': matches.group('objspec'),
            '_type': 'organization',
            'name': matches.group('org'),
            'subdomain': matches.group('org'),
        }

    return {
        'perm': perm_fqn,
        'obj': obj,
    }


@org_realm('/user', methods=['POST'])
def user_create():
    new_user = User.validate(**request.form.to_dict()).save()

    # Append org
    new_user.orgs.add(g.org)
    new_user.orgs.fill()

    return ok_response(new_user.to_json(**g.json_params)), 201


@org_realm('/user', methods=['GET'])
def user_list():
    g.org.users.fill()
    users = g.org.users

    return ok_response(list(map(lambda x: x.to_json(**g.json_params), users)))


@org_realm('/user/<id>', methods=['GET'])
def user_read(id):
    user = User.get_or_exception(id)

    if g.org not in user.orgs:
        raise HttpNotFound(resource='user')

    return ok_response(user.to_json(**g.json_params))


@org_realm('/user/<id>', methods=['PUT'])
def user_update(id):
    user = User.get_or_exception(id)

    if user not in g.org.users:
        raise HttpNotFound(resource='user')

    user.update(**request.form.to_dict())

    return ok_response(user.to_json(**g.json_params))


@org_realm('/user/<id>', methods=['DELETE'])
def user_delete(id):
    user = User.get_or_exception(id)

    if user not in g.org.users:
        raise ModelNotFoundError('user not in this org')

    if user.orgs.count() == 0:
        user.delete()
    else:
        user.orgs.remove(g.org)

    return '', 204


@org_realm('/user/<id>/perms', methods=['POST', 'PUT', 'DELETE'])
def user_perms(id):
    user = User.get_or_exception(id)

    data = PermissionForm.validate(**request.form.to_dict())

    if request.method == 'POST':
        user.allow(data.perm)
    elif request.method == 'DELETE':
        user.revoke(data.perm)
    elif request.method == 'PUT':
        if data.perm.endswith('/view'):
            # is downgrading to view
            user.revoke(data.perm.split('/')[0])

        user.allow(data.perm)

    return ok_response({
        'perms': list(map(
            perm_embed,
            user.perms_for_org(g.org.subdomain)
        )),
    })


@org_realm('/user/<id>/perms', methods=['GET'])
def list_user_perms(id):
    user = User.get_or_exception(id)

    return ok_response({
        'perms': list(map(
            perm_embed,
            user.perms_for_org(g.org.subdomain)
        )),
    })


@org_realm('/user/<id>/scheduled_trips')
def list_scheduled_trips(id):
    user = User.get_or_exception(id)

    query = Trip.query.filter(
        Trip.user_id == user.id
    ).filter(
        Trip.device_id is not None
    ).filter(
        Trip.status != TripStatus.FINISHED
    )

    trips = query.all()

    def seralize(trip):
        json_trip = trip.to_json()

        if trip.device_id:
            json_trip['device'] = bounded.Device.get(trip.device_id).to_json()

        return json_trip

    return ok_response(list(map(seralize, trips)))
