from coralillo.utils import camelCase
from flask import request, g
from functools import partial

from fleety.db.models import User, bounded
from fleety.helpers.strings import submatch
from fleety.http.json import ok_response
from fleety.http.middleware import org_realm


def _build_searchable(Class, model):
    json = model.to_json()

    if hasattr(Class, '_searchable'):
        cols = filter(
            lambda x: x,
            map(
                lambda col: getattr(model, col),
                Class._searchable
            )
        )

        json['_searchable'] = ' '.join(cols)
    else:
        json['_searchable'] = ''

    return json


@org_realm('/search')
def search():
    query = request.args.get('q')
    context = request.args.get('context')
    if context is None:
        context = 'user,device'

    valid = ['device', 'user', 'geofence', 'fleet', 'route']

    context = list(filter(
        lambda x: x in valid,
        map(lambda y: y.strip(), context.split(','))
    ))

    results = []

    for class_name in context:
        if class_name == 'user':
            users = g.org.users.all()
            users = map(partial(_build_searchable, User), users)
            users = filter(
                lambda model: submatch(model['_searchable'], query),
                users
            )
            results += list(users)
        else:
            Class = getattr(bounded, camelCase(class_name))
            models = Class.all()
            models = map(partial(_build_searchable, Class), models)
            models = filter(
                lambda model: submatch(model['_searchable'], query),
                models
            )
            results += list(models)

    return ok_response(results[:15])
