from flask import request, g, json
from coralillo.errors import ModelNotFoundError

from fleety import sql, eng
from fleety.http.json import ok_response
from fleety.http.middleware import org_realm
from fleety.http.forms.api import ReportCreateForm
from fleety.db.models.sql import Report
from fleety.http.api_spec import sql_filtered_query


@org_realm('/report', methods=['GET'])
def report_list():
    base_query = Report.query \
        .filter(Report.org_subdomain == g.org.subdomain) \
        .filter(Report.user_id == g.user.id)

    return sql_filtered_query(
        Report, base_query, request.args, default_order='requested_at.desc'
    )


@org_realm('/report', methods=['POST'])
def report_create():
    data = ReportCreateForm.validate(**request.form.to_dict())

    params = json.dumps({
        key[6:]: value for key, value in request.form.to_dict().items()
        if key.startswith('param_')
    })

    new_report = Report(
        org_subdomain=g.org.subdomain,
        name=data.name,
        builder=data.builder,
        user_id=g.user.id,
        params=params,
    )

    sql.session.add(new_report)
    sql.session.commit()

    channel = '{}:user:{}'.format(g.org.subdomain, g.user.id)

    eng.redis.publish(channel, json.dumps({
        'event': 'report-requested',
        'data': {
            'org_name': g.org.subdomain,
            'report': new_report.to_json(),
        },
    }))

    return ok_response(new_report.to_json()), 201


@org_realm('/report/<id>', methods=['GET'])
def report_read(id):
    report = Report.query.get(id)

    if report is None:
        raise ModelNotFoundError('report not found')

    return ok_response(report.to_json())


@org_realm('/report/<id>', methods=['DELETE'])
def report_delete(id):
    report = Report.query.get(id)

    if report is None:
        raise ModelNotFoundError('report not found')

    sql.session.delete(report)
    sql.session.commit()

    return ok_response(None), 204
