from flask import request, g

from fleety.db.models.bounded import Fleet, eng
from fleety.http.middleware import org_realm
from fleety.http.json import ok_response
from fleety.http.api_spec import redis_filtered_query


@org_realm('/fleet', methods=['GET'])
def fleet_list():
    return redis_filtered_query(Fleet, request.args)


@org_realm('/fleet', methods=['POST'])
def fleet_create():
    new_obj = Fleet.validate(**request.form.to_dict()).save()

    return ok_response(new_obj.to_json(**g.json_params)), 201


@org_realm('/fleet/<id>', methods=['DELETE'])
def fleet_delete(id):
    fleet = Fleet.get_or_exception(id)

    # get or create the replacement fleet
    ndv = Fleet.get_by('abbr', 'NDV')

    if ndv is None:
        ndv = Fleet(name='Nuevos dispositivos', abbr='NDV').save()

    # relocate this fleet's devices
    for dev in fleet.devices.get():
        dev.fleet.set(ndv)

    fleet.delete()
    eng.lua.clear_related_permissions(
        keys=['user:members'],
        args=[fleet.permission()]
    )

    return '', 204


@org_realm('/fleet/<id>', methods=['GET'])
def fleet_read(id):
    fleet = Fleet.get_or_exception(id)

    fleet.devices.fill()

    return ok_response(fleet.to_json(**g.json_params))


@org_realm('/fleet/<id>', methods=['PUT'])
def fleet_update(id):
    return ok_response(Fleet.get_or_exception(id).update(
        **request.form.to_dict()
    ).to_json(**g.json_params))
