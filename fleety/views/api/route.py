from flask import request, g

from fleety.db.models.bounded import Route
from fleety.http.middleware import org_realm
from fleety.http.json import ok_response
from fleety.http.api_spec import redis_filtered_query


@org_realm('/route', methods=['GET'])
def route_list():
    return redis_filtered_query(Route, request.args)


@org_realm('/route', methods=['POST'])
def route_create():
    new_route = Route.validate(**request.form.to_dict()).save()

    return ok_response(new_route.to_json(**g.json_params)), 201


@org_realm('/route/<id>', methods=['GET'])
def route_read(id):
    route = Route.get_or_exception(id)

    return ok_response(route.to_json(**g.json_params))


@org_realm('/route/<id>', methods=['PUT'])
def route_update(id):
    route = Route.get_or_exception(id)

    route.update(**request.form.to_dict())

    return ok_response(route.to_json(**g.json_params))


@org_realm('/route/<id>', methods=['DELETE'])
def route_delete(id):
    Route.get_or_exception(id).delete()

    return '', 204
