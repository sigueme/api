from flask import request, g, json, abort
from datetime import datetime
from coralillo.errors import ModelNotFoundError, InvalidFieldError
import polyline

from fleety.db.models import User
from fleety.db.models.bounded import Device, Route
from fleety.db.models.sql import Trip, Position, TripStatus
from fleety.http.middleware import org_realm
from fleety.http.json import ok_response
from fleety.http.forms.api import TripForm, TripUpdateForm
from fleety.helpers.geo import route_sampling
from fleety import eng, sql, hashids
from fleety.http.api_spec import sql_filtered_query


@org_realm('/trip', methods=['GET'])
def trip_list():
    base_query = Trip.query.filter(Trip.org_subdomain == g.org.subdomain)

    return sql_filtered_query(
        Trip, base_query, request.args,
        default_order='status.asc,departure.desc'
    )


@org_realm('/trip', methods=['POST'])
def trip_create():
    data = TripForm.validate(**request.form.to_dict())

    models = {
        'user_id': User,
        'route_id': Route,
        'device_id': Device,
    }

    for fieldname, model in models.items():
        if request.form.get(fieldname):
            obj = model.get(request.form.get(fieldname))
            if obj is None:
                raise InvalidFieldError(fieldname)
            else:
                models[fieldname] = obj

    if data.scheduled_arrival <= data.scheduled_departure:
        raise InvalidFieldError('scheduled_arrival')

    trip = Trip(**data.to_dict())
    trip.org_subdomain = g.org.subdomain

    if request.form.get('device_id'):
        trip.fleet_id = models['device_id'].fleet

    sql.session.add(trip)
    sql.session.commit()

    trip_propagate(trip, 'create')

    return ok_response(trip.to_json()), 201


@org_realm('/trip/<id>', methods=['GET'])
def trip_read(id):
    trip = Trip.query.get(id)

    if trip is None:
        abort(404)

    json_trip = trip.to_json()

    # TODO do this in a separate endpoint or in a clean way
    # fill positions
    if json_trip['device'] is not None and trip.departure is not None:
        range = (trip.departure, datetime.now())

        if trip.arrival is not None:
            range = (trip.departure, trip.arrival)

        data = sql.session\
            .query(Position)\
            .filter(
                Position.deviceid == json_trip['device']['traccar_id']
            )\
            .filter(Position.fixtime.between(*range))\
            .order_by(Position.fixtime)\
            .all()

        json_trip['location_history'] = list(map(lambda x: x.to_json(), data))
    else:
        json_trip['location_history'] = []

    # calc public id
    # TODO encode an expiration time with this, validate
    json_trip['hashid'] = hashids.encode(trip.id)

    return ok_response(json_trip)


@org_realm('/trip/<id>', methods=['PUT'])
def trip_update(id):
    trip = Trip.query.get(id)

    if trip is None:
        raise ModelNotFoundError('trip not found')

    data = TripUpdateForm.validate(**request.form.to_dict())

    models = {
        'user_id': User,
        'route_id': Route,
        'device_id': Device,
    }

    for fieldname, model in models.items():
        model_id = request.form.get(fieldname)

        if model_id == '':
            setattr(trip, fieldname, None)
        if request.form.get(fieldname):
            obj = model.get(model_id)
            if obj is None:
                raise InvalidFieldError(fieldname)

            setattr(trip, fieldname, model_id)

            if fieldname == 'device_id':
                trip.fleet_id = obj.fleet

    fields = [
        'origin',
        'destination',
        'scheduled_departure',
        'scheduled_arrival',
        'notes',
        'notify_stop',
    ]

    for fieldname in fields:
        if request.form.get(fieldname):
            setattr(trip, fieldname, getattr(data, fieldname))

    sql.session.commit()
    trip_propagate(trip, 'update')

    return ok_response(trip.to_json())


@org_realm('/trip/<id>/control', methods=['PUT'])
def trip_control(id):
    ''' Just status changes '''
    trip = Trip.query.get(id)

    if trip is None:
        raise ModelNotFoundError('trip not found')

    status = request.form.get('status')

    if status == TripStatus.ONGOING.name:
        if trip.status != TripStatus.SCHEDULED:
            raise InvalidFieldError('status')

        if not trip.device_id:
            raise InvalidFieldError('status')

        trip.departure = datetime.now()
        trip.status = TripStatus.ONGOING

        trip_propagate(trip, 'trip-started')

    elif status == TripStatus.FINISHED.name:
        if trip.status != TripStatus.ONGOING:
            raise InvalidFieldError('status')

        trip.status = TripStatus.FINISHED
        trip.arrival = datetime.now()

        device = Device.get(trip.device_id)

        points = Position.query\
            .filter(Position.deviceid == device.traccar_id)\
            .filter(Position.fixtime.between(trip.departure, trip.arrival))\
            .all()

        points = list(map(lambda p: (p.latitude, p.longitude), points))

        if points:
            trip.polyline = polyline.encode(route_sampling(points, 200), 5)

        trip_propagate(trip, 'trip-finished')

    sql.session.commit()

    return ok_response(trip.to_json())


@org_realm('/trip/<id>', methods=['DELETE'])
def trip_delete(id):
    trip = Trip.query.get(id)

    if trip is None:
        raise ModelNotFoundError('trip not found')

    trip_propagate(trip, 'delete')

    sql.session.delete(trip)
    sql.session.commit()

    return ok_response(None), 204


def trip_propagate(trip, event):
    channel = '{}:lost_found'.format(trip.org_subdomain)

    message = {
        'event': event,
        'data': {
            'org_name': trip.org_subdomain,
            'trip': trip.to_json(),
        },
    }

    if trip.device_id:
        dev = Device.get(trip.device_id)

        if dev:
            channel = '{}:fleet:{}'.format(
                trip.org_subdomain, dev.fleet.get().id
            )

            # TODO device information is being sent twice
            message['data']['device'] = dev.to_json()

    if trip.route_id:
        route = Route.get(trip.route_id)

        if route:
            message['data']['route'] = route.to_json()

    if trip.user_id:
        user = User.get(trip.user_id)

        if user:
            message['data']['user'] = user.to_json()

    eng.redis.publish(channel, json.dumps(message))
