from flask import request, g

from fleety.db.models.bounded import Geofence, eng
from fleety.http.middleware import org_realm
from fleety.http.json import ok_response
from fleety.http.api_spec import redis_filtered_query


@org_realm('/geofence', methods=['GET'])
def geofence_list():
    return redis_filtered_query(Geofence, request.args)


@org_realm('/geofence', methods=['POST'])
def geofence_create():
    new_geofence = Geofence.validate(**request.form.to_dict()).save()

    return ok_response(new_geofence.to_json(**g.json_params)), 201


@org_realm('/geofence/<id>', methods=['GET'])
def geofence_read(id):
    geofence = Geofence.get_or_exception(id)

    return ok_response(geofence.to_json(**g.json_params))


@org_realm('/geofence/<id>', methods=['PUT'])
def geofence_update(id):
    geofence = Geofence.get_or_exception(id)

    geofence.update(**request.form.to_dict())

    return ok_response(geofence.to_json(**g.json_params))


@org_realm('/geofence/<id>', methods=['DELETE'])
def geofence_delete(id):
    geofence = Geofence.get_or_exception(id)

    geofence.delete()
    eng.lua.clear_related_permissions(
        keys=['user:members'],
        args=[geofence.permission()]
    )

    return ok_response({
        'data': None
    }), 204
