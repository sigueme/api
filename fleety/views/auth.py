from coralillo.errors import MissingFieldError, InvalidFieldError
from flask import request, redirect, g

from fleety import app, hashids
from fleety.auth.hashing import check_password
from fleety.db.models import User, Organization
from fleety.http.errors import AuthFieldError
from fleety.http.forms.auth import LoginForm, ConfirmForm
from fleety.http.forms.auth import KeyLoginForm
from fleety.http.json import ok_response
from fleety.http.middleware import basic_auth


@app.route('/auth/login', methods=['POST'])
def login():
    requestdata = LoginForm.validate(**request.form.to_dict())

    org = Organization.get_by('subdomain', requestdata.org_subdomain)

    if org is None:
        raise InvalidFieldError('org_subdomain')

    user = User.get_by('email', requestdata.email)

    if not user:
        raise AuthFieldError(
            field='email',
            reason='not_found',
        )

    if not check_password(requestdata.password, user.password):
        raise AuthFieldError(
            field='password',
            reason='invalid',
        )

    user.check_login(org)

    return ok_response({
        'session': user.to_session(org.subdomain),
        'org': org.to_json(),
    })


@app.route('/auth/key_login', methods=['POST'])
def key_login():
    requestdata = KeyLoginForm.validate(**request.form.to_dict())

    org = Organization.get_by('subdomain', requestdata.org_subdomain)

    if org is None:
        raise InvalidFieldError('org_subdomain')

    user = User.get_by('api_key', requestdata.api_key)

    if not user:
        raise AuthFieldError(
            field='api_key',
            reason='not_found',
        )

    user.check_login(org)

    return ok_response({
        'session': user.to_session(org.subdomain),
        'org': org.to_json(),
    })


@app.route('/auth/ping')
@basic_auth
def ping():
    if not g.user:
        return ok_response({
            'session': None,
            'org': None,
        })

    if not request.args.get('org_subdomain'):
        raise MissingFieldError('org_subdomain')

    org = Organization.get_by('subdomain', request.args.get('org_subdomain'))

    if org is None:
        raise InvalidFieldError('org_subdomain')

    return ok_response({
        'session': g.user.to_session(org.subdomain),
        'org': org.to_json(),
    })


@app.route('/auth/confirm')
def confirm():
    data = ConfirmForm.validate(**request.args.to_dict())

    try:
        used_id, expire_date = hashids.decode(data.token)
    except ValueError:
        raise InvalidFieldError('token')

    user = User.get_or_exception(hashids.encode(used_id))
    org = Organization.get_by_or_exception('subdomain', data.org)

    if org not in user.orgs:
        raise InvalidFieldError('org')

    user.confirmed = True
    user.save()

    return redirect('{protocol}://{subdomain}.getfleety.{extension}'.format(
        protocol=app.config['PROTOCOL'],
        subdomain=data.org,
        extension=app.config['DOMAIN_EXTENSION'],
    ))
