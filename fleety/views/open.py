from fleety import app, hashids, sql
from fleety.http.json import ok_response
from fleety.db.models.bounded import Device
from fleety.db.models.sql import Trip, TripStatus, Position
from fleety.http.errors import HttpNotFound
from datetime import timedelta
from fleety.helpers.geo import geocoding


@app.route('/open/track/<hash>')
def track_trip(hash):
    id = hashids.decode(hash)
    trip = Trip.query.get(id)

    if trip is None:
        raise HttpNotFound('trip')

    app.config['ORGANIZATION'] = trip.org_subdomain

    json_trip = trip.to_json()

    # fill device
    device = Device.get(trip.device_id)
    json_trip['device_name'] = device.code
    if device.name is not None:
        json_trip['device_name'] = device.name

    # Sample route
    departure = trip.departure
    sample_limit = device.last_update

    if trip.status == TripStatus.FINISHED:
        sample_limit = trip.arrival

    sample_freq = timedelta(hours=2)
    sample_error = timedelta(minutes=15)
    sample_time = departure.replace(
        minute=0,
        second=0,
        microsecond=0,
    ) + timedelta(hours=1)
    route_sample = []

    while sample_time < sample_limit:
        position = sql.session\
                      .query(Position)\
                      .filter(Position.deviceid == device.traccar_id)\
                      .filter(Position.fixtime <= sample_time)\
                      .order_by(Position.fixtime.desc())\
                      .limit(1)\
                      .all()

        if not position:
            sample_time = sample_time + sample_freq
            continue

        position = position[0]

        if sample_time - position.fixtime > sample_error:
            sample_time = sample_time + sample_freq
            continue

        address = geocoding(position.latitude, position.longitude)
        if address is None:
            sample_time = sample_time + sample_freq
            continue

        route_sample.append({
            'time': sample_time,
            'address': address,
        })

        sample_time = sample_time + sample_freq

    if trip.status == TripStatus.ONGOING:
        address = geocoding(device.last_pos.lat, device.last_pos.lon)
        if address is not None:
            route_sample.append({
                'time': device.last_update,
                'address': address,
            })

    json_trip['route_sample'] = route_sample

    return ok_response(json_trip)
