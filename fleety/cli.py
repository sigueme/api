from coralillo.datamodel import Location
from datetime import datetime
from functools import reduce
from random import random
from itertools import cycle as itercycle
import click
from flask import json
import time
import requests

from fleety import app, sql, eng
from fleety.db.models import User, Organization, Subscription
from fleety.db.models.sql import Log, Trip, Position
from fleety.db.models.bounded import Geofence, Device, Fleet
from fleety.core import id_function


@app.cli.command()
def seed():
    """Seed the redis database"""
    # Clear the database
    eng.lua.drop(args=['*'])
    sql.session.query(Position).delete()
    sql.session.query(Trip).delete()
    sql.session.query(Log).delete()
    sql.session.commit()

    # Seed it again
    org = Organization(
        name='Local Inc.',
        subdomain='local',
        code='12345678',
    ).save()

    user1 = User(
        name='Admin',
        last_name='Wayers',
        email='getfleety@mailinator.com',
        password='123456',
        api_key='da39a3ee5e6b4b0d3255bfef95601890afd80709',
        is_active=True,
    ).save()
    user1.orgs.set([org])
    user1.allow('local')

    click.echo('Your api_key: {}'.format(user1.api_key))
    click.echo('Organization code: {}'.format(org.code))

    app.config['ORGANIZATION'] = org.subdomain

    fleet = Fleet(name='The best fleet', abbr='TBF').save()

    loc1 = Location(-103.3590316772461, 20.72400644605942)
    dev1 = Device(name='T1', code='00001', traccar_id=1, last_pos=loc1).save()

    dev1.fleet.set(fleet)

    loc2 = Location(-103.3995223045349, 20.60675661487856)
    dev2 = Device(name='T2', code='00002', traccar_id=2, last_pos=loc2).save()
    dev2.fleet.set(fleet)

    loc3 = Location(-103.36572647094727, 20.691731620265905)
    dev3 = Device(name='T3', code='00003', traccar_id=3, last_pos=loc3).save()
    dev3.fleet.set(fleet)

    Geofence(
        name='Tracsa Matriz',
        polyline='ilw|BrcbvR}BcBqAeAGLuFiEuA_AO\\WT_@Vc@Ps@VsAXaBb@i@R_@^_@d@f'
                 'BtAnCpBbBjAp@^PFrANv@D~@Aj@CvAGr@Cv@EVSRKLBp@o@ZuA',
    ).save()

    sub1 = Subscription(
        channel=fleet.fqn(),
        event='geofence-enter',
        handler='Email',
        params={},
    ).save()
    sub2 = Subscription(
        channel=fleet.fqn(),
        event='geofence-leave',
        handler='Email',
        params={},
    ).save()
    sub1.user.set(user1)
    sub2.user.set(user1)

    click.echo('Database seeded')


@app.cli.command()
@click.argument('filename')
@click.argument('code')
@click.argument('id')
@click.option('--cycle/--no-cycle', default=False)
@click.option('--sleep', type=float, default=1,
              help='seconds to wait between positions')
@click.option('--subdomain', default='local',
              help='the subdomain of the organization to use')
@click.option('--statuses/--no-statuses', default=False)
def play(filename, code, id, sleep, subdomain, cycle, statuses):
    assert len(code) == 5, 'Code must be 5 characters long'
    assert reduce(
        lambda x, y: x and y,
        map(
            lambda c: c in '0123456789',
            code
        ),
        True
    ), 'Code must be only numbers'

    coordinates = json.load(
        open(filename)
    )['features'][0]['geometry']['coordinates']
    org = Organization.get_by('subdomain', subdomain)

    app.config['ORGANIZATION'] = subdomain

    Device.get_by('code', code)
    chances = 1 / 10 if statuses else 0
    eventiter = itercycle(['moving', 'stopped', 'offline', 'alarm'])
    curevent = next(eventiter)
    itemiter = itercycle(enumerate(coordinates))
    index, coordinate = next(itemiter)

    while True:
        eng.lua.device_motion(args=[
            str(coordinate[0]),
            str(coordinate[1]),
            org.code + code,
            id,
            int(time.time()),
            int(time.time()),
            id_function(),
            id_function(),
            '0',
            datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ'),
            curevent,
        ])

        sql.session.add(Position(
            deviceid=id,
            latitude=coordinate[1],
            longitude=coordinate[0],
            devicetime=datetime.now(),
            servertime=datetime.now(),
            fixtime=datetime.now(),
            valid=True,
            altitude=0,
            speed=0,
            course=0,
        ))
        sql.session.commit()

        print('{} sent {}, {}'.format(index, coordinate[0], coordinate[1]))

        time.sleep(sleep)
        index, coordinate = next(itemiter)

        if random() < chances:
            curevent = next(eventiter)

        if not cycle and index == len(coordinates) - 1:
            break


@app.cli.command()
@click.argument('code')
@click.argument('alarm')
def alarm(code, alarm):
    res = requests.post(
        'http://api.getfleety.{}/webhook/{}/traccar/event'.format(
            app.config['DOMAIN_EXTENSION'],
            app.config['WEBHOOK_KEY'],
        ), data=json.dumps({
            'event': {
                'type': 'alarm',
                'attributes': {
                    'alarm': alarm,
                },
                'serverTime': datetime.now().strftime(
                    '%Y-%m-%dT%H:%M:%S.%f+0000'
                ),
            },
            'device': {
                'id': 5,
                "uniqueId": code,
                "status": "online",
                "lastUpdate": datetime.now().strftime(
                    '%Y-%m-%dT%H:%M:%S.%f+0000'
                ),
            },
        }))

    if res.status_code != 200:
        raise click.ClickException('Response failed with status {}'.format(
            res.status_code
        ))


@app.cli.command()
@click.argument('code')
@click.argument('longitude', type=float)
@click.argument('latitude', type=float)
@click.option('--course', type=int, default=90)
def move(code, longitude, latitude, course):
    print(repr(longitude))
    print(repr(latitude))
    res = requests.post(
        'http://api.getfleety.{}/webhook/{}/traccar/event'.format(
            app.config['DOMAIN_EXTENSION'],
            app.config['WEBHOOK_KEY'],
        ), data=json.dumps({
            'event': {
                "type": "deviceMoving",
                "serverTime": datetime.now().strftime(
                    '%Y-%m-%dT%H:%M:%S.%f+0000'
                ),
            },
            'device': {
                "id": 1,
                "uniqueId": '12345678' + code,
                "status": "online",
                "lastUpdate": datetime.now().strftime(
                    '%Y-%m-%dT%H:%M:%S.%f+0000'
                ),
            },
            'position': {
                'altitude': 1400,
                'course': course,
                'deviceTime': datetime.now().strftime(
                    '%Y-%m-%dT%H:%M:%S.%f+0000'
                ),
                'fixTime': datetime.now().strftime(
                    '%Y-%m-%dT%H:%M:%S.%f+0000'
                ),
                'latitude': latitude,
                'longitude': longitude,
                'outdated': False,
                'protocol': 'pt502',
                'serverTime': datetime.now().strftime(
                    '%Y-%m-%dT%H:%M:%S.%f+0000'
                ),
                'speed': 15,  # in knots
                'valid': True,
            },
        }))

    if res.status_code != 200:
        raise click.ClickException('Response failed with status {}'.format(
            res.status_code
        ))


@app.cli.command()
@click.argument('org')
def create(org):
    ''' Create the specified organization '''
    neworg = Organization.validate(name=org, subdomain=org).save()
    click.echo('Created org {}'.format(neworg))


def reset_func(subdomain):
    org = Organization.get_by('subdomain', subdomain)

    if org is None:
        return click.echo('Organization not found')

    org.users.fill()

    for user in org.users:
        if user.orgs.count() == 1:
            user.delete()

    eng.lua.drop(args=['{}:*'.format(org.subdomain)])


@app.cli.command()
@click.argument('subdomain')
def reset(subdomain):
    ''' Resets a organization to its original state '''
    reset_func(subdomain)
