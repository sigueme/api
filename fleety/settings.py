import os

# Flask Settings
SECRET_KEY = os.environ.get('SECRET_KEY', 'a long and random string')

# This is for communication with the GPS backend
WEBHOOK_KEY = 'change me'

# Database settings
REDIS_URL = os.environ.get('REDIS_URL', 'redis://redis/0')
SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI', 'postgresql://fleety@postgis-postgis/fleety')
SQLALCHEMY_TRACK_MODIFICATIONS = False

# CORS
CORS_ORIGINS = '.*'

# protected domains
FLEETY_RESTRICTED_DOMAINS = [
    'app',
    'api',
    'dev',
    'gps',
    'docs',
    'help',
    'fleety',
    'meta',
]

# Available (supported) API versions
FLEETY_API_VERSIONS = [
    'v1',
]

# Available handlers for events
FLEETY_BROKER_HANDLERS = [
    'Email',
    'Telegram',
    # 'Sms',
]

FLEETY_REPORT_BUILDERS = [
    'TripCSV',
    'DeviceGeoJSON',
]

# Error messages will be sent to this channel
FLEETY_ERROR_CHANNEL = 'meta:server:fleety-back'

# For unique id generation we need to know the node identifier.
# Currently a 4bit integer (0-15)
FLEETY_NODE = 1

# Default limit for objects in queries
API_LIST_LIMIT = 50

# When an event is received from traccar, we set one of this statuses on the
# device
TRACCAR_EVENT_MAPPING = {
    "deviceOnline": 'stopped',
    "deviceUnknown": 'offline',
    "deviceOffline": 'offline',
    "deviceMoving": 'moving',
    "deviceStopped": 'stopped',
    "deviceOverspeed": 'alarm',
    "deviceFuelDrop": 'alarm',
}

# Timezone
TIMEZONE = 'UTC'

# This changes from environmet to environment
PROTOCOL = 'http'
DOMAIN_EXTENSION = 'com'

# Google static map api key
GOOGLE_API_STATIC_MAPS_KEY = '<google static map api key>'
GOOGLE_API_GEOCODING_KEY = '<google geocoding api key>'

# For sending messages to telegram
TELEGRAM_BOT_KEY = '<the key>'
