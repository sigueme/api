from flask import json


def get_json(data):
    if data is None:
        return None

    return json.loads(data)
