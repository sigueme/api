import unicodedata
import re
from functools import partial


def normalize(s):
    s = s.strip()
    s = s.lower()
    s = re.sub(r'\s+', ' ', s)
    s = ''.join(c for c in unicodedata.normalize('NFD', s)
                if unicodedata.category(c) != 'Mn')

    return s


def word_subsequence(text, word):
    pnt = 0
    for char in text:
        if word[pnt] == char:
            pnt += 1

        if pnt == len(word):
            return True

        if char == ' ':
            pnt = 0

    return False


def submatch(text, query):
    assert type(text) == str, 'arg 0 is not a string'
    assert type(query) == str, 'arg 1 is not a string'

    text = normalize(text)
    query = normalize(query)

    terms = query.split()
    return all(map(partial(word_subsequence, text), terms))
