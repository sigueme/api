def get_iso(date):
    """ gets the iso representation of a datetime object """
    if date is None:
        return None

    return date.replace(microsecond=0).isoformat() + 'Z'
