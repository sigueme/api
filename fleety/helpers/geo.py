from math import radians, cos, sin, asin, sqrt
from fleety import app
import requests

heading_funcs = {
    'N': lambda x: 0 <= x < radians(90) or radians(270) < x <= radians(360),
    'NE': lambda x: 0 < x < radians(90),
    'E': lambda x: 0 < x < radians(180),
    'SE': lambda x: radians(90) < x < radians(180),
    'S': lambda x: radians(90) < x < radians(270),
    'SW': lambda x: radians(180) < x < radians(270),
    'W': lambda x: radians(180) < x < radians(360),
    'NW': lambda x: radians(270) < x < radians(360),
}


def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * asin(sqrt(a))
    r = 6371000  # Radius of earth in meters.
    return c * r


def route_sampling(route, sample_size):
    if len(route) < 2:
        return route

    distance = 0

    for i in range(1, len(route)):
        p1 = route[i]
        p2 = route[i - 1]
        local = haversine(p1[1], p1[0], p2[1], p2[0])
        distance += local

    step = distance / sample_size
    sampling = [0]
    step_dist = 0

    for i in range(1, len(route) - 1):
        p1 = route[i]
        p2 = route[i - 1]
        local = haversine(p1[1], p1[0], p2[1], p2[0])

        step_dist += local
        if step_dist - step > 1e-6:
            sampling.append(i)
            step_dist -= step

    sampling.append(len(route) - 1)

    return list(map(lambda x: route[x], sampling))


def geocoding(lat, lon):
    latlng = '{},{}'.format(lat, lon)

    uri = 'https://maps.googleapis.com/maps/api/geocode/json?' \
          'latlng={latlng}&key={key}'.format(
              latlng=latlng,
              key=app.config['GOOGLE_API_GEOCODING_KEY'],
          )

    res = requests.get(uri)

    if res.status_code != 200:
        raise Exception('Got response status {} in geocoding request'.format(
            res.status_code
        ))

    data = res.json()
    results = data['results']

    if 'error_message' in data:
        raise Exception('Got error: "{}" from geocoding request'.format(
            data['error_message']
        ))

    for result in results:
        if 'administrative_area_level_2' in result['types']:
            return result['formatted_address']

    return None
