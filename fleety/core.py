import time
import random
from fleety import app
from fleety import hashids

_last_timestamp = None


def id_function():
    """Generate a UUID from a host ID, sequence number, and the current time.
    If 'node' is not given, getnode() is used to obtain the hardware
    address.  If 'clock_seq' is given, it is used as the sequence number;
    otherwise a random 14-bit sequence number is chosen."""

    global _last_timestamp

    timestamp = int(time.time() * 1e3)  # Miliseconds since epoch

    if _last_timestamp is not None and timestamp <= _last_timestamp:
        timestamp = _last_timestamp + 1

    _last_timestamp = timestamp

    clock_seq = random.getrandbits(12)  # instead of stable storage
    node = app.config['FLEETY_NODE']
    num = (timestamp << 16) | (clock_seq << 4) | node

    return hashids.encode(num)
