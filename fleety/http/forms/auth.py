from coralillo import Form, fields
from fleety import eng


class LoginForm(Form):
    email = fields.Text()
    password = fields.Text()
    org_subdomain = fields.Text()

    class Meta:
        engine = eng


class KeyLoginForm(Form):
    api_key = fields.Text()
    org_subdomain = fields.Text()

    class Meta:
        engine = eng


class ConfirmForm(Form):
    token = fields.Text()
    org = fields.Text()

    class Meta:
        engine = eng


class RegisterForm(Form):
    organization = fields.Text()
    subdomain = fields.Text()
    name = fields.Text()
    last_name = fields.Text()
    email = fields.Text()
    password = fields.Hash()

    class Meta:
        engine = eng
