from coralillo import Form, fields
from coralillo.validation import validation_rule
from coralillo.errors import InvalidFieldError
from crontab import CronTab

from fleety import eng, app
from fleety.http.forms.fields import Permission, PostgisPoint
from fleety.http.errors import ConflictingScheduleError


class BoundedForm(Form):
    class Meta:
        engine = eng


class TraccarPositionForm(BoundedForm):
    lon = fields.Text()
    lat = fields.Text()
    code = fields.Text()
    id = fields.Text()
    device_time = fields.Text()


class UpdateLastReadForm(BoundedForm):
    last_read = fields.Datetime()


class PermissionForm(BoundedForm):
    perm = Permission()


class TripForm(BoundedForm):
    origin = fields.Text()
    origin_pos = PostgisPoint()
    destination = fields.Text()
    destination_pos = PostgisPoint()
    scheduled_departure = fields.Datetime()
    scheduled_arrival = fields.Datetime()
    user_id = fields.Text(required=False)
    device_id = fields.Text(required=False)
    route_id = fields.Text(required=False)
    notify_stop = fields.Integer(required=False)

    def to_dict(self):
        return {
            'origin': self.origin,
            'origin_pos': self.origin_pos,
            'destination': self.destination,
            'destination_pos': self.destination_pos,
            'scheduled_departure': self.scheduled_departure,
            'scheduled_arrival': self.scheduled_arrival,
            'user_id': self.user_id,
            'device_id': self.device_id,
            'route_id': self.route_id,
            'notify_stop': self.notify_stop,
        }


class TripUpdateForm(BoundedForm):
    origin = fields.Text(required=False)
    destination = fields.Text(required=False)
    scheduled_departure = fields.Datetime(required=False)
    scheduled_arrival = fields.Datetime(required=False)
    user_id = fields.Text(required=False)
    device_id = fields.Text(required=False)
    route_id = fields.Text(required=False)
    notes = fields.Text(required=False)
    notify_stop = fields.Integer(required=False, default=-1)


class ReportCreateForm(BoundedForm):
    builder = fields.Text()
    name = fields.Text(required=False)

    @validation_rule
    def validate_builder(data):
        if data.builder not in app.config['FLEETY_REPORT_BUILDERS']:
            raise InvalidFieldError('builder')


class ReportSchedulerCreateForm(BoundedForm):
    builder = fields.Text()
    name = fields.Text(required=False)
    run_at = fields.Datetime(required=False)
    cron = fields.Text(required=False)

    @validation_rule
    def validate_builder(data):
        if data.builder not in app.config['FLEETY_REPORT_BUILDERS']:
            raise InvalidFieldError('builder')

    @validation_rule
    def enforce_schedules(data):
        ''' ensures that one and only one of 'cron' and 'run_at' is sent '''
        if (data.run_at and data.cron) or (not data.run_at and not data.cron):
            raise ConflictingScheduleError(field='run_at')

    @validation_rule
    def validate_cron(data):
        ''' checks the format of cron field '''
        if data.cron:
            try:
                CronTab(data.cron)
            except ValueError:
                raise InvalidFieldError('cron')


class ReportSchedulerUpdateForm(BoundedForm):
    name = fields.Text(required=False)
    run_at = fields.Datetime(required=False)
    cron = fields.Text(required=False)
    active = fields.Bool()

    @validation_rule
    def enforce_schedules(data):
        ''' ensures that one and only one of 'cron' and 'run_at' is sent '''
        if (data.run_at and data.cron) or (not data.run_at and not data.cron):
            raise ConflictingScheduleError(field='run_at')

    @validation_rule
    def validate_cron(data):
        ''' checks the format of cron field '''
        if data.cron:
            try:
                CronTab(data.cron)
            except ValueError:
                raise InvalidFieldError('cron')
