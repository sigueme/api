from coralillo import fields
from coralillo.errors import MissingFieldError, InvalidFieldError
from coralillo.utils import camelCase
from coralillo.fields import Location
from flask import g
from fleety.db import models
from fleety.db.models import bounded
import re


class Permission(fields.Text):

    def validate(self, value, redis=None):
        if not value:
            raise MissingFieldError(self.name)
        if type(value) != str:
            raise InvalidFieldError(self.name)

        value = value.strip()

        match = re.match(
            '^(?P<objspec>(?P<org>[a-z_]+)(:(?P<classname>[a-z_]+)'
            '(:(?P<id>[a-zA-Z0-9]{12}))?)?)(/(?P<type>view))?$', value,
            flags=re.ASCII
        )

        if not match:
            raise InvalidFieldError(self.name)

        if match.group('org') != g.org.subdomain:
            raise InvalidFieldError(self.name)

        if not match.group('classname'):
            return value

        clsname = camelCase(match.group('classname'))
        obj_id = match.group('id')

        try:
            cls = getattr(bounded, clsname)
        except AttributeError:
            cls = None

        if not cls:
            try:
                cls = getattr(models, clsname)
            except AttributeError:
                raise InvalidFieldError(self.name)

        if obj_id:
            try:
                cls.get_or_exception(obj_id)
            except MissingFieldError:
                raise InvalidFieldError(self.name)

        return value


class PostgisPoint(Location):

    def validate(self, value, redis):
        value = super().validate(value, redis)

        return 'POINT({} {})'.format(value.lon, value.lat)
