from coralillo.errors import ValidationErrors, ModelNotFoundError, BadField
from flask import jsonify, json, request, g
import logging
import sys

from fleety import app, eng
from fleety.http.errors import HttpUnauthorized, AuthFieldError
from fleety.http.errors import HttpClientError, HttpNotFound


@app.errorhandler(BadField)
def handle_bad_field(e):
    return jsonify({'errors': [e.to_json()]}), 400


@app.errorhandler(ValidationErrors)
def handle_validation_errors(e):
    return jsonify({'errors': e.to_json()}), 400


@app.errorhandler(ModelNotFoundError)
def handle_notfound(e):
    return jsonify({'errors': [{
        'i18n': 'errors.http.not_found',
        'detail': 'resource not found',
    }]}), 404


@app.errorhandler(HttpNotFound)
def handle_http_not_found(e):
    return jsonify({'errors': [{
        'i18n': 'errors.http.not_found',
        'detail': '{} not found'.format(e.resource),
        'resource': e.resource,
    }]}), 404


@app.errorhandler(HttpClientError)
def handle_client_error(e):
    return jsonify({'errors': [{
        'i18n': 'errors.http.' + e.code,
        'detail': str(e),
    }]}), e.status_code


@app.errorhandler(AuthFieldError)
def handle_auth_field(e):
    return jsonify({'errors': [{
        'i18n': 'errors.http401.' + e.reason,
        'field': e.field,
        'detail': str(e),
    }]}), 401


@app.errorhandler(HttpUnauthorized)
def handle_unauthorized(e):
    return jsonify({'errors': [{
        'i18n': 'errors.http.unauthorized',
        'detail': 'Unauthorized',
    }]}), 401, {
        'WWW-Authenticate': 'Basic realm="User Visible Realm"',
    }


@app.errorhandler(403)
def handle_forbidden(e):
    return jsonify({'errors': [{
        'i18n': 'errors.http.forbidden',
        'detail': 'not enough permissions for this',
    }]}), 403


@app.errorhandler(404)
def handle_404(e):
    return jsonify({'errors': [{
        'i18n': 'errors.http.not_found',
        'detail': 'resource not found',
    }]}), 404


@app.errorhandler(405)
def handle_methodnotallowed(e):
    return jsonify({'errors': [{
        'i18n': 'errors.http.method_not_allowed',
        'detail': 'method not allowed',
    }]}), 405


class FleetyBrokerHandler(logging.Handler):

    def __init__(self, redis=None, channel=None):
        logging.Handler.__init__(self)

        self.redis = redis
        self.channel = channel

    def emit(self, record):
        try:
            traceback = self.format(record)

            message = {
                'event': 'server-error',
                'data': {
                    'user': {
                        'name': g.user.name,
                        'email': g.user.email,
                        'id': g.user.name,
                    } if hasattr(g, 'user') else None,
                    'traceback': traceback,
                    'get_data': self.parse_dict(request.args),
                    'post_data': self.parse_dict(request.form),
                    'method': request.method,
                    'path': request.path,
                    'org_name': g.org.subdomain if hasattr(g, 'org') else None,
                },
            }

            self.redis.publish(self.channel, json.dumps(message))
        except Exception:
            self.handleError(record)

    def parse_dict(self, data):
        return '\n'.join(
            '{}: {}'.format(key, value)
            for key, value in data.items()
        )


# Report errors to the broker
if not app.debug:
    # Debug messages to stderr
    formatter = logging.Formatter(
        fmt='[%(asctime)s] [%(process)d] [%(levelname)s] %(message)s - '
        '%(pathname)s:%(lineno)d', datefmt='%Y-%m-%d %H:%M:%S %z'
    )

    file_handler = logging.StreamHandler(stream=sys.stderr)
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(formatter)

    # Send messages to email
    broker_handler = FleetyBrokerHandler(
        redis=eng.redis,
        channel=app.config['FLEETY_ERROR_CHANNEL'],
    )
    broker_handler.setLevel(logging.ERROR)

    app.logger.addHandler(broker_handler)
    app.logger.addHandler(file_handler)
