from coralillo.errors import InvalidFieldError
from coralillo.datamodel import Location
from itertools import islice
from flask import g

from fleety import app, eng
from fleety.http.json import ok_response
from fleety.helpers.geo import heading_funcs
import re


def _apply_fields(json_data, fields=None):
    if fields is None:
        return json_data

    return {
        key: value
        for key, value in json_data.items()
        if key in fields
    }


def sql_filtered_query(model, query, args, default_order='id.asc'):
    # apply filters
    for arg in args:
        try:
            fieldname, query_func = arg.split('__', 1)
        except ValueError:
            fieldname, query_func = arg, 'eq'

        if not hasattr(model, fieldname):
            continue

        value = args.get(arg)

        if query_func == 'eq':
            query = query.filter(getattr(model, fieldname) == value)
        elif query_func == 'startswith':
            query = query.filter(getattr(model, fieldname).startswith(value))
        elif query_func == 'endswith':
            query = query.filter(getattr(model, fieldname).endswith(value))
        elif query_func == 'lt':
            query = query.filter(getattr(model, fieldname) < value)
        elif query_func == 'lte':
            query = query.filter(getattr(model, fieldname) <= value)
        elif query_func == 'gt':
            query = query.filter(getattr(model, fieldname) > value)
        elif query_func == 'gte':
            query = query.filter(getattr(model, fieldname) >= value)
        elif query_func == 'in':
            query = query.filter(
                getattr(model, fieldname).in_(value.split(','))
            )
        elif query_func == 'between':
            try:
                low, high = value.split(',')
            except ValueError:
                raise InvalidFieldError(arg)
            query = query.filter(getattr(model, fieldname).between(low, high))

    # apply ordering
    orderarg = args.get('order_by')
    criterion = []

    if orderarg:
        orderspec = orderarg
    else:
        orderspec = default_order

    for orderpart in orderspec.split(','):
        try:
            fieldname, direction = orderpart.split('.', 1)
        except ValueError:
            fieldname, direction = orderpart, 'asc'

        try:
            assert direction in ['asc', 'desc']
            assert hasattr(model, fieldname)
        except AssertionError:
            raise InvalidFieldError('order_by')

        criterion.append(getattr(getattr(model, fieldname), direction)())

    if criterion:
        query = query.order_by(*criterion)

    # If only requests a count, return the count
    if args.get('count'):
        return ok_response(query.count())

    # apply limits
    else:
        limit = args.get('limit', app.config['API_LIST_LIMIT'])
        query = query.limit(limit)

    # return the results
    fields = args.get('fields')

    if fields is not None:
        fields = list(filter(
            lambda x: x,
            map(
                lambda x: x.strip(),
                fields.split(',')
            )
        ))

    return ok_response(list(map(
        lambda l: _apply_fields(l.to_json(), fields=fields),
        query.all()
    )))


def _make_filter(fieldname, query_func, value):
    # These filters expect values of a different type than the one found in
    # the object
    CUSTOM_VALUE_FILTERS = ['heading', 'in', 'between', 'radius', ]

    def actual_filter(obj):
        obj_val = getattr(obj, fieldname)

        if obj_val is None:
            return False

        if query_func not in CUSTOM_VALUE_FILTERS:
            exp_val = getattr(type(obj), fieldname).validate(obj_val, value, eng.redis)

        if query_func == 'eq':
            return obj_val == exp_val
        elif query_func == 'startswith':
            return obj_val.startswith(exp_val)
        elif query_func == 'endswith':
            return obj_val.endswith(exp_val)
        elif query_func == 'contains':
            return exp_val in obj_val
        elif query_func == 'lt':
            return obj_val < exp_val
        elif query_func == 'lte':
            return obj_val <= exp_val
        elif query_func == 'gt':
            return obj_val > exp_val
        elif query_func == 'gte':
            return obj_val >= exp_val

        # These filters compute their own exp_val
        elif query_func == 'in':
            exp_val = list(filter(
                lambda x: x,
                map(
                    lambda v: v.strip(),
                    value.split(',')
                )
            ))

            return obj_val in exp_val
        elif query_func == 'between':
            try:
                low, high = list(map(
                    lambda v: getattr(type(obj), fieldname).validate(
                        obj_val, v, eng.redis
                    ),
                    filter(
                        lambda x: x,
                        map(
                            lambda v: v.strip(),
                            value.split(',')
                        )
                    )
                ))
            except ValueError:
                raise InvalidFieldError('{}__{}'.format(fieldname, query_func))

            return low <= obj_val <= high
        elif query_func == 'heading':
            exp_val = value.strip()

            if exp_val not in heading_funcs:
                raise InvalidFieldError('{}__{}'.format(fieldname, query_func))

            return heading_funcs[exp_val](obj_val)
        elif query_func == 'radius':
            try:
                lon, lat, radius = value.split(',', 2)
                lon, lat = float(lon), float(lat)
                matches = re.match(r'^(\d+)(m|km)', radius)

                if matches is None:
                    raise ValueError('')

                radius = int(matches.group(1)) * {
                    'm': 1,
                    'km': 1000,
                }[matches.group(2)]

                return getattr(obj, fieldname).distance(
                    Location(lon, lat)
                ) <= radius
            except ValueError:
                raise InvalidFieldError('{}__{}'.format(fieldname, query_func))

    return actual_filter


def _make_sort_key(sortfield):
    def key(obj):
        obj_val = getattr(obj, sortfield)

        if obj_val is not None:
            return obj_val

        return getattr(type(obj), sortfield).nullval()

    return key


def redis_filtered_query(model, args, default_order='created_at.asc'):
    data = model.all()

    # apply filters
    for arg in args:
        try:
            fieldname, query_func = arg.split('__', 1)
        except ValueError:
            fieldname, query_func = arg, 'eq'

        if not hasattr(model, fieldname):
            continue

        value = args.get(arg)

        data = filter(_make_filter(fieldname, query_func, value), data)

    # apply ordering
    orderarg = args.get('order_by')

    if orderarg:
        try:
            sortfield, direction = orderarg.split('.', 1)
        except ValueError:
            sortfield, direction = orderarg, 'asc'
    else:
        sortfield, direction = default_order.split('.')

    try:
        assert direction in ['asc', 'desc']
        assert hasattr(model, sortfield)
    except AssertionError:
        raise InvalidFieldError('order_by')

    data = sorted(data, key=_make_sort_key(sortfield))

    # If only requests a count, return the count
    if args.get('count'):
        return ok_response(len(data))

    # apply limits
    else:
        limit = args.get('limit', app.config['API_LIST_LIMIT'])
        data = islice(data, 0, limit)

    # return the results
    return ok_response(list(map(
        lambda l: l.to_json(**g.json_params),
        data
    )))
