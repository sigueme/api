from coralillo.errors import InvalidFieldError
import datetime


def parse_range(rangespec, fieldname):
    if not rangespec:
        return None

    try:
        pieces = list(map(int, rangespec.split('+')))
    except ValueError:
        raise InvalidFieldError(fieldname)

    return (pieces[0], sum(pieces))


def parse_datetime(value, fieldname):
    '''
    Parse data obtained from a request in ISO 8061 and returns it in Datetime
    data type
    '''

    if value is None:
        return None

    if isinstance(value, datetime.date):
        return value

    if type(value) == str:
        try:
            value = datetime.datetime.strptime(value, '%Y-%m-%dT%H:%M:%SZ')
        except ValueError:
            raise InvalidFieldError('Invalid ISO format date')

        return value

    raise InvalidFieldError('datetime source not handled')


def parse_int(number, fieldname):
    try:
        return int(number)
    except ValueError:
        raise InvalidFieldError(fieldname)
