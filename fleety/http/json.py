'''
Implements http://jsonapi.org/format/
'''
from flask import jsonify


def ok_response(data):
    return jsonify({
        'data': data,
    })
