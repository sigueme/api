from functools import wraps
from flask import request, g
from fleety.db.models import User, Organization
from fleety import app
from fleety.http.errors import HttpNotFound, HttpUnauthorized, HttpForbidden


def org_realm(rule, **options):
    def decorator(view):
        def wrapper(v):
            @wraps(v)
            def decorated_view(*args, **kwargs):
                # check api version
                api_version = kwargs.pop('api')

                if api_version not in app.config.get('FLEETY_API_VERSIONS', []):
                    raise HttpNotFound(resource='api_version')

                g.api_version = api_version

                # check organization
                org_name = kwargs.pop('org')

                org = Organization.get_by('subdomain', org_name)

                if org is None:
                    raise HttpNotFound(resource='organization')

                app.config['ORGANIZATION'] = org_name
                g.org = org

                # check auth
                auth = request.authorization

                if auth is None:
                    raise HttpUnauthorized('no auth provided')

                user = User.get_by('api_key', auth.password)

                if user is None:
                    raise HttpUnauthorized('user not found')

                if user not in org.users and not user.is_god:
                    raise HttpForbidden('user doesn\'t belong to organization')

                g.user = user

                # useful stuff for APIs
                g.json_params = dict()

                for thing in ('include',):
                    if request.args.get(thing):
                        g.json_params[thing] = list(filter(
                            lambda x: x,
                            map(
                                lambda x: x.strip(),
                                request.args.get(thing).split(',')
                            )
                        ))

                return v(*args, **kwargs)
            return decorated_view

        wrapped = wrapper(view)

        app.add_url_rule('/<api>/<org>' + rule, None, wrapped, **options)

        return wrapped

    return decorator


def webhook(rule, **options):
    def decorator(view):
        def wrapper(v):
            ''' Restricts traffic to the ones providing the right key '''
            @wraps(v)
            def decorated_view(*args, **kwargs):
                key = kwargs.pop('key')

                if key != app.config['WEBHOOK_KEY']:
                    raise HttpUnauthorized('invalid webhook key')

                return v(*args, **kwargs)
            return decorated_view

        wrapped = wrapper(view)

        app.add_url_rule('/webhook/<key>' + rule, None, wrapped, **options)

        return wrapped

    return decorator


def basic_auth(view):
    @wraps(view)
    def decorated(*args, **kwargs):
        auth = request.authorization

        if auth is None:
            g.user = None
        else:
            user = User.get_by('api_key', auth.password)
            g.user = user

        return view(*args, **kwargs)

    return decorated
