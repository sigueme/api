from coralillo.errors import BadField


# 4xx
class HttpClientError(Exception):
    pass


# 400
class HttpBadRequest(HttpClientError):
    code = 'bad_request'
    status_code = 400


class WantsJsonError(HttpBadRequest):
    pass


class DeviceNotReady(HttpBadRequest):
    pass


# 401
class HttpUnauthorized(HttpClientError):
    code = 'unauthorized'
    status_code = 401


class AuthFieldError(HttpUnauthorized):

    def __init__(self, field, reason):
        super().__init__('{} {}'.format(reason, field))
        self.field = field
        self.reason = reason


# 403
class HttpForbidden(HttpClientError):
    code = 'forbidden'
    status_code = 403


# 404
class HttpNotFound(HttpClientError):
    code = 'not_found'
    status_code = 404

    def __init__(self, resource):
        super().__init__('{} not found'.format(resource))
        self.resource = resource


# 409
class HttpConflict(HttpClientError):
    code = 'conflict'
    status_code = 409


class NoMoreDeviceCodes(HttpConflict):
    pass


class ConflictingScheduleError(BadField):
    message = 'Must have exactly one of run_at or cron fields'
    errorcode = 'conflicting_schedule'
