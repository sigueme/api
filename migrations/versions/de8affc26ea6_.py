"""check other not nulls on the databse

Revision ID: de8affc26ea6
Revises: a944935caa01
Create Date: 2018-01-12 21:55:49.819662

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'de8affc26ea6'
down_revision = 'a944935caa01'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('report_scheduler', 'org_subdomain',
               existing_type=sa.VARCHAR(),
               nullable=False)
    op.alter_column('trip', 'org_subdomain',
               existing_type=sa.VARCHAR(),
               nullable=False)


def downgrade():
    op.alter_column('trip', 'org_subdomain',
               existing_type=sa.VARCHAR(),
               nullable=True)
    op.alter_column('report_scheduler', 'org_subdomain',
               existing_type=sa.VARCHAR(),
               nullable=True)
