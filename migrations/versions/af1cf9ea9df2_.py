"""empty message

Revision ID: af1cf9ea9df2
Revises: 31beed9ee0c0
Create Date: 2017-11-07 20:48:14.605687

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'af1cf9ea9df2'
down_revision = '31beed9ee0c0'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('trip', sa.Column('fleet_id', sa.String(), nullable=True))

def downgrade():
    op.drop_column('trip', 'fleet_id')
