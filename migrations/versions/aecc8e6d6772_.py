"""Add url and failed_reason columns to report

Revision ID: aecc8e6d6772
Revises: 9a75a0849bf7
Create Date: 2018-01-31 21:47:04.280694

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'aecc8e6d6772'
down_revision = '9a75a0849bf7'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('report', sa.Column('failed_reason', sa.Text(), nullable=True))
    op.add_column('report', sa.Column('url', sa.String(), nullable=True))


def downgrade():
    op.drop_column('report', 'url')
    op.drop_column('report', 'failed_readon')
