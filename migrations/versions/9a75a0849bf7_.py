"""Add user_id to report and report_scheduler

Revision ID: 9a75a0849bf7
Revises: de8affc26ea6
Create Date: 2018-01-19 21:38:11.061605

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9a75a0849bf7'
down_revision = 'de8affc26ea6'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('report', sa.Column('user_id', sa.String(), nullable=False))
    op.add_column('report_scheduler', sa.Column('user_id', sa.String(), nullable=False))


def downgrade():
    op.drop_column('report_scheduler', 'user_id')
    op.drop_column('report', 'user_id')
