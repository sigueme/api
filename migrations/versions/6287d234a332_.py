"""report_scheduler and report tables

Revision ID: 6287d234a332
Revises: f8a07e03b276
Create Date: 2018-01-12 18:33:26.592537

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6287d234a332'
down_revision = 'f8a07e03b276'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('report_scheduler',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('org_subdomain', sa.String(), nullable=True),
        sa.Column('builder', sa.String(), nullable=False),
        sa.Column('params', sa.Text(), nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('last_run_at', sa.DateTime(), nullable=True),
        sa.Column('name', sa.String(), nullable=True),
        sa.Column('run_at', sa.DateTime(), nullable=True),
        sa.Column('cron', sa.String(), nullable=True),
        sa.Column('active', sa.Boolean(), nullable=False),
        sa.PrimaryKeyConstraint('id')
    )

    op.create_index(op.f('ix_report_scheduler_org_subdomain'), 'report_scheduler', ['org_subdomain'], unique=False)

    op.create_table('report',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('scheduler_id', sa.Integer(), nullable=True),
        sa.Column('org_subdomain', sa.String(), nullable=True),
        sa.Column('status', sa.Enum('IN_PROGRESS', 'COMPLETED', 'FAILED', name='reportstatus'), nullable=False),
        sa.Column('builder', sa.String(), nullable=False),
        sa.Column('params', sa.Text(), nullable=False),
        sa.Column('requested_at', sa.DateTime(), nullable=False),
        sa.Column('completed_at', sa.DateTime(), nullable=True),
        sa.Column('name', sa.String(), nullable=True),
        sa.ForeignKeyConstraint(['scheduler_id'], ['report_scheduler.id'], ondelete='SET NULL'),
        sa.PrimaryKeyConstraint('id')
    )

    op.create_index(op.f('ix_report_org_subdomain'), 'report', ['org_subdomain'], unique=False)
    op.create_index(op.f('ix_report_status'), 'report', ['status'], unique=False)


def downgrade():
    op.drop_index(op.f('ix_report_status'), table_name='report')
    op.drop_index(op.f('ix_report_org_subdomain'), table_name='report')
    op.drop_table('report')
    op.drop_index(op.f('ix_report_scheduler_org_subdomain'), table_name='report_scheduler')
    op.drop_table('report_scheduler')
