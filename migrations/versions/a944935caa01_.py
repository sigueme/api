"""make org_subdomain of report not null

Revision ID: a944935caa01
Revises: 6287d234a332
Create Date: 2018-01-12 21:47:22.751154

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a944935caa01'
down_revision = '6287d234a332'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('report', 'org_subdomain',
               existing_type=sa.VARCHAR(),
               nullable=False)


def downgrade():
    op.alter_column('report', 'org_subdomain',
               existing_type=sa.VARCHAR(),
               nullable=True)
